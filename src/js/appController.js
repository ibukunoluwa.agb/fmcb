/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your application specific code will go here
 */
define(['ojs/ojcore', 'knockout', 'ojs/ojmodule-element-utils', 'ojs/ojmodule-element', 'ojs/ojrouter', 'ojs/ojknockout', 'ojs/ojarraytabledatasource',
  'ojs/ojoffcanvas'],
  function(oj, ko, moduleUtils) {
     function ControllerViewModel() {
       var self = this;

	  function getParameterByName(name, url) {
			if (!url) url = window.location.href;
			name = name.replace(/[\[\]]/g, '\\$&');
			var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
				results = regex.exec(url);
			if (!results) return null;
			if (!results[2]) return '';
			return decodeURIComponent(results[2].replace(/\+/g, ' '));
	  }
	  // Application Name used in Branding Area
      self.appName = ko.observable("FCMB Blockchain");
	  self.useBlockchain = ko.observable("");
      
	  //Host URL for Database Service running on Node JS
	  self.ORDSURL = "https://ords-gse00015021.uscom-east-1.oraclecloud.com:443";
	  //Deployed Host URI that will be used for email
	  self.APPURI = "https://amini-gse00015021.uscom-east-1.oraclecloud.com:443";
	  
	  
	  self.BCSExecuteURL = "https://fcmbblockchaingateway-gse00015021.uscom-east-1.oraclecloud.com:443/blockchain/api/execute";
	  self.BCSQueryURL = "https://fcmbblockchaingateway-gse00015021.uscom-east-1.oraclecloud.com:443/blockchain/api/query";
	  self.BCSChainCodeName = "tradefin_ledger_prod";
	  self.BCSChannel = "tradefinance";
	  self.BCSChainCodeVer = "v1";
	  
	  
	  self.DoCSinstance = "https://docs-gse00015021.uscom-east-1.oraclecloud.com:443";
	  self.DOCSUrl = self.DoCSinstance + '/documents/api/1.2';
	  //Folder ID where all content will be stored
	  self.DOCSParentId = "F9CAB6E889ACE449822F35736C8BBB84DA3CDC82D01C";
	  
	  //key used for encryption
	  self.AESKey = "0r@C13";
	  
	  //PROFILE TYPES
	  self.CUST_PROFILETYPE = "C001";
	  self.DATA_PROFILETYPE = "D001";
	  self.APPR_PROFILETYPE = "A001";
	  self.MAGR_PROFILETYPE = "M001";
	  
	  self.signOutText = "Sign In";
	  self.userName = "Guest";
	  self.isLoggedIn = false;
	  if (sessionStorage.getItem("userKey")) {
		  self.isLoggedIn = true;
		  self.userName = sessionStorage.getItem("userKey");
		  self.signOutText = "Sign Out";
	  }else{
		  $( "#changepassword" ).remove();
		  $( "#pref" ).remove();
	  }
	  
      // Media queries for repsonsive layouts
      var smQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
      self.smScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);
      var mdQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.MD_UP);
      self.mdScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(mdQuery);

	  // Router setup
      self.router = oj.Router.rootInstance;
      self.router.configure({
         'welcome': {label: 'Welcome', isDefault: true},
         'login': {label: 'Login'},
         'register': {label: 'Customer Register'},
         'success': {label: 'Success'},
         'error': {label: 'Error'},
         'resetpassword': {label: 'ResetPassword'}
      });
      //oj.Router.defaults['urlAdapter'] = new oj.Router.urlParamAdapter();

	  var navData = [
			{name: 'Welcome', id: 'welcome',
				iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-person-icon-24'},
			{name: 'Login', id: 'login',
				iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-person-icon-24'},
			{name: 'Register', id: 'register',
			   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-people-icon-24'}
			
	  ];
	  
	  if(self.isLoggedIn == true){
		  if (sessionStorage.getItem("profileType") == "M001") {
			  self.router.configure({
				 'dashboard': {label: 'Dashboard', isDefault: true},
				 
				 'form_m_clist': {label: 'CBN Form Approval'},
				 'form_view': {label: 'Form View'},
				 'form_preview': {label: 'Form Preview'},
				 'form_m_alist': {label: 'Approved Form M Applications'},
				 
				 'fx_list': {label: 'FX Request View'},  
				 'fx_approved_view': {label: 'View Approved FX Request'}, 
				 'profile': {label: 'Profile'},
				 'success': {label: 'Success'},
				 'error': {label: 'Error'},
				 'changepassword': {label: 'ChangePassword'},
				 'signout': {label: 'SignOut'}
			   });
			   
			    navData = [
				  {name: 'Dashboard', id: 'dashboard',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-chart-icon-24'},
				   
				   {name: 'CBN Form Approval', id: 'form_m_clist',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-library-icon-24'},
				   
				   {name: 'All Form M Applications', id: 'form_m_alist',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-library-icon-24'},
				   
				   {name: 'Rejected FX Request', id: 'fx_list',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-person-icon-24'},
				   
				   {name: 'Profile', id: 'profile',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-person-icon-24'},
				   
				  {name: 'Change Password', id: 'changepassword',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-edit-icon-24'}
			   ];
		  } else if (sessionStorage.getItem("profileType") == "D001") {
			  self.router.configure({
				 'dashboard': {label: 'Dashboard', isDefault: true},
				 'upload_xml': {label: 'Upload CBN XML'},
				 'form_m_list': {label: 'Form M Applications'},
				 'form_m_alist': {label: 'Approved Form M Applications'},
				 'form_view': {label: 'Form View'},
				 'form_preview': {label: 'Form Preview'},
				 
				 'form_m': {label: 'New FormM'},
				 'fx_request': {label: 'FX Request'},
				 'fx_list': {label: 'FX Request List'},
				 'fx_approved_view': {label: 'FX Approved View'},
				 'fx_netoff': {label: 'FX Net-Off'},
				 'profile': {label: 'Profile'},
				 'success': {label: 'Success'},
				 'error': {label: 'Error'},
				 'changepassword': {label: 'ChangePassword'},
				 'signout': {label: 'SignOut'}
			   });
			   
			    navData = [
				  {name: 'Dashboard', id: 'dashboard',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-chart-icon-24'},
				   
				   {name: 'Upload CBN XML', id: 'upload_xml', 
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-library-icon-24'},
				   
				   {name: 'New Form M', id: 'form_m', 
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-library-icon-24'},
				   {name: 'My Form M Applications', id: 'form_m_list',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-library-icon-24'},
				   
				   {name: 'All Form M Applications', id: 'form_m_alist',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-library-icon-24'},
				   
				   {name: 'New FX Request', id: 'fx_request',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-person-icon-24'},
				   
				   {name: 'FX Request', id: 'fx_list',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-person-icon-24'},
				   
				   {name: 'FX Net-Off', id: 'fx_netoff',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-person-icon-24'},
				  
				   {name: 'Profile', id: 'profile',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-person-icon-24'},
				   
				   {name: 'HSCodes', id: 'hscode_list',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-person-icon-24'},
				  {name: 'Change Password', id: 'changepassword',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-edit-icon-24'}
			   ];
		  } else if (sessionStorage.getItem("profileType") == "A001") {
			  self.router.configure({
				 'dashboard': {label: 'Dashboard', isDefault: true},
				 'form_m_plist': {label: 'Approve Pending Form M'},
				 'form_view': {label: 'Form View'},
				 'form_preview': {label: 'Form Preview'},
				 'form_m_alist': {label: 'Approved Form M Applications'},
				 
				 'hscode_list': {label: 'HSCode List'},
				 'fx_approval': {label: 'FX Approval'},
				 'fx_approval_edit': {label: 'Approve FX Request'}, 
				 'fx_list': {label: 'FX Request List'}, 
				 'fx_approved_view': {label: 'View Approved FX Request'}, 
				 'fx_approve_netoff': {label: 'FX Approve Net-Off'},
				 'profile': {label: 'Profile'},
				 'success': {label: 'Success'},
				 'error': {label: 'Error'},
				 'changepassword': {label: 'ChangePassword'},
				 'signout': {label: 'SignOut'}
			   });
			   
			    navData = [
				  {name: 'Dashboard', id: 'dashboard',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-chart-icon-24'},
				   
				   {name: 'Approve Pending Form M', id: 'form_m_plist',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-library-icon-24'},
				   
				   {name: 'All Form M Applications', id: 'form_m_alist',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-library-icon-24'},
				   
				   {name: 'FX Pending Request', id: 'fx_approval',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-person-icon-24'},
				   
				   {name: 'FX Request List', id: 'fx_list',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-person-icon-24'},
				  
				   {name: 'Approved Net-Off', id: 'fx_approve_netoff',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-person-icon-24'},
				   
				   {name: 'Profile', id: 'profile',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-person-icon-24'},
				   
				   {name: 'HSCodes', id: 'hscode_list',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-person-icon-24'},
				  {name: 'Change Password', id: 'changepassword',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-edit-icon-24'}
			   ];
		  }
		  else if (sessionStorage.getItem("profileType") == "K001") {
			  self.router.configure({
				 'dashboard': {label: 'Dashboard', isDefault: true},
				 'form_m_list': {label: 'Form M Applications'},
				 'form_m_plist': {label: 'Approve Pending Form M'},
				 'form_m_clist': {label: 'CBN Form Approval'},
				 'form_view': {label: 'Form View'},
				 'form_m': {label: 'New FormM'},
				 'hscode_list': {label: 'HSCode List'},
				 'fx_request': {label: 'FX Request'},
				 'fx_approval': {label: 'FX Approval'},
				 'fx_approval_edit': {label: 'Approve FX Request'}, 
				 'fx_rejected': {label: 'Rejected FX Request'}, 
				 'fx_approved': {label: 'Approved FX Request'}, 
				 'fx_approved_view': {label: 'View Approved FX Request'}, 
				 'profile': {label: 'Profile'},
				 'success': {label: 'Success'},
				 'error': {label: 'Error'},
				 'changepassword': {label: 'ChangePassword'},
				 'signout': {label: 'SignOut'}
			   });
			   
			    navData = [
				  {name: 'Dashboard', id: 'dashboard',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-chart-icon-24'},
				   {name: 'Form M Applications', id: 'form_m_list',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-library-icon-24'},
				   {name: 'Approve Pending Form M', id: 'form_m_plist',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-library-icon-24'},
				   
				   {name: 'CBN Form Approval', id: 'form_m_clist',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-library-icon-24'},
				   
				   {name: 'New Form M', id: 'form_m', 
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-library-icon-24'},
				   
				   {name: 'FX Request', id: 'fx_request',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-person-icon-24'},
				   {name: 'FX Pending Request', id: 'fx_approval',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-person-icon-24'},
				   {name: 'Rejected FX Request', id: 'fx_rejected',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-person-icon-24'},
				   {name: 'Approved FX Request', id: 'fx_approved',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-person-icon-24'},
				   
				   {name: 'Profile', id: 'profile',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-person-icon-24'},
				   
				   {name: 'HSCodes', id: 'hscode_list',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-person-icon-24'},
				  {name: 'Change Password', id: 'changepassword',
				   iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-edit-icon-24'}
			   ];
		  }
		   
			 
	  }
	  /*
       // Router setup
       self.router = oj.Router.rootInstance;
       self.router.configure({
         'dashboard': {label: 'Dashboard', isDefault: true},
         'incidents': {label: 'Incidents'},
         'customers': {label: 'Customers'},
         'about': {label: 'About'}
       });
	   */
      oj.Router.defaults['urlAdapter'] = new oj.Router.urlParamAdapter();

      self.moduleConfig = ko.observable({'view':[], 'viewModel':null});

      self.loadModule = function() {
        ko.computed(function() {
          var name = self.router.moduleConfig.name();
          var viewPath = 'views/' + name + '.html';
          var modelPath = 'viewModels/' + name;
          var masterPromise = Promise.all([
            moduleUtils.createView({'viewPath':viewPath}),
            moduleUtils.createViewModel({'viewModelPath':modelPath})
          ]);
          masterPromise.then(
            function(values){
              self.moduleConfig({'view':values[0],'viewModel':values[1]});
            }
          );
        });
      };

      
      self.navDataSource = new oj.ArrayTableDataSource(navData, {idAttribute: 'id'});

      // Drawer
      // Close offcanvas on medium and larger screens
      self.mdScreen.subscribe(function() {oj.OffcanvasUtils.close(self.drawerParams);});
      self.drawerParams = {
        displayMode: 'push',
        selector: '#navDrawer',
        content: '#pageContent'
      };
      // Called by navigation drawer toggle button and after selection of nav drawer item
      self.toggleDrawer = function() {
        return oj.OffcanvasUtils.toggle(self.drawerParams);
      }
      // Add a close listener so we can move focus back to the toggle button when the drawer closes
      $("#navDrawer").on("ojclose", function() { $('#drawerToggleButton').focus(); });

      // Header
      // Application Name used in Branding Area
      //self.appName = ko.observable("App Name");
      // User Info used in Global Navigation area
      // User Info used in Global Navigation area
      self.userLogin = ko.observable(self.userName);

      // Footer
      function footerLink(name, id, linkTarget) {
        this.name = name;
        this.linkId = id;
        this.linkTarget = linkTarget;
      }
      self.footerLinks = ko.observableArray([
        new footerLink('About Oracle', 'aboutOracle', 'http://www.oracle.com/us/corporate/index.html#menu-about'),
        new footerLink('Contact Us', 'contactUs', 'http://www.oracle.com/us/corporate/contact/index.html'),
        new footerLink('Legal Notices', 'legalNotices', 'http://www.oracle.com/us/legal/index.html'),
        new footerLink('Terms Of Use', 'termsOfUse', 'http://www.oracle.com/us/legal/terms/index.html'),
        new footerLink('Your Privacy Rights', 'yourPrivacyRights', 'http://www.oracle.com/us/legal/privacy/index.html')
      ]);
     }

	 self.menuItemSelect = function( event, ui ) {
        //self.selectedMenuItem(ui.item.children("a").text());
        console.log('In menu item selection' + event.target.value);
        navTo = event.target.value;
        if(navTo == 'signout'){
			if (sessionStorage.getItem("userKey")) {
				window.location.replace('/?root=' + navTo);
			}else{
				navTo = "login";
				window.location.replace('/?root=' + navTo);
			}
		}else{
			window.location.replace('/?root=' + navTo);
		}
     };
	 
     return new ControllerViewModel();
  }
);
