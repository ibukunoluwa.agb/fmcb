/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your about ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
		'ojs/ojbutton', 'ojs/ojrouter', 'ojs/ojlabel', 'messages', 'ojs/ojarraydataprovider', 'ojs/ojtable', 'ojs/ojswitch',
		'ojs/ojmessages', 'ojs/ojmessage', 'ojs/ojaccordion', 'ojs/ojdialog'],
 function(oj, ko, $) {
  
	function getParameterByName(name, url) {
		if (!url) url = window.location.href;
		name = name.replace(/[\[\]]/g, '\\$&');
		var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, ' '));
	}
	
	function generateCID(prefix){
			 
			var d = new Date();
			var multiplier = 3;
			var time = d.getTime(); 
			var year = d.getUTCFullYear();
			
			var guid =  prefix + "-" + (parseInt(multiplier) * parseInt(time));
			return guid;
			
	};
	
	function encode(str){
		return btoa(str);
	}
	function decode(str){
		return atob(str);
	}
	
    function FXApprovedViewModel() {
      var self = this;
	  self.rootModel = ko.dataFor(document.getElementById('globalBody'));
	  console.log("*****");
	  self.navToError = "error";
	  self.navTo = "success";
	  self.titlePrefix = "N/A";
	  
	  self.requestID = getParameterByName("req");
	  self.requestMode = getParameterByName("mode");
	  self.requestStatus = getParameterByName("s");
	  if(self.requestID == null || self.requestID == ""){
		  window.location.replace('/?root=' + self.navToError + "&code=100");
		  return;
	  }
	  self.formStatus = ko.observable("APPROVED");
	  
	  
	  //self.formStatus = ko.observable("APPROVED");
	  if(self.requestMode == null || self.requestMode == ""){
		  self.formStatus("APPROVED"); //default
		  self.titlePrefix = "Approved";
	  }else{
		  console.log("found mode...." + decode(self.requestMode));
		  self.formStatus(decode(self.requestMode));
		  if(self.formStatus() == "APPROVED"){
			  self.titlePrefix = "Approved";
		  }else{
			  self.titlePrefix = "Rejected";
		  }
	  }
	  
	  if(self.requestStatus == null || self.requestStatus == ""){
		  self.formStatus("APPROVED");
	  }else{
		  self.formStatus(self.requestStatus);
	  }
	  
	  self.inputer = ko.observable(sessionStorage.getItem("userId"));
	  
	  //form M request object
	  self.approvalID = ko.observable("");
	  self.appNumber = ko.observable("");
	  self.formMNumber = ko.observable("");
	  self.formMID = ko.observable("");
	  self.requestAmount = ko.observable("");
	  self.requestLetter = ko.observable("");
	  self.requestDate = ko.observable("");
	  self.createDate = ko.observable("");
	  self.creator = ko.observable("");
	  
	  //switch control values
	  self.isFundAvailable = ko.observable(false);
	  self.isDocCorrect = ko.observable(false);
	  self.isValidity = ko.observable(false);
	  self.isValidOutstanding = ko.observable(false);
	  
	  self.fundCheckedClass = ko.observable("green");
	  self.docCheckedClass = ko.observable("green");
	  self.validityCheckedClass = ko.observable("green");
	  self.outstandingCheckedClass = ko.observable("green");
	  
	  self.fxAuditArray = [];
	  self.fxAuditObservableArray = ko.observableArray(self.fxAuditArray);
	  //self.formMDataProvider = new oj.ArrayDataProvider(self.formMObservableArray, {keyAttributes: 'formid'});
	  self.fxAuditDataProvider = new oj.ArrayTableDataSource(self.fxAuditObservableArray, {idAttribute: 'auditId'});
	  self.paginDataSource = new oj.PagingTableDataSource(self.fxAuditDataProvider);
	  
      console.log('----------**-==-**-------------'); 
	  
	  self.closeDialog = function (event) {
		document.getElementById('modalDialog1').close();
	  }

	  self.openDialog = function (event) {
		document.getElementById('modalDialog1').open();
		
	  }
	  
	  self.cancelButtonAction = function(event){
		  //var comment = $("#txtComment").val();
		  //self.approveRequest("rejecting", self.requestID, self.approvalID(), "APPROVED", "PENDING", generateCID('AUD'), comment);
	  };
	
	  
      self.getFXRequestDetails = function(){
		console.log("in getting formM list " + self.formStatus());
		//URL to instantiate a process
		//https://129.156.113.190/ords/pdb1/fcmb/fx/request/details/
		var serviceURL = self.rootModel.ORDSURL + "/ords/pdb1/fcmb/fx/request/details/";
		
		$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting form data.</em>");
		console.log("---->>>>>>>>>>>" + serviceURL);
		
		$.ajax ( {
			type: 'GET',
			url: serviceURL,
			cache: true,
			processData: false,
			contentType: false,
			crossDomain: true,
			xhrFields: { withCredentials: false },
			beforeSend: function (xhr) { 
				console.log('setting credentials.......');
				xhr.setRequestHeader ('Content-Type', 'application/json');  
				xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
				xhr.setRequestHeader ('status', self.formStatus());
				xhr.setRequestHeader ('id', self.requestID);
			},
			success: function(data) {  
				
				console.log(JSON.stringify(data));
				
				var countedResult = data.items.length;
				var topData = [];
				for(i = 0; i < countedResult; i++){
					itemData = data.items[i];
					jsonData = itemData;//JSON.parse(JSON.stringify(itemData));
					
					self.approvalID(jsonData.approvalid);
					self.appNumber(jsonData.application_number);
					self.formMNumber(jsonData.form_m_number);
					self.requestAmount(jsonData.amount);
					self.requestLetter(jsonData.letter);
					self.requestDate(jsonData.request_date);
					self.createDate(jsonData.application_number);
					self.creator(jsonData.action_by_name);
					self.formMID(jsonData.formid);
					
					self.fundCheckedClass(jsonData.check_fund_availability == "1" ? "green" : "red");
					self.isFundAvailable(jsonData.check_fund_availability == "1" ? true : false);
					
					self.docCheckedClass(jsonData.check_documentation == "1" ? "green" : "red");
					self.isDocCorrect(jsonData.check_documentation == "1" ? true : false);
					
					self.validityCheckedClass(jsonData.check_validity == "1" ? "green" : "red");
					self.isValidity(jsonData.check_validity == "1" ? true : false);
					
					self.outstandingCheckedClass(jsonData.check_total_outstanding == "1" ? "green" : "red");
					self.isValidOutstanding(jsonData.check_total_outstanding == "1" ? true : false);
					
					console.log(self.appNumber() + "========" + self.requestAmount());
					self.getAuditList();
					self.getFormMDetails(self.formMID());
				}
				//var formMTable = document.getElementById('formMList');
				//formMTable.refresh();
				//$("#status").html("");
				//self.formMDataProvider(new oj.ArrayTableDataSource(self.formMArray, {idAttribute: 'formid'}));
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("=====sending error ========" + jqXHR.responseText);
				$("#status").html("<br/><em class='error-message'>Error getting FX Request details. Please reload again.</em>");
				//$("#status").html(jqXHR.responseText);
			}
		} );
		
		
	  };
	  
	  self.getAuditList = function(){
		console.log("in getting formM list " + self.formStatus());
		//URL to instantiate a process
		//https://129.156.113.190/ords/pdb1/fcmb/fx/request/audit/list/
		var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/fx/request/audit/list/";
		
		$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting FX Approved audit information for transaction.</em>");
		console.log(self.approvalID() + "---->>>>>>>>>>>" + serviceURL);
		
		$.ajax ( { 
			type: 'GET',
			url: serviceURL,
			cache: true,
			processData: false,
			contentType: false,
			crossDomain: true,
			xhrFields: { withCredentials: false },
			beforeSend: function (xhr) { 
				console.log('setting credentials.......');
				xhr.setRequestHeader ('Content-Type', 'application/json');  
				xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
				xhr.setRequestHeader ('id', self.approvalID());
			},
			success: function(data) { 
				
				console.log(JSON.stringify(data));
				
				var countedResult = data.items.length;
				var topData = [];
				for(i = 0; i < countedResult; i++){
					itemData = data.items[i];
					jsonData = itemData;//JSON.parse(JSON.stringify(itemData));
					//console.log("branch..." + data.items[i].branchname);
					var fxAuditData = {
								 'auditId': jsonData.id,
								 'approvalid': jsonData.approvalid,
								 'from_status': jsonData.from_status,
								 'to_status': jsonData.to_status,
								 'comment': jsonData.action_comment,
								 'audit_date': jsonData.action_date,
								 'actionBy': jsonData.action_by_name
							  };
					self.fxAuditArray.push(fxAuditData);
					self.fxAuditObservableArray.push(fxAuditData);
					
				}
				var fxAuditTable = document.getElementById('auditList');
				fxAuditTable.refresh();
				$("#status").html("");
				
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("=====sending error ========" + jqXHR.responseText);
				$("#status").html("<br/><em class='error-message'>Error getting existing audit report. Please try again later</em>");
				//$("#status").html(jqXHR.responseText);
			}
		} );
		
	  };
	  
	  self.getFormMDetails = function(formID){
		var _formID = formID;
		//console.log("session form ID: " + _formID);
		//URL to instantiate a process
		var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/formm/details/" + _formID;
		
		$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting form data.</em>");
		console.log("---->>>>>>>>>>>" + serviceURL);
		
		$.ajax ( {
			type: 'GET',
			url: serviceURL,
			cache: true,
			processData: false,
			contentType: false,
			crossDomain: true,
			xhrFields: { withCredentials: false },
			beforeSend: function (xhr) { 
				//console.log('setting credentials.......');
				xhr.setRequestHeader ('Content-Type', 'application/json');  
				xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
			},
			success: function(data) { 
				//console.log("log from form M table: " + data);
				self.setFormData(data);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("=====sending error ========" + jqXHR.responseText);
				$("#status").html("<br/><em class='error-message'>Error getting existing Form M Details. Please try again later</em>");
				//$("#status").html(jqXHR.responseText);
			}
		} ); 
	  };
	  
	  
	  self.setFormData = function(data){
			//console.log("Branch id...." + data.branchid);
			$("#cbBranch").val(data.branchid);
			$("#txtValidityDate").val();

			$("#txtApplicantName").val(data.applicant_name);
			$("#txtApplicantNumber").val(data.applicant_account_no);
			$("#txtApplicantRCNo").val(data.applicant_rc_number);
			$("#txtApplicantTIN").val(data.taxid);
			$("#txtApplicantEmail").val(data.applicant_email);
			$("#txtApplicantPhone").val(data.applicant_phoneno);
			$("#txtApplicantAddr").val(data.applicant_address);

			$("#txtBeneName").val(data.beneficiary_name);
			$("#txtBeneEmail").val(data.beneficiary_email);
			$("#txtBenePhone").val(data.beneficiary_phoneno);
			$("#txtBeneAddr").val(data.beneficiary_address);
			
			$("#ckFXValidity").val(data.forex_validity);
			$("#cbPaymentMode").val(data.payment_mode);
			$("#txtDesc").val(data.general_description);
			$("#txtTotalFOB").val(data.total_fob_value);
			$("#txtFCharge").val(data.total_freight_charge);
			$("#txtAncilary").val(data.ancilary_charge);
			$("#txtTotalCF").val(data.total_cf_value);
			$("#txtTransferMode").val(data.transfer_mode);
			$("#cbShipmentMode").val(data.shipment_mode);
			$("#txtDischargePort").val(data.actual_port_discharge);
			$("#txtAgent").val(data.inspection_agent);
			$("#cbOriginCountry").val(data.country_of_origin);
			$("#cbSupplyCountry").val(data.country_of_supply);
			
			$("#status").html("");
	  };
	  
	  
	  self.getFXRequestDetails();
	  
      // Below are a subset of the ViewModel methods invoked by the ojModule binding
      // Please reference the ojModule jsDoc for additional available methods.

      /**
       * Optional ViewModel method invoked when this ViewModel is about to be
       * used for the View transition.  The application can put data fetch logic
       * here that can return a Promise which will delay the handleAttached function
       * call below until the Promise is resolved.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @return {Promise|undefined} - If the callback returns a Promise, the next phase (attaching DOM) will be delayed until
       * the promise is resolved
       */
      self.handleActivated = function(info) {
        // Implement if needed
		sessionStorage.removeItem("mod_FormMID");
	    sessionStorage.removeItem("mod_Mode");
      };

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {boolean} info.fromCache - A boolean indicating whether the module was retrieved from cache.
       */
      self.handleAttached = function(info) {
        // Implement if needed
		
      };


      /**
       * Optional ViewModel method invoked after the bindings are applied on this View. 
       * If the current View is retrieved from cache, the bindings will not be re-applied
       * and this callback will not be invoked.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       */
      self.handleBindingsApplied = function(info) {
        // Implement if needed
      };

      /*
       * Optional ViewModel method invoked after the View is removed from the
       * document DOM.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {Array} info.cachedNodes - An Array containing cached nodes for the View if the cache is enabled.
       */
      self.handleDetached = function(info) {
        // Implement if needed
      };
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    //return new FormMPendingListViewModel();
	var fmM = new FXApprovedViewModel();
	$(document).ready
    (
		function()
		{
			console.log("-------event when document is ready");
			//var formMTable = document.getElementById('formMList');
			//ko.applyBindings(fmM, formMTable);
			//formMTable.addEventListener('currentRowChanged', fmM.currentRowListener);
			
			/*
			
			
			table.addEventListener('ojBeforeCurrentRow', vm.beforeCurrentRowListener);
			table.addEventListener('ojSort', vm.sortListener);
			table.addEventListener('selectionChanged', vm.selectionListener);*/
		}
	);
	return fmM;
  }
);
