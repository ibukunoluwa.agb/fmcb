/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your about ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
		'ojs/ojbutton', 'ojs/ojrouter', 'ojs/ojlabel', 'messages', 'ojs/ojarraydataprovider', 'ojs/ojtable', 'ojs/ojtrain',
		'ojs/ojmessages', 'ojs/ojmessage', 'ojs/ojdatetimepicker', 'ojs/ojtimezonedata','ojs/ojchart', 'ojs/ojgauge','ojs/ojtimeline'],
 function(oj, ko, $) {
  
	function getParameterByName(name, url) {
		if (!url) url = window.location.href;
		name = name.replace(/[\[\]]/g, '\\$&');
		var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, ' '));
	}
	
	function timeConverter(UNIX_timestamp){
	  var a = new Date(UNIX_timestamp * 1000);
	  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	  var year = a.getFullYear();
	  var month = months[a.getMonth()];
	  var date = a.getDate();
	  var hour = a.getHours();
	  var min = a.getMinutes();
	  var sec = a.getSeconds();
	  var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
	  return time;
	}
	function dateConverter(UNIX_timestamp){
	  var a = new Date(UNIX_timestamp * 1000);
	  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	  var year = a.getFullYear();
	  var month = months[a.getMonth()];
	  var date = a.getDate();
	  var hour = a.getHours();
	  var min = a.getMinutes();
	  var sec = a.getSeconds();
	  var time = month + ' ' + date + ', ' + year ;
	  return time;
	}
	function timelineDateAdd(monthIndex){
	  
	  var a = new Date();
	  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	  //set the new date
	  //console.log("old Date: " + a);
	  var adjustedDate = a;
	  adjustedDate.setMonth(a.getMonth() + monthIndex);
	  //console.log("New Date: " + adjustedDate);
	  
	  var year = a.getFullYear();
	  var month = months[a.getMonth()] ;
	  var date = a.getDate();
	  var hour = a.getHours();
	  var min = a.getMinutes();
	  var sec = a.getSeconds();
	  var time = month + ' ' + date + ', ' + year ;
	  return time;
	}
	
	function getTimestamp(){
		var currentTime = parseInt((new Date().getTime() / 1000).toFixed(0));
		return currentTime
	}
	function getTimestampFromString(dateString){
		var currentTime = parseInt((new Date(dateString).getTime() / 1000).toFixed(0));
		return currentTime
	}
	
	
    function FormViewModel() {
		var self = this;
		self.rootModel = ko.dataFor(document.getElementById('globalBody'));
		self.navToError = "error";
		self.inputer = ko.observable(sessionStorage.getItem("userId"));
		self.navTo = "form_m_plist";
		
		var timestamp = getTimestampFromString("2019-02-12");
		console.log("Time converter string..." + getTimestampFromString("2019-02-12"));
		console.log("Time converter..." + dateConverter(604800000));
		
		//action button settings
		self.saveButtonText = ko.observable("");
		self.rejectButtonText = ko.observable("");
		self.subTitle = ko.observable("");
		
		self.formID = ko.observable(getParameterByName("fid"));
		if(self.formID() == null || self.formID() == ""){
			window.location.replace('/?root=' + self.navToError + "&code=100");
			return;
		}
		//=====removeAttribute
		function randomDate(start, end)
		{
		return new Date(start.getTime() + Math.random() *
		  (end.getTime() - start.getTime()));
		}
		
		/**
		* Generates random data
		* @param {Number} numItems Number of items per series
		* @return {Object} Data object containing random data
		*/
		function generateRandomData(numItems) {
			var data = {'series': []}, i, dateRandom, date, title, start, end, week = 604800000;
			
			data['series'].push({'id': 'series', 'items': []});
			for (i = 0; i < numItems; i++) {
			  start = randomDate(new Date(2014, 0, 1), new Date());
			  end = randomDate(new Date(start), new Date(start.getTime() + week));
			  title = 'Item ' + i;
			  data['series'][0]['items'].push({
					id: title,
					start: start,
					title: title
			  });
			}
			return data;
		}
		numItems = 2;
		var timelineData = generateRandomData(numItems);
		console.log("Tumeline generated data " + JSON.stringify(timelineData));
		self.seriesValue = ko.observableArray([]);
		self.seriesValue(timelineData['series']);
		
		//********** analytics ***************
		
		//---- timeline series
		self.fxItems = ko.observableArray([]);
		self.timelineSeries = ko.computed(function () {
			return [{id: 's1', emptyText: 'No Data.', label:'FX Approved', items: self.fxItems()}]
		});
		self.timelineStart = timelineDateAdd(-12); //new Date("Mar 1, 2019").toISOString()
		self.timelineEnd = timelineDateAdd(4); //new Date("Jan 1, 2014").toISOString()
		self.currentDateString = new Date();
		var currentDate = new Date(self.currentDateString).toISOString();
		self.referenceObjects = ko.observableArray([{value: currentDate}]);

		
		self.maxTotalAmount = ko.observable(100000);
		self.segment1 = ko.observable(0);
		self.segment2 = ko.observable(0);
		self.currentAmount = ko.observable(0);
        self.numberConverterOptions = {style: 'currency', currency: 'USD', maximumFractionDigits: 0};
        self.numberConverter = oj.Validation.converterFactory("number").createConverter(self.numberConverterOptions);
        self.thresholdValues = [{max: self.segment1()}, {max: self.segment2()}, {}];
		
		
		/* toggle button variables */
		self.stackValue = ko.observable('off');
		self.orientationValue = ko.observable('vertical');

		/* bar chart data */
		var barSeries = [];
		var barGroups = ["Form M by Status"];
		self.barSeriesValue = ko.observableArray(barSeries);
		self.barGroupsValue = ko.observableArray(barGroups);

		//self.threeDValue = ko.observable('off');

		/* pie chart data */
		self.threeDValue = ko.observable('on');
		var entriesSeries = [];
		var fxSeries = [];
		self.entriesSeriesValue = ko.observableArray(entriesSeries);
		self.fxSeriesValue = ko.observableArray(fxSeries);
		
		this.validityDate = ko.observable(oj.IntlConverterUtils.dateToLocalIso(new Date()));
		self.converterValue = ko.observable("yyyy-MM-dd");
		self.dateConverter = ko.observable(oj.Validation.converterFactory(oj.ConverterFactory.CONVERTER_TYPE_DATETIME).
			createConverter(
			{
			  pattern : self.converterValue()
			}));
		
		
		//input values
		self.input_ApplicationNo = ko.observable("");
		self.input_FormMNo = ko.observable("");
		self.input_ValidityDate = ko.observable("");
		self.xmlMessage = ko.observable("");
		
		//status handler
		self.formStatus = ko.observable("PENDING");
		
		//form details fields
		self.saveMode = ko.observable("");
		self.formMNumber = ko.observable("");
		self.applicationNumber = ko.observable("");
		self.validity_end_date = ko.observable("");
		
		//declare all variables for form
		self.formtype = ko.observable("");
		self.forex_validity = ko.observable("");
		self.form_prefix = ko.observable("");
		self.general_description = ko.observable("");
		self.status = ko.observable("");
		self.create_date = ko.observable("");
		self.branchId = ko.observable("");

		self.applicant_name = ko.observable("");
		self.applicant_rc_number = ko.observable("");
		self.applicant_account_no = ko.observable("");
		self.applicant_address = ko.observable("");
		self.applicant_email = ko.observable("");
		self.applicant_phoneno = ko.observable("");
		self.applicant_taxid = ko.observable("");
		self.applicant_fax = ko.observable("");
		self.applicant_statecode = ko.observable("");
		self.applicant_statedesc = ko.observable("");
		
		self.beneficiary_name = ko.observable("");
		self.beneficiary_address = ko.observable("");
		self.beneficiary_email = ko.observable("");
		self.beneficiary_phoneno = ko.observable("");
		self.beneficiary_fax = ko.observable("");
		
		self.customs_office = ko.observable("");
		self.shipment_mode = ko.observable("");
		self.country_of_origin = ko.observable("");
		self.country_of_supply = ko.observable("");
		self.actual_port_loading = ko.observable("");
		self.actual_port_discharge = ko.observable("");
		self.inspection_agent = ko.observable("");
		self.shipment_date = ko.observable("");
		self.designated_bank = ko.observable("");
		self.fund_source = ko.observable("");
		self.currency_code = ko.observable("");

		self.exchange_rate = ko.observable("");
		self.total_fob_value = ko.observable("");
		self.total_freight_charge = ko.observable("");
		self.ancilary_charge = ko.observable("");
		self.insurance_value = ko.observable("");
		self.total_cf_value = ko.observable("");
		self.total_foc = ko.observable("");
		self.proforma_invoice_no = ko.observable("");
		self.proforma_invoice_date = ko.observable("");
		self.payment_mode = ko.observable("");
		self.payment_date = ko.observable("");
		self.transfer_mode = ko.observable("");
		self.delivery_term = ko.observable("");
		
		self.no_of_goods = ko.observable("");
		self.net_weight = ko.observable("");
		
		self.goodsXML = ko.observable("");
		self.fullXML = ko.observable("");
		
		
	  self.newButtonAction = function(event){
		  
		  window.location.replace('/?root=' + self.navTo);
	  };
	  
	  
	  self.cancelButtonAction = function(event){
		  
	  };
	  
	  //steps processing and settings
	  self.previousStep = ko.observable(0);
	  self.previousStepObj = ko.observable();
	  self.isSaved = ko.observable(0);
      self.selectedStepValue = ko.observable('step0');
      this.stepArray =
        ko.observableArray(
                [{label:'Visualization', id:'step0'},
				 {label:'Summary/Header', id:'step1'},
                 {label:'Applicant/Beneficiary Details', id:'step2'},
                 {label:'Goods/Items', id:'step3'},
                 {label:'Financials', id:'step4'},
                 {label:'Transportation', id:'step5'},
                 {label:'Attachment', id:'step6'}]);
		
	  this.deselectStepAction = function(event) {
		  
			//console.log("deselect step.....===>" + JSON.stringify(event.detail));
			//self.previousStepObj(event.detail['fromStep']);
	  };
	  this.updateBeforeStepsAction = function(event) {
			//console.log("update before step.....===>" + JSON.stringify(event.detail));
			//self.previousStepObj(event.detail['fromStep']);
	  };
	  this.updateStepsAction = function(event) {
		  console.log("update before step.....===>" + JSON.stringify(event.detail));
		  var toStep = event.detail['toStep'];
		  self.processStepDisplayAction(toStep);
		  
	  };
	  
	  this.defaultNextStep = function(event) {
		 var train = document.getElementById("train");
		 
		 var toStepId = train.getNextSelectableStep();
		 var toStep = train.getStep(toStepId);
		 self.processStepDisplayAction(toStep, true);
	  }
	  
	  this.previousStepsAction = function(event) {
         var train = document.getElementById("train");
		 
		 var toStepId = train.getPreviousSelectableStep();
		 var toStep = train.getStep(toStepId);
		 self.processStepDisplayAction(toStep, true);
      };
	  
	  self.processStepDisplayAction = function(toStep){
		 console.log('In processStepDisplayAction event ---');
		 //console.log(self.isSaved() + '*******************************');
		 var train = document.getElementById("train");
		
		 var tmpStepName = 'step' + self.previousStep();
		 self.selectedStepValue(toStep.id);
		 train.updateStep(toStep.id, toStep);
		 train.refresh();
		 $("#status").html("");
		 
		 console.log('.... ready to hide and show component for ' + toStep.id);
		 $( "#dvstep0").hide('slow');
		 $( "#dvstep1").hide('slow');
		 $( "#dvstep2").hide('slow');
		 $( "#dvstep3").hide('slow');
		 $( "#dvstep4").hide('slow');
		 $( "#dvstep5").hide('slow');
		 $( "#dvstep6").hide('slow');
		 
		 //$( "#dvstep0").css("display","none");
		 $("#dv" + toStep.id).show('slow');
		 var _selectedStepNo = toStep.id.substr(toStep.id.length - 1, toStep.id.length);
		 //console.log("Now showing this as my previous step" + _selectedStepNo);
		 self.previousStep(_selectedStepNo);
		 //reset isSaved back
		 //self.isSaved(0);
	  }
	  
	  self.getAmountSummary = function(){
		
			//URL to instantiate a process
			var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/fx/form/outstanding/";
			var formId = self.formID();
			$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting amount summary.</em>");
			console.log("---->>>>>>>>>>>" + serviceURL);
			
			$.ajax ( {
				type: 'GET',
				url: serviceURL,
				cache: true,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					//console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
					xhr.setRequestHeader ('formid', formId);
				},
				success: function(data) { 
					console.log("-------amount summary---------");
					console.log(JSON.stringify(data));
					
					self.maxTotalAmount(data.total_amount);
					self.currentAmount(parseFloat(data.total_amount) - parseFloat(data.outstanding));
					self.segment1(parseFloat(data.total_amount) / 4);
					self.segment2(parseFloat(data.total_amount) / 2);
					
					$("#status").html("");
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("=====sending error ========" + jqXHR.responseText);
					$("#status").html("<br/><em class='error-message'>Error verifying email and password. Please try again later</em>");
					//$("#status").html(jqXHR.responseText);
				}
			} );
			
		
	    };
		self.getAmountSummary();
	
	  self.generateTimelineData = function(restData) {
			var data = {'series': []}, i, dateRandom, date, title, start, end, week = 604800000;
			
			data['series'].push({'id': 'series', 'items': []});
			/*
			for (i = 0; i < numItems; i++) {
			  start = randomDate(new Date(2014, 0, 1), new Date());
			  end = randomDate(new Date(start), new Date(start.getTime() + week));
			  title = 'Item ' + i;
			  data['series'][0]['items'].push({
					id: title,
					start: start,
					title: title
			  });
			}
			*/
			data['series'].push({'id': 'series', 'items': []});
			var countedResult = restData.items.length;
			var lastDate = "";
			var topData = [];
			for(i = 0; i < countedResult; i++){
				itemData = restData.items[i];
				jsonData = itemData;//JSON.parse(JSON.stringify(itemData));
				
				var saveDate = jsonData.request_date.split("T")[0];
				var timestamp = getTimestampFromString(saveDate);
				/*
				timelineItem = {};
				timelineItem['id'] = jsonData.requestid;
				timelineItem['title'] = "Amount: " + jsonData.amount + " | Status: " + jsonData.status;
				timelineItem['description'] = jsonData.letter;
				timelineItem['start'] = dateConverter(timestamp);
				self.fxItems.push(timelineItem);
				*/
				start = randomDate(new Date(2014, 0, 1), new Date());
				end = randomDate(new Date(start), new Date(start.getTime() + week));
				title = "Amount: " + jsonData.amount + " | Status: " + jsonData.status;
				data['series'][0]['items'].push({
						id: jsonData.requestid,
						start: dateConverter(timestamp),
						title: title
				  });
				  
				lastDate = dateConverter(timestamp);	  
			}
			console.log("********......series" + JSON.stringify(data['series']));
			self.seriesValue(data['series']);
			//return data;
	  };
	  
	  
	  self.getFXTimeline = function(){
		console.log("in getting fx timeline list");
		var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/analytics/form/fx/requests/";
		
		$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting FX timeline.</em>");
		console.log("---->>>>>>>>>>>" + serviceURL);
		
		$.ajax ( {
			type: 'GET',
			url: serviceURL,
			cache: true,
			processData: false,
			contentType: false,
			crossDomain: true,
			xhrFields: { withCredentials: false },
			beforeSend: function (xhr) { 
				//console.log('setting credentials.......');
				xhr.setRequestHeader ('Content-Type', 'application/json');  
				xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
				xhr.setRequestHeader ('formid', self.formID());
			},
			success: function(data) { 
				//console.log("----------------");
				console.log(JSON.stringify(data));
				/*
				var countedResult = data.items.length;
				var lastDate = "";
				var topData = [];
				for(i = 0; i < countedResult; i++){
					itemData = data.items[i];
					jsonData = itemData;//JSON.parse(JSON.stringify(itemData));
					
					var saveDate = jsonData.request_date.split("T")[0];
					var timestamp = getTimestampFromString(saveDate);
					
					timelineItem = {};
					timelineItem['id'] = jsonData.requestid;
					timelineItem['title'] = "Amount: " + jsonData.amount + " | Status: " + jsonData.status;
					timelineItem['description'] = jsonData.letter;
					timelineItem['start'] = dateConverter(timestamp);
					self.fxItems.push(timelineItem);
					
					lastDate = dateConverter(timestamp);	  
				}
				console.log(JSON.stringify(self.fxItems()));
				console.log(self.timelineSeries);
				console.log(JSON.stringify(self.timelineSeries));
				//self.currentDateString = lastDate;
				//var currentDate = new Date(self.currentDateString).toISOString();
				//self.referenceObjects([{value: currentDate}]);
				*/
				self.generateTimelineData(data);
				$("#status").html("");
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("=====sending error ========" + jqXHR.responseText);
				$("#status").html("<br/><em class='error-message'>Error verifying email and password. Please try again later</em>");
				//$("#status").html(jqXHR.responseText);
			}
		} );
	  };
	  self.getFXTimeline();
	  
	  self.getFormItemsDetails = function(){
		console.log("in getting formM list");
		//URL to instantiate a process
		//https://129.156.113.190/ords/pdb1/fcmb/formm/hscode/list/{formid}
		var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/formm/hscode/list/" + self.formID();
		
		$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting form goods data.</em>");
		console.log("---->>>>>>>>>>>" + serviceURL);
		
		$.ajax ( {
			type: 'GET',
			url: serviceURL,
			cache: true,
			processData: false,
			contentType: false,
			crossDomain: true,
			xhrFields: { withCredentials: false },
			beforeSend: function (xhr) { 
				//console.log('setting credentials.......');
				xhr.setRequestHeader ('Content-Type', 'application/json');  
				xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
				xhr.setRequestHeader ('status', self.formStatus());
				xhr.setRequestHeader ('inputer', self.inputer());
			},
			success: function(data) { 
				//console.log("----------------");
				//console.log(JSON.stringify(data));
				var countedResult = data.items.length;
				var topData = [];
				var total_net_weight = 0;
				for(i = 0; i < countedResult; i++){
					itemData = data.items[i];
					jsonData = itemData;//JSON.parse(JSON.stringify(itemData));
					total_net_weight = parseFloat(total_net_weight) + parseFloat(jsonData.net_weight);
					self.renderHTML(i + 1, jsonData);
				}
				 
				self.no_of_goods(countedResult);
				self.net_weight(total_net_weight);
				//generate XML down
				//self.generateXML();
				$("#status").html("");
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("=====sending error ========" + jqXHR.responseText);
				$("#status").html("<br/><em class='error-message'>Error verifying email and password. Please try again later</em>");
				//$("#status").html(jqXHR.responseText);
			}
		} );
		
		
	  };
	  self.renderHTML = function (counter, item){ 
			
			var template = $("#item_template").html();
			var generatedOutput  = template.replace(/TEMP_SN/g, counter);
			generatedOutput  = generatedOutput.replace(/TEMP_HSCODE/g, item.hscode);
			generatedOutput  = generatedOutput.replace(/TEMP_DESC/g, item.commercial_description);
			generatedOutput  = generatedOutput.replace(/TEMP_STATEOFGOODS/g, item.state_of_goods);
			generatedOutput  = generatedOutput.replace(/TEMP_SEC_PURPOSE/g, item.sectoral_purpose);
			generatedOutput  = generatedOutput.replace(/TEMP_P_QTY/g, item.packages);
			generatedOutput  = generatedOutput.replace(/TEMP_P_TYPE/g, item.packages_unit);
			
			generatedOutput  = generatedOutput.replace(/TEMP_NET/g, item.net_weight);
			generatedOutput  = generatedOutput.replace(/TEMP_M_QTY/g, item.measurement_quantity);
			generatedOutput  = generatedOutput.replace(/TEMP_M_TYPE/g, item.measurement_unit);
			
			generatedOutput  = generatedOutput.replace(/TEMP_PRICE/g, item.unit_price);
			generatedOutput  = generatedOutput.replace(/TEMP_FOB/g, item.fob_value);
			generatedOutput  = generatedOutput.replace(/TEMP_FREIGHT/g, item.freight_charge);
			
			currentHTML = $("#itemTable").html();
			$("#itemTable").html(currentHTML + generatedOutput);
			//document.getElementById["flightinfo"].innerHTML = currentHTML + output;
			var xmlAsString = "";
			xmlAsString = xmlAsString + "\r\n\t<ITM>";
			xmlAsString = xmlAsString + "\r\n\t\t<RNK>" + counter + "</RNK>";
			xmlAsString = xmlAsString + "\r\n\t\t<HSC>" + item.hscode + "</HSC>";
			xmlAsString = xmlAsString + "\r\n\t\t<DSC>" + item.commercial_description + "</DSC>";
			xmlAsString = xmlAsString + "\r\n\t\t<FRG>" + item.freight_charge + "</FRG>";
			xmlAsString = xmlAsString + "\r\n\t\t<SOG>" + item.state_of_goods + "</SOG>";
			
			xmlAsString = xmlAsString + "\r\n\t\t<SEP>";
			xmlAsString = xmlAsString + "\r\n\t\t\t<COD>" + item.sectoral_purpose + "</COD>";
			xmlAsString = xmlAsString + "\r\n\t\t</SEP>";
			
			xmlAsString = xmlAsString + "\r\n\t\t<PKG>";
			xmlAsString = xmlAsString + "\r\n\t\t\t<NBR>" + item.packages + "</NBR>";
			xmlAsString = xmlAsString + "\r\n\t\t\t<COD>" + item.packages_unit + "</COD>";
			xmlAsString = xmlAsString + "\r\n\t\t</PKG>";
			
			xmlAsString = xmlAsString + "\r\n\t\t<FOB>";
			xmlAsString = xmlAsString + "\r\n\t\t\t<VAL>" + item.fob_value + "</VAL>";
			xmlAsString = xmlAsString + "\r\n\t\t</FOB>";
			
			xmlAsString = xmlAsString + "\r\n\t\t<CTY>";
			xmlAsString = xmlAsString + "\r\n\t\t\t<ORG>";
			xmlAsString = xmlAsString + "\r\n\t\t\t\t<COD>" + item.country_of_origin + "</COD>";
			xmlAsString = xmlAsString + "\r\n\t\t\t</ORG>";
			xmlAsString = xmlAsString + "\r\n\t\t</CTY>";
			
			xmlAsString = xmlAsString + "\r\n\t\t<QTY>" + item.measurement_quantity + "</QTY>";
			
			xmlAsString = xmlAsString + "\r\n\t\t<UNT>";
			xmlAsString = xmlAsString + "\r\n\t\t\t<PRI>" + item.unit_price + "</PRI>";
			xmlAsString = xmlAsString + "\r\n\t\t\t<COD>" + item.measurement_unit + "</COD>";
			xmlAsString = xmlAsString + "\r\n\t\t</UNT>";
			
			xmlAsString = xmlAsString + "\r\n\t\t<WGT>";
			xmlAsString = xmlAsString + "\r\n\t\t\t<GRS>" + "" + "</GRS>"; //gross
			xmlAsString = xmlAsString + "\r\n\t\t\t<NET>" + item.net_weight + "</NET>"; //net
			xmlAsString = xmlAsString + "\r\n\t\t</WGT>";
			
			xmlAsString = xmlAsString + "\r\n\t\t<FOC>" + "0" + "</FOC>";
			
			xmlAsString = xmlAsString + "\r\n\t</ITM>";
			
			self.goodsXML(self.goodsXML() + "\r\n" + xmlAsString);
	  }
	  
      self.getFormDetails = function(){
		console.log("in getting formM list");
		//URL to instantiate a process
		var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/formm/details/" + self.formID();
		
		$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting form data.</em>");
		console.log("---->>>>>>>>>>>" + serviceURL);
		
		$.ajax ( {
			type: 'GET',
			url: serviceURL,
			cache: true,
			processData: false,
			contentType: false,
			crossDomain: true,
			xhrFields: { withCredentials: false },
			beforeSend: function (xhr) { 
				//console.log('setting credentials.......');
				xhr.setRequestHeader ('Content-Type', 'application/json');  
				xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
				xhr.setRequestHeader ('status', self.formStatus());
				xhr.setRequestHeader ('inputer', self.inputer());
			},
			success: function(data) { 
				
				//console.log(JSON.stringify(data));
				
				jsonData = data;//JSON.parse(JSON.stringify(itemData));
				//console.log("branch..." + data.items[i].branchname);
				
				
				self.formMNumber(jsonData.formm_number == null ? "" : jsonData.formm_number);
				self.applicationNumber(jsonData.application_number == null ? "" : jsonData.application_number);
				self.validity_end_date(jsonData.validity_end_date == null ? "" : jsonData.validity_end_date.split("T")[0]);
				self.saveMode(jsonData.save_mode == null ? "" : jsonData.save_mode);
				if(self.saveMode() == "XML"){
					self.input_ApplicationNo(self.applicationNumber());
					self.input_FormMNo(self.formMNumber());
					self.input_ValidityDate(self.validity_end_date());
					self.xmlMessage("Application Number, Form M Number and Validity Date were part of XML document.");
				}
				
				
				self.branchId(jsonData.branchid);
				self.formtype(jsonData.formtype);
				self.forex_validity(jsonData.forex_validity);
				self.form_prefix(jsonData.form_prefix);
				self.general_description(jsonData.general_description);
				self.status(jsonData.status);
				self.create_date(jsonData.create_date == null ? "" : jsonData.create_date.split("T")[0]);
				
				self.applicant_name(jsonData.applicant_name);
				self.applicant_rc_number(jsonData.applicant_rc_number);
				self.applicant_account_no(jsonData.applicant_account_no);
				self.applicant_address(jsonData.applicant_address);
				self.applicant_email(jsonData.applicant_email);
				self.applicant_phoneno(jsonData.applicant_phoneno);
				self.applicant_taxid(jsonData.taxid);
				self.applicant_fax(jsonData.applicant_fax);
				self.applicant_statecode(jsonData.applicant_state);
				self.applicant_statedesc(jsonData.applicant_state);
				
				self.beneficiary_name(jsonData.beneficiary_name);
				self.beneficiary_address(jsonData.beneficiary_address);
				self.beneficiary_email(jsonData.beneficiary_email);
				self.beneficiary_phoneno(jsonData.beneficiary_phoneno);
				self.beneficiary_fax(jsonData.beneficiary_fax);
				
				self.customs_office(jsonData.customs_office);
				self.shipment_mode(jsonData.shipment_mode);
				self.country_of_origin(jsonData.country_of_origin);
				self.country_of_supply(jsonData.country_of_supply);
				self.actual_port_loading(jsonData.actual_port_loading);
				self.actual_port_discharge(jsonData.actual_port_discharge);
				self.inspection_agent(jsonData.inspection_agent);
				self.shipment_date(jsonData.shipment_date == null ? "" : jsonData.shipment_date.split("T")[0]);
				self.designated_bank(jsonData.designated_bank);
				self.fund_source(jsonData.fund_source);
				self.currency_code(jsonData.currency_code);
				
				self.exchange_rate(jsonData.exchange_rate);
				self.total_fob_value(jsonData.total_fob_value);
				self.total_freight_charge(jsonData.total_freight_charge);
				self.ancilary_charge(jsonData.ancilary_charge);
				self.insurance_value(jsonData.insurance_value);
				self.total_cf_value(jsonData.total_cf_value);
				self.total_foc(jsonData.total_foc);
				self.proforma_invoice_no(jsonData.proforma_invoice_no);
				self.proforma_invoice_date(jsonData.proforma_invoice_date == null? "" : jsonData.proforma_invoice_date.split("T")[0]);
				self.payment_mode(jsonData.payment_mode);
				self.payment_date(jsonData.payment_date == null ? "" : jsonData.payment_date.split("T")[0]);
				self.transfer_mode(jsonData.transfer_mode);
				self.delivery_term(jsonData.delivery_term);
				
				$("#status").html("");
				self.getFormItemsDetails();
				
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("=====sending error ========" + jqXHR.responseText);
				$("#status").html("<br/><em class='error-message'>Error verifying email and password. Please try again later</em>");
				//$("#status").html(jqXHR.responseText);
			}
		} );
		
		
	  };
	  
	  /*
		
	  */
	  self.generateXML = function(){
		var xmlAsString = '<?xml version="1.0" encoding="UTF-8" ?>\r\n<' + (self.formtype() == "FORM M" ? "FORMM" : "") + '>';
		xmlAsString = xmlAsString + "\r\n\t<OPE>register</OPE>";
		xmlAsString = xmlAsString + "\r\n\t<INF>";
		xmlAsString = xmlAsString + "\r\n\t\t<FRX>" + (self.forex_validity() == "Y" ? "1" : "0") + "</FRX>";
		xmlAsString = xmlAsString + "\r\n\t\t<YEA>" + "2019" + "</YEA>";
		xmlAsString = xmlAsString + "\r\n\t\t<SER>" + self.applicationNumber() + "</SER>";
		xmlAsString = xmlAsString + "\r\n\t\t<PFX>" + self.form_prefix() + "</PFX>";
		xmlAsString = xmlAsString + "\r\n\t\t<BNK>";
		xmlAsString = xmlAsString + "\r\n\t\t\t<CODE>" + "214" + "</CODE>";
		xmlAsString = xmlAsString + "\r\n\t\t\t<COD>" + self.formMNumber() + "</COD>";
		xmlAsString = xmlAsString + "\r\n\t\t\t<BRA>" + self.branchId() + "</BRA>";
		xmlAsString = xmlAsString + "\r\n\t\t</BNK>";
		xmlAsString = xmlAsString + "\r\n\t</INF>";
		
		xmlAsString = xmlAsString + "\r\n\t<APP>";
		xmlAsString = xmlAsString + "\r\n\t\t<NAM>" + self.applicant_name() + "</NAM>";
		xmlAsString = xmlAsString + "\r\n\t\t<ADR>" + self.applicant_address() + "</ADR>";
		xmlAsString = xmlAsString + "\r\n\t\t<CIT>" + self.applicant_statedesc() + "</CIT>";
		xmlAsString = xmlAsString + "\r\n\t\t<TEL>" + self.applicant_phoneno() + "</TEL>";
		xmlAsString = xmlAsString + "\r\n\t\t<FAX>" + (self.applicant_fax() == null ? "" : self.applicant_fax()) + "</FAX>";
		xmlAsString = xmlAsString + "\r\n\t\t<MAI>" + self.applicant_email() + "</MAI>";
		xmlAsString = xmlAsString + "\r\n\t\t<STA>" + (self.applicant_statecode() == null ? "": self.applicant_statecode()) + "</STA>";
		xmlAsString = xmlAsString + "\r\n\t\t<RCN>" + self.applicant_rc_number() + "</RCN>";
		xmlAsString = xmlAsString + "\r\n\t\t<TIN>" + self.applicant_taxid() + "</TIN>";
		xmlAsString = xmlAsString + "\r\n\t</APP>";
		
		xmlAsString = xmlAsString + "\r\n\t<BEN>";
		xmlAsString = xmlAsString + "\r\n\t\t<NAM>" + self.beneficiary_name() + "</NAM>";
		xmlAsString = xmlAsString + "\r\n\t\t<ADR>" + self.beneficiary_address() + "</ADR>";
		xmlAsString = xmlAsString + "\r\n\t\t<CIT>" + "" + "</CIT>";
		xmlAsString = xmlAsString + "\r\n\t\t<ZIP>" + "N/A" + "</ZIP>";
		xmlAsString = xmlAsString + "\r\n\t\t<TEL>" + self.beneficiary_phoneno() + "</TEL>";
		xmlAsString = xmlAsString + "\r\n\t\t<FAX>" + (self.beneficiary_fax()== null? "": self.beneficiary_fax()) + "</FAX>";
		xmlAsString = xmlAsString + "\r\n\t\t<MAI>" + self.beneficiary_email() + "</MAI>";
		xmlAsString = xmlAsString + "\r\n\t</BEN>";
		
		xmlAsString = xmlAsString + "\r\n\t<GDS>";
		xmlAsString = xmlAsString + "\r\n\t\t<GEN>";
		xmlAsString = xmlAsString + "\r\n\t\t\t<DSC>" + self.general_description() + "</DSC>";
		xmlAsString = xmlAsString + "\r\n\t\t\t<TIT>" + self.no_of_goods() + "</TIT>";
		xmlAsString = xmlAsString + "\r\n\t\t\t<WGT>" + self.net_weight() + "</WGT>";
		xmlAsString = xmlAsString + "\r\n\t\t</GEN>";
		
		xmlAsString = xmlAsString + "\r\n\t\t<VAL>";
		xmlAsString = xmlAsString + "\r\n\t\t\t<FOB>" + self.total_fob_value() + "</FOB>";
		xmlAsString = xmlAsString + "\r\n\t\t\t<FRG>" + self.total_freight_charge() + "</FRG>";
		xmlAsString = xmlAsString + "\r\n\t\t\t<TAC>" + self.ancilary_charge() + "</TAC>";
		xmlAsString = xmlAsString + "\r\n\t\t\t<CUR>";
		xmlAsString = xmlAsString + "\r\n\t\t\t\t<COD>" + self.currency_code() + "</COD>";
		xmlAsString = xmlAsString + "\r\n\t\t\t</CUR>";
		xmlAsString = xmlAsString + "\r\n\t\t\t<EXC>" + self.exchange_rate() + "</EXC>";
		xmlAsString = xmlAsString + "\r\n\t\t\t<INC>" + self.insurance_value() + "</INC>";
		xmlAsString = xmlAsString + "\r\n\t\t\t<TCF>";
		xmlAsString = xmlAsString + "\r\n\t\t\t\t<VAL>" + self.total_cf_value() + "</VAL>";
		xmlAsString = xmlAsString + "\r\n\t\t\t</TCF>";
		xmlAsString = xmlAsString + "\r\n\t\t\t<SRC>" + self.fund_source() + "</SRC>";
		xmlAsString = xmlAsString + "\r\n\t\t</VAL>";
		
		xmlAsString = xmlAsString + "\r\n\t\t<PRO>";
		xmlAsString = xmlAsString + "\r\n\t\t\t<INV>" + self.proforma_invoice_no() + "</INV>";
		xmlAsString = xmlAsString + "\r\n\t\t\t<DAT>" + self.proforma_invoice_date() + "</DAT>";
		xmlAsString = xmlAsString + "\r\n\t\t</PRO>";
		
		xmlAsString = xmlAsString + "\r\n\t\t<MOP>";
		xmlAsString = xmlAsString + "\r\n\t\t\t<COD>" + self.payment_mode() + "</COD>";
		xmlAsString = xmlAsString + "\r\n\t\t\t<DAT>" + self.payment_date() + "</DAT>";
		xmlAsString = xmlAsString + "\r\n\t\t</MOP>";
		
		xmlAsString = xmlAsString + "\r\n\t\t<TRF>";
		xmlAsString = xmlAsString + "\r\n\t\t\t<COD>" + self.transfer_mode() + "</COD>";
		xmlAsString = xmlAsString + "\r\n\t\t</TRF>";
		
		xmlAsString = xmlAsString + "\r\n\t\t<MOT>";
		xmlAsString = xmlAsString + "\r\n\t\t\t<COD>" + self.shipment_mode() + "</COD>";
		xmlAsString = xmlAsString + "\r\n\t\t</MOT>";
		
		xmlAsString = xmlAsString + "\r\n\t\t<DIS>";
		xmlAsString = xmlAsString + "\r\n\t\t\t<COD>" + self.actual_port_discharge() + "</COD>";
		xmlAsString = xmlAsString + "\r\n\t\t</DIS>";
		
		xmlAsString = xmlAsString + "\r\n\t\t<DST>";
		xmlAsString = xmlAsString + "\r\n\t\t\t<COD>" + self.actual_port_loading() + "</COD>";
		xmlAsString = xmlAsString + "\r\n\t\t</DST>";
		
		xmlAsString = xmlAsString + "\r\n\t\t<CUO>";
		xmlAsString = xmlAsString + "\r\n\t\t\t<COD>" + self.customs_office() + "</COD>";
		xmlAsString = xmlAsString + "\r\n\t\t</CUO>";
		
		xmlAsString = xmlAsString + "\r\n\t\t<CTY>";
		xmlAsString = xmlAsString + "\r\n\t\t\t<ORG>";
		xmlAsString = xmlAsString + "\r\n\t\t\t\t<COD>" + self.country_of_origin() + "</COD>";
		xmlAsString = xmlAsString + "\r\n\t\t\t</ORG>";
		xmlAsString = xmlAsString + "\r\n\t\t\t<SUP>";
		xmlAsString = xmlAsString + "\r\n\t\t\t\t<COD>" + self.country_of_supply() + "</COD>";
		xmlAsString = xmlAsString + "\r\n\t\t\t</SUP>";
		xmlAsString = xmlAsString + "\r\n\t\t</CTY>";
		
		xmlAsString = xmlAsString + "\r\n\t\t<TOD>";
		xmlAsString = xmlAsString + "\r\n\t\t\t<COD>" + self.delivery_term() + "</COD>";
		xmlAsString = xmlAsString + "\r\n\t\t</TOD>";
		
		xmlAsString = xmlAsString + "\r\n\t</GDS>";
		xmlAsString = xmlAsString + "\r\n\t<TSP></TSP>";
		
		xmlAsString = xmlAsString + "\r\n\t<BNK>";
		xmlAsString = xmlAsString + "\r\n\t\t<COD>" + "214" + "</COD>";
		xmlAsString = xmlAsString + "\r\n\t</BNK>";
		
		xmlAsString = xmlAsString + "\r\n\t<SCA>";
		xmlAsString = xmlAsString + "\r\n\t\t<COD>" + (self.inspection_agent() == null ? "" : self.inspection_agent()) + "</COD>";
		xmlAsString = xmlAsString + "\r\n\t</SCA>";
		
		xmlAsString = xmlAsString + self.goodsXML();
		
		xmlAsString = xmlAsString + "\r\n\t<VAL>";
		xmlAsString = xmlAsString + "\r\n\t\t<DAT>" + (self.validity_end_date() == null ? "" : self.validity_end_date()) + "</DAT>";
		xmlAsString = xmlAsString + "\r\n\t</VAL>";
		
		//end tag and val (validity) tag not required
		xmlAsString = xmlAsString + "\r\n</" + (self.formtype() == "FORM M" ? "FORMM" : "") + ">";
		self.fullXML(xmlAsString);
		console.log(xmlAsString);
	  };
	  
	  
	  self.getFormDetails();
	  
      // Below are a subset of the ViewModel methods invoked by the ojModule binding
      // Please reference the ojModule jsDoc for additional available methods.

      /**
       * Optional ViewModel method invoked when this ViewModel is about to be
       * used for the View transition.  The application can put data fetch logic
       * here that can return a Promise which will delay the handleAttached function
       * call below until the Promise is resolved.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @return {Promise|undefined} - If the callback returns a Promise, the next phase (attaching DOM) will be delayed until
       * the promise is resolved
       */
      self.handleActivated = function(info) {
        
      };

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {boolean} info.fromCache - A boolean indicating whether the module was retrieved from cache.
       */
      self.handleAttached = function(info) {
        // Implement if needed
		
      };


      /**
       * Optional ViewModel method invoked after the bindings are applied on this View. 
       * If the current View is retrieved from cache, the bindings will not be re-applied
       * and this callback will not be invoked.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       */
      self.handleBindingsApplied = function(info) {
        // Implement if needed
      };

      /*
       * Optional ViewModel method invoked after the View is removed from the
       * document DOM.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {Array} info.cachedNodes - An Array containing cached nodes for the View if the cache is enabled.
       */
      self.handleDetached = function(info) {
        // Implement if needed
      };
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    //return new FormMPendingListViewModel();
	var fmM = new FormViewModel();
	$(document).ready
    (
		function()
		{
			console.log("-------event when document is ready");
			var actionType = $("#actionType").val();
			if(actionType == "c"){
				$("#cbnDv").show();
			}
			if(actionType == "p"){
				$("#cbnDv").hide();
			}
			if(actionType == "v"){
				$("#cbnDv").hide();
				$("#dvButton").hide();
			}
			//var formMTable = document.getElementById('formMList');
			//ko.applyBindings(fmM, formMTable);
			//formMTable.addEventListener('currentRowChanged', fmM.currentRowListener);
			
			/*
			
			
			table.addEventListener('ojBeforeCurrentRow', vm.beforeCurrentRowListener);
			table.addEventListener('ojSort', vm.sortListener);
			table.addEventListener('selectionChanged', vm.selectionListener);*/
		}
	);
	return fmM;
  }
);
