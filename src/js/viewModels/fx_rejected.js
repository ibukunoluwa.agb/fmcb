/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your about ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
		'ojs/ojbutton', 'ojs/ojrouter', 'ojs/ojlabel', 'messages', 'ojs/ojarraydataprovider', 'ojs/ojtable',
		'ojs/ojmessages', 'ojs/ojmessage'],
 function(oj, ko, $) {
  
	function getParameterByName(name, url) {
		if (!url) url = window.location.href;
		name = name.replace(/[\[\]]/g, '\\$&');
		var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, ' '));
	}
	function encode(str){
		return btoa(str);
	}
	function decode(str){
		return atob(str);
	}
	
    function FXApprovalViewModel() {
      var self = this;
	  self.rootModel = ko.dataFor(document.getElementById('globalBody'));
	  
	  self.formStatus = ko.observable("REJECTED");
	  self.inputer = ko.observable(sessionStorage.getItem("userId"));
	  
	  self.selectedRowKey = ko.observable("");	  
	  
	  self.navTo = "fx_approved_view";
	  self.formMArray = [];
	  self.formMObservableArray = ko.observableArray(self.formMArray);
	  //self.formMDataProvider = new oj.ArrayDataProvider(self.formMObservableArray, {keyAttributes: 'formid'});
	  self.formMDataProvider = new oj.ArrayTableDataSource(self.formMObservableArray, {idAttribute: 'requestId'});
	  self.paginDataSource = new oj.PagingTableDataSource(self.formMDataProvider);
	  
      console.log('----------**-==-**-------------'); 
	  
	  self.currentRowListener = function(event)
	  {
		  console.log("*******************");
		  var data = event.detail;
		  console.log(data);
		  if (event.type == 'currentRowChanged' && data['value'] != null)
		  {
			var rowIndex = data['value']['rowIndex'];
			var rowKey = data['value']['rowKey'];
			console.log("selected Row Key" + rowKey);
			self.selectedRowKey(rowKey);
			
			
		  }
      }; 
	  
	  
	  self.editButtonAction = function(event) 
	  {
		  console.log("*******edit button************" + self.selectedRowKey());
		  var data = event.detail;
		  console.log(data);
		  if (self.selectedRowKey() != "")
		  {
			
			window.location.replace('/?root=' + self.navTo + "&mode=" + encode("REJECTED") + "&req=" + self.selectedRowKey());
			
		  }else{
			   $("#status").html("<br/><em class='error-message'><img src='./images/error.png' width='24px' height='24' class='img-align'/>&nbsp;&nbsp;Select one of the Approved FX from the list above.</em><br/>&nbsp;<br/>");
		  }
      };
	
      self.getFXList = function(){
		console.log("in getting formM list " + self.formStatus());
		//URL to instantiate a process
		//https://129.156.113.190/ords/PDB1/fcmb/fx/request/approval/list/
		var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/fx/request/approval/list/";
		
		$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting form data.</em>");
		console.log("---->>>>>>>>>>>" + serviceURL);
		
		$.ajax ( {
			type: 'GET',
			url: serviceURL,
			cache: true,
			processData: false,
			contentType: false,
			crossDomain: true,
			xhrFields: { withCredentials: false },
			beforeSend: function (xhr) { 
				console.log('setting credentials.......');
				xhr.setRequestHeader ('Content-Type', 'application/json');  
				xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
				xhr.setRequestHeader ('status', self.formStatus());
				//xhr.setRequestHeader ('inputer', self.inputer());
			},
			success: function(data) { 
				
				console.log(JSON.stringify(data));
				
				/*
				select f.amount, f.application_number, f.form_m_number, f.formid, f.request_date, a.approvalid, a.requestid, a.check_validity, a.check_documentation, a.check_total_outstanding, a.check_fund_availability, a.status, a.action_by
from fx_request_approval a join fx_request f on a.requestid = f.requestid where status=:status
				*/
				var countedResult = data.items.length;
				var topData = [];
				for(i = 0; i < countedResult; i++){
					itemData = data.items[i];
					jsonData = itemData;//JSON.parse(JSON.stringify(itemData));
					//console.log("branch..." + data.items[i].branchname);
					var fxData = {
								 'formid': jsonData.formid,
								 'applicationNumber': jsonData.application_number,
								 'amount': jsonData.amount,
								 'formMNumber': jsonData.form_m_number,
								 'requestDate': jsonData.request_date,
								 'actionBy': jsonData.action_by,
								 'approvalId': jsonData.approvalid,
								 'requestId': jsonData.requestid
							  };
					self.formMArray.push(fxData);
					self.formMObservableArray.push(fxData);
					
				}
				var formMTable = document.getElementById('formMList');
				formMTable.refresh();
				$("#status").html("");
				//self.formMDataProvider(new oj.ArrayTableDataSource(self.formMArray, {idAttribute: 'formid'}));
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("=====sending error ========" + jqXHR.responseText);
				$("#status").html("<br/><em class='error-message'>Error getting existing Form M List. Please try again later</em>");
				//$("#status").html(jqXHR.responseText);
			}
		} );
		
		
	  };
	  self.getFXList();
	  
      // Below are a subset of the ViewModel methods invoked by the ojModule binding
      // Please reference the ojModule jsDoc for additional available methods.

      /**
       * Optional ViewModel method invoked when this ViewModel is about to be
       * used for the View transition.  The application can put data fetch logic
       * here that can return a Promise which will delay the handleAttached function
       * call below until the Promise is resolved.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @return {Promise|undefined} - If the callback returns a Promise, the next phase (attaching DOM) will be delayed until
       * the promise is resolved
       */
      self.handleActivated = function(info) {
        // Implement if needed
		sessionStorage.removeItem("mod_FormMID");
	    sessionStorage.removeItem("mod_Mode");
      };

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {boolean} info.fromCache - A boolean indicating whether the module was retrieved from cache.
       */
      self.handleAttached = function(info) {
        // Implement if needed
		
      };


      /**
       * Optional ViewModel method invoked after the bindings are applied on this View. 
       * If the current View is retrieved from cache, the bindings will not be re-applied
       * and this callback will not be invoked.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       */
      self.handleBindingsApplied = function(info) {
        // Implement if needed
      };

      /*
       * Optional ViewModel method invoked after the View is removed from the
       * document DOM.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {Array} info.cachedNodes - An Array containing cached nodes for the View if the cache is enabled.
       */
      self.handleDetached = function(info) {
        // Implement if needed
      };
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    //return new FormMPendingListViewModel();
	var fmM = new FXApprovalViewModel();
	$(document).ready
    (
		function()
		{
			console.log("-------event when document is ready");
			var formMTable = document.getElementById('formMList');
			//ko.applyBindings(fmM, formMTable);
			formMTable.addEventListener('currentRowChanged', fmM.currentRowListener);
			
			/*
			
			
			table.addEventListener('ojBeforeCurrentRow', vm.beforeCurrentRowListener);
			table.addEventListener('ojSort', vm.sortListener);
			table.addEventListener('selectionChanged', vm.selectionListener);*/
		}
	);
	return fmM;
  }
);
