/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojarraydataprovider', 'ojs/ojtable', 'ojs/ojprogress',
		'ojs/ojbutton', 'ojs/ojinputnumber', 'ojs/ojrouter', 'ojs/ojlabel', 'messages', 'ojs/ojswitch', 'ojs/ojfilepicker',
		'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
		'ojs/ojdatetimepicker', 'ojs/ojselectcombobox', 'ojs/ojtimezonedata', 'ojs/ojselectcombobox', 'ojs/ojtrain'],
 function(oj, ko, $, ArrayDataProvider) {
  
	function getParameterByName(name, url) {
		if (!url) url = window.location.href;
		name = name.replace(/[\[\]]/g, '\\$&');
		var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, ' '));
	}
	
    function FormMViewModel() {
      var self = this;
	  self.rootModel = ko.dataFor(document.getElementById('globalBody'));
	  self.navToError = "error";
	  self.formType = "FORM M";
	  
	  /*
	  self._sendFormType = getParameterByName("type");
	  if(self._sendFormType == null || self._sendFormType == ""){
		  window.location.replace('/?root=' + self.navToError + "&code=100");
		  return;
	  }else{
		  if(atob(self._sendFormType) == "M"){
			  self.formType = "FORM M";
		  }else{
			  //form not supported
			  window.location.replace('/?root=' + self.navToError + "&code=101");
			  return;
		  }
	  }
	  */
	  
	  self.sessionFromID = ko.observable(sessionStorage.getItem("mod_FormMID"));
	  self.modMode = ko.observable(sessionStorage.getItem("mod_Mode"));
	  
	  self.formMID = ko.observable("");
	  self.navTo = "success";
	  
	  if(self.modMode() == null && self.sessionFromID() == null){
		  self.modMode("NEW");
	  }
	  console.log(self.sessionFromID ()+ "<===========>" + self.modMode());
	  
	  function generateCID(){
			 
			var d = new Date();
			var multiplier = 3;
			var time = d.getTime(); 
			var year = d.getUTCFullYear();
			
			var guid =  "FORMM-" + (parseInt(multiplier) * parseInt(time));
			return guid;
			
	  };
	  function generateRandomValue(){
		var randomPassword = Math.random().toString(36).slice(-8);
		return randomPassword;
	  }
	  function getExtension(path) {
		var basename = path.split(/[\\/]/).pop(),  // extract file name from full path ... // (supports `\\` and `/` separators)
			pos = basename.lastIndexOf(".");       // get last position of `.`

		if (basename === "" || pos < 1)            // if file name is empty or ...
			return "";                             //  `.` not found (-1) or comes first (0)

		return basename.slice(pos + 1);            // extract extension ignoring `.`
	 }
	  
	  self.hsArray = [];
	  //self.hsObservableArray = ko.observableArray([]);
	  //self.hsTempObservableArray = ko.observableArray([]); //this will be sued to swap list
	  //self.hsDataProvider = new oj.ArrayDataProvider(self.hsObservableArray, {keyAttributes: 'hscode'});
	  
	  self.hsObservableArray = ko.observableArray(self.hsArray);
	  self.hscodeUploadDataProvider = new oj.ArrayTableDataSource(self.hsObservableArray, {idAttribute: 'hscode'});
	  self.uploadPaginDataSource = new oj.PagingTableDataSource(self.hscodeUploadDataProvider);
	  
	  self.emailPatternValue = ko.observable();
	  self.lastStep = ko.observable(0);
	  
	  //default values for dropdown
	  self.val = "";
	  self.paymentMode = "";
	  self.shipmentMode = "";
	  self.hscode = "";
	  self.currentCountry = "Nigeria";
	  this.checkedPlaceholder = ko.observable(false);
	  
	  //good info 
	  self.goodsQty = ko.observable(0);
	  self.unitPrice = ko.observable(0);
	  self.totalFOC = ko.observable(0);
	  self.fobValue = ko.computed(function() {
		  return parseFloat((self.goodsQty() * self.unitPrice()).toFixed(2));
	  }, this);
	  self.freightValue = ko.computed(function() {
		  return 0;//parseFloat((self.fobValue() * 0.0440314).toFixed(2));
	  }, this);
	  
	  //financials section for FOB and freightCharge
	  self.totalFOBValue = ko.observable(0);
	  self.ancilaryCharge = ko.observable(0);
	  self.totalFreightValue = ko.observable(0);
	  self.totalCFValue = ko.computed(function() {
		  return parseFloat((parseFloat(self.totalFOBValue()) + parseFloat(self.totalFreightValue()) + parseFloat(self.ancilaryCharge())).toFixed(2));
	  }, this);
	  
	  self.userName = ko.observable("");
	  self.isChecked = ko.observable("1");
	  self.isFreeChargeChecked = ko.observable(false);
	  
	  this.validityDate = ko.observable(oj.IntlConverterUtils.dateToLocalIso(new Date()));
	  this.branches = ko.observableArray([]);
	  this.hscodes = ko.observableArray([]);
	  
	  this.prefixList = ko.observableArray([
		  {value: 'BA', label: 'BA'},
		  {value: 'CB',  label: 'CB'}
		]);
	  self.lovCustomsOffices = ko.observableArray([]);
	  self.lovTransportModes = ko.observableArray([]);
	  self.lovPorts = ko.observableArray([]);
	  self.lovFundSources = ko.observableArray([]);
	  self.lovCurrencies =  ko.observableArray([]);
	  self.lovPaymentModes =  ko.observableArray([]);
	  self.lovTransferModes =  ko.observableArray([]);
	  self.lovDeliveryTerms =  ko.observableArray([]);
	  self.lovStateOfGoods =  ko.observableArray([]);
	  self.lovSectoralPurposes =  ko.observableArray([]);
	  self.lovStates =  ko.observableArray([]);
	  //self.lovCurrencies =  ko.observableArray([]);
	  self.lovCurrencies = ko.observableArray([
		  {code: 'USD', description: 'United State of America - USD'},
		  {code: 'GBP',  description: 'United Kingdom - Pounds'}
		]);
	  self.lovUnit = ko.observableArray([
		  {code: 'U', description: 'Unit'},
		  {code: '2U',  description: 'Pair'},
		  {code: 'CL',  description: 'Centilitre'},
		  {code: 'L',  description: 'Litre'}
		]); 
		
	  self.uploadedDocumentTypes = [];
	  this.docTypes = ko.observableArray([
		  {value: 'SONCAP', label: 'SONCAP'},
		  {value: 'NAFDAC',  label: 'NAFDAC'},
		  {value: 'PHCN',  label: 'PHCN'},
		  {value: 'INSURANCE',  label: 'Insurance'},
		  {value: 'Pro-Forma',  label: 'Pro-Forma'},
		  {value: 'BL',  label: 'BL'},
		  {value: 'Others',  label: 'Others'}
		]);
	  
	  self.converterValue = ko.observable("yyyy-MM-dd");
	  self.dateConverter = ko.observable(oj.Validation.converterFactory(oj.ConverterFactory.CONVERTER_TYPE_DATETIME).
        createConverter(
        {
          pattern : self.converterValue()
        }));
		
	  
	  self.countries = ko.observableArray([
		{"name": "Afghanistan", "code": "AF"},
		{"name": "Åland Islands", "code": "AX"},
		{"name": "Albania", "code": "AL"},
		{"name": "Algeria", "code": "DZ"},
		{"name": "American Samoa", "code": "AS"},
		{"name": "AndorrA", "code": "AD"},
		{"name": "Angola", "code": "AO"},
		{"name": "Anguilla", "code": "AI"},
		{"name": "Antarctica", "code": "AQ"},
		{"name": "Antigua and Barbuda", "code": "AG"},
		{"name": "Argentina", "code": "AR"},
		{"name": "Armenia", "code": "AM"},
		{"name": "Aruba", "code": "AW"},
		{"name": "Australia", "code": "AU"},
		{"name": "Austria", "code": "AT"},
		{"name": "Azerbaijan", "code": "AZ"},
		{"name": "Bahamas", "code": "BS"},
		{"name": "Bahrain", "code": "BH"},
		{"name": "Bangladesh", "code": "BD"},
		{"name": "Barbados", "code": "BB"},
		{"name": "Belarus", "code": "BY"},
		{"name": "Belgium", "code": "BE"},
		{"name": "Belize", "code": "BZ"},
		{"name": "Benin", "code": "BJ"},
		{"name": "Bermuda", "code": "BM"},
		{"name": "Bhutan", "code": "BT"},
		{"name": "Bolivia", "code": "BO"},
		{"name": "Bosnia and Herzegovina", "code": "BA"},
		{"name": "Botswana", "code": "BW"},
		{"name": "Bouvet Island", "code": "BV"},
		{"name": "Brazil", "code": "BR"},
		{"name": "British Indian Ocean Territory", "code": "IO"},
		{"name": "Brunei Darussalam", "code": "BN"},
		{"name": "Bulgaria", "code": "BG"},
		{"name": "Burkina Faso", "code": "BF"},
		{"name": "Burundi", "code": "BI"},
		{"name": "Cambodia", "code": "KH"},
		{"name": "Cameroon", "code": "CM"},
		{"name": "Canada", "code": "CA"},
		{"name": "Cape Verde", "code": "CV"},
		{"name": "Cayman Islands", "code": "KY"},
		{"name": "Central African Republic", "code": "CF"},
		{"name": "Chad", "code": "TD"},
		{"name": "Chile", "code": "CL"},
		{"name": "China", "code": "CN"},
		{"name": "Christmas Island", "code": "CX"},
		{"name": "Cocos (Keeling) Islands", "code": "CC"},
		{"name": "Colombia", "code": "CO"},
		{"name": "Comoros", "code": "KM"},
		{"name": "Congo", "code": "CG"},
		{"name": "Congo, The Democratic Republic of the", "code": "CD"},
		{"name": "Cook Islands", "code": "CK"},
		{"name": "Costa Rica", "code": "CR"},
		{"name": "Cote D'Ivoire", "code": "CI"},
		{"name": "Croatia", "code": "HR"},
		{"name": "Cuba", "code": "CU"},
		{"name": "Cyprus", "code": "CY"},
		{"name": "Czech Republic", "code": "CZ"},
		{"name": "Denmark", "code": "DK"},
		{"name": "Djibouti", "code": "DJ"},
		{"name": "Dominica", "code": "DM"},
		{"name": "Dominican Republic", "code": "DO"},
		{"name": "Ecuador", "code": "EC"},
		{"name": "Egypt", "code": "EG"},
		{"name": "El Salvador", "code": "SV"},
		{"name": "Equatorial Guinea", "code": "GQ"},
		{"name": "Eritrea", "code": "ER"},
		{"name": "Estonia", "code": "EE"},
		{"name": "Ethiopia", "code": "ET"},
		{"name": "Falkland Islands (Malvinas)", "code": "FK"},
		{"name": "Faroe Islands", "code": "FO"},
		{"name": "Fiji", "code": "FJ"},
		{"name": "Finland", "code": "FI"},
		{"name": "France", "code": "FR"},
		{"name": "French Guiana", "code": "GF"},
		{"name": "French Polynesia", "code": "PF"},
		{"name": "French Southern Territories", "code": "TF"},
		{"name": "Gabon", "code": "GA"},
		{"name": "Gambia", "code": "GM"},
		{"name": "Georgia", "code": "GE"},
		{"name": "Germany", "code": "DE"},
		{"name": "Ghana", "code": "GH"},
		{"name": "Gibraltar", "code": "GI"},
		{"name": "Greece", "code": "GR"},
		{"name": "Greenland", "code": "GL"},
		{"name": "Grenada", "code": "GD"},
		{"name": "Guadeloupe", "code": "GP"},
		{"name": "Guam", "code": "GU"},
		{"name": "Guatemala", "code": "GT"},
		{"name": "Guernsey", "code": "GG"},
		{"name": "Guinea", "code": "GN"},
		{"name": "Guinea-Bissau", "code": "GW"},
		{"name": "Guyana", "code": "GY"},
		{"name": "Haiti", "code": "HT"},
		{"name": "Heard Island and Mcdonald Islands", "code": "HM"},
		{"name": "Holy See (Vatican City State)", "code": "VA"},
		{"name": "Honduras", "code": "HN"},
		{"name": "Hong Kong", "code": "HK"},
		{"name": "Hungary", "code": "HU"},
		{"name": "Iceland", "code": "IS"},
		{"name": "India", "code": "IN"},
		{"name": "Indonesia", "code": "ID"},
		{"name": "Iran, Islamic Republic Of", "code": "IR"},
		{"name": "Iraq", "code": "IQ"},
		{"name": "Ireland", "code": "IE"},
		{"name": "Isle of Man", "code": "IM"},
		{"name": "Israel", "code": "IL"},
		{"name": "Italy", "code": "IT"},
		{"name": "Jamaica", "code": "JM"},
		{"name": "Japan", "code": "JP"},
		{"name": "Jersey", "code": "JE"},
		{"name": "Jordan", "code": "JO"},
		{"name": "Kazakhstan", "code": "KZ"},
		{"name": "Kenya", "code": "KE"},
		{"name": "Kiribati", "code": "KI"},
		{"name": "Korea, Democratic People'S Republic of", "code": "KP"},
		{"name": "Korea, Republic of", "code": "KR"},
		{"name": "Kuwait", "code": "KW"},
		{"name": "Kyrgyzstan", "code": "KG"},
		{"name": "Lao People'S Democratic Republic", "code": "LA"},
		{"name": "Latvia", "code": "LV"},
		{"name": "Lebanon", "code": "LB"},
		{"name": "Lesotho", "code": "LS"},
		{"name": "Liberia", "code": "LR"},
		{"name": "Libyan Arab Jamahiriya", "code": "LY"},
		{"name": "Liechtenstein", "code": "LI"},
		{"name": "Lithuania", "code": "LT"},
		{"name": "Luxembourg", "code": "LU"},
		{"name": "Macao", "code": "MO"},
		{"name": "Macedonia, The Former Yugoslav Republic of", "code": "MK"},
		{"name": "Madagascar", "code": "MG"},
		{"name": "Malawi", "code": "MW"},
		{"name": "Malaysia", "code": "MY"},
		{"name": "Maldives", "code": "MV"},
		{"name": "Mali", "code": "ML"},
		{"name": "Malta", "code": "MT"},
		{"name": "Marshall Islands", "code": "MH"},
		{"name": "Martinique", "code": "MQ"},
		{"name": "Mauritania", "code": "MR"},
		{"name": "Mauritius", "code": "MU"},
		{"name": "Mayotte", "code": "YT"},
		{"name": "Mexico", "code": "MX"},
		{"name": "Micronesia, Federated States of", "code": "FM"},
		{"name": "Moldova, Republic of", "code": "MD"},
		{"name": "Monaco", "code": "MC"},
		{"name": "Mongolia", "code": "MN"},
		{"name": "Montserrat", "code": "MS"},
		{"name": "Morocco", "code": "MA"},
		{"name": "Mozambique", "code": "MZ"},
		{"name": "Myanmar", "code": "MM"},
		{"name": "Namibia", "code": "NA"},
		{"name": "Nauru", "code": "NR"},
		{"name": "Nepal", "code": "NP"},
		{"name": "Netherlands", "code": "NL"},
		{"name": "Netherlands Antilles", "code": "AN"},
		{"name": "New Caledonia", "code": "NC"},
		{"name": "New Zealand", "code": "NZ"},
		{"name": "Nicaragua", "code": "NI"},
		{"name": "Niger", "code": "NE"},
		{"name": "Nigeria", "code": "NG"},
		{"name": "Niue", "code": "NU"},
		{"name": "Norfolk Island", "code": "NF"},
		{"name": "Northern Mariana Islands", "code": "MP"},
		{"name": "Norway", "code": "NO"},
		{"name": "Oman", "code": "OM"},
		{"name": "Pakistan", "code": "PK"},
		{"name": "Palau", "code": "PW"},
		{"name": "Palestinian Territory, Occupied", "code": "PS"},
		{"name": "Panama", "code": "PA"},
		{"name": "Papua New Guinea", "code": "PG"},
		{"name": "Paraguay", "code": "PY"},
		{"name": "Peru", "code": "PE"},
		{"name": "Philippines", "code": "PH"},
		{"name": "Pitcairn", "code": "PN"},
		{"name": "Poland", "code": "PL"},
		{"name": "Portugal", "code": "PT"},
		{"name": "Puerto Rico", "code": "PR"},
		{"name": "Qatar", "code": "QA"},
		{"name": "Reunion", "code": "RE"},
		{"name": "Romania", "code": "RO"},
		{"name": "Russian Federation", "code": "RU"},
		{"name": "RWANDA", "code": "RW"},
		{"name": "Saint Helena", "code": "SH"},
		{"name": "Saint Kitts and Nevis", "code": "KN"},
		{"name": "Saint Lucia", "code": "LC"},
		{"name": "Saint Pierre and Miquelon", "code": "PM"},
		{"name": "Saint Vincent and the Grenadines", "code": "VC"},
		{"name": "Samoa", "code": "WS"},
		{"name": "San Marino", "code": "SM"},
		{"name": "Sao Tome and Principe", "code": "ST"},
		{"name": "Saudi Arabia", "code": "SA"},
		{"name": "Senegal", "code": "SN"},
		{"name": "Serbia and Montenegro", "code": "CS"},
		{"name": "Seychelles", "code": "SC"},
		{"name": "Sierra Leone", "code": "SL"},
		{"name": "Singapore", "code": "SG"},
		{"name": "Slovakia", "code": "SK"},
		{"name": "Slovenia", "code": "SI"},
		{"name": "Solomon Islands", "code": "SB"},
		{"name": "Somalia", "code": "SO"},
		{"name": "South Africa", "code": "ZA"},
		{"name": "South Georgia and the South Sandwich Islands", "code": "GS"},
		{"name": "Spain", "code": "ES"},
		{"name": "Sri Lanka", "code": "LK"},
		{"name": "Sudan", "code": "SD"},
		{"name": "Suriname", "code": "SR"},
		{"name": "Svalbard and Jan Mayen", "code": "SJ"},
		{"name": "Swaziland", "code": "SZ"},
		{"name": "Sweden", "code": "SE"},
		{"name": "Switzerland", "code": "CH"},
		{"name": "Syrian Arab Republic", "code": "SY"},
		{"name": "Taiwan, Province of China", "code": "TW"},
		{"name": "Tajikistan", "code": "TJ"},
		{"name": "Tanzania, United Republic of", "code": "TZ"},
		{"name": "Thailand", "code": "TH"},
		{"name": "Timor-Leste", "code": "TL"},
		{"name": "Togo", "code": "TG"},
		{"name": "Tokelau", "code": "TK"},
		{"name": "Tonga", "code": "TO"},
		{"name": "Trinidad and Tobago", "code": "TT"},
		{"name": "Tunisia", "code": "TN"},
		{"name": "Turkey", "code": "TR"},
		{"name": "Turkmenistan", "code": "TM"},
		{"name": "Turks and Caicos Islands", "code": "TC"},
		{"name": "Tuvalu", "code": "TV"},
		{"name": "Uganda", "code": "UG"},
		{"name": "Ukraine", "code": "UA"},
		{"name": "United Arab Emirates", "code": "AE"},
		{"name": "United Kingdom", "code": "GB"},
		{"name": "United States", "code": "US"},
		{"name": "United States Minor Outlying Islands", "code": "UM"},
		{"name": "Uruguay", "code": "UY"},
		{"name": "Uzbekistan", "code": "UZ"},
		{"name": "Vanuatu", "code": "VU"},
		{"name": "Venezuela", "code": "VE"},
		{"name": "Viet Nam", "code": "VN"},
		{"name": "Virgin Islands, British", "code": "VG"},
		{"name": "Virgin Islands, U.S.", "code": "VI"},
		{"name": "Wallis and Futuna", "code": "WF"},
		{"name": "Western Sahara", "code": "EH"},
		{"name": "Yemen", "code": "YE"},
		{"name": "Zambia", "code": "ZM"},
		{"name": "Zimbabwe", "code": "ZW"}
	]);
	  
	  //steps processing and settings
	  self.previousStep = ko.observable(0);
	  self.previousStepObj = ko.observable();
	  self.isSaved = ko.observable(0);
      self.selectedStepValue = ko.observable('step0');
      this.stepArray =
        ko.observableArray(
                [{label:'Header', id:'step0'},
				 {label:'Applicant Details', id:'step1'},
                 {label:'Beneficiary Details', id:'step2'},
                 {label:'Transportation', id:'step3'},
                 {label:'Financials', id:'step4'},
                 {label:'Goods', id:'step5'},
                 {label:'Attachment', id:'step6'},
                 {label:'End', id:'step7'}]);
		
	  this.deselectStepAction = function(event) {
		  
			//console.log("deselect step.....===>" + JSON.stringify(event.detail));
			//self.previousStepObj(event.detail['fromStep']);
	  };
	  this.updateBeforeStepsAction = function(event) {
			//console.log("update before step.....===>" + JSON.stringify(event.detail));
			//self.previousStepObj(event.detail['fromStep']);
	  };
	  this.updateStepsAction = function(event) {
		  //console.log("update before step.....===>" + JSON.stringify(event.detail));
		  var toStep = event.detail['toStep'];
		  if(toStep.visited == true){
				self.processStepDisplayAction(toStep, true);
		  }
	  };
	  
	  this.defaultNextStep = function(event) {
		 var train = document.getElementById("train");
		 
		 var toStepId = train.getNextSelectableStep();
		 var toStep = train.getStep(toStepId);
		 self.processStepDisplayAction(toStep, true);
	  }
	  
	  this.attachmentNextStep = function(event) {
		 //check the type of attachments 
		 var hasProforma = false;
		 var hasInsurance = false;
		 var counted = self.uploadedDocumentTypes.length;
		 
		 for(i = 0; i < counted; i++){
			 var _tmpType = self.uploadedDocumentTypes[i];
			 
			 if(_tmpType == "Pro-Forma"){
				 hasProforma = true;
			 }                        
			 if(_tmpType == "INSURANCE"){
				 hasInsurance = true;
			 }
		 }
		 $("#dstatus").html("");
		 if(hasProforma == false){
			$("#status").html("<br/><em class='error-message'>There is no attached pro-forma invoice. Upload and save scanned pro-forma invoice.</em>");
			$("#dstatus").html($("#status").html());
			return; 
		 }
		 if(hasInsurance == false){
			$("#status").html("<br/><em class='error-message'>There is no attached insurance document. Upload and save scanned insurance document.</em>");
			$("#dstatus").html($("#status").html());
			return; 
		 }
		 
		 var train = document.getElementById("train");
		 
		 var toStepId = train.getNextSelectableStep();
		 var toStep = train.getStep(toStepId);
		 self.processStepDisplayAction(toStep, true);
	  }
	  /**
	  This is the action button for moving from step 1 to 4
	  */
	  
      this.nextStepsAction = function(event) {
         var train = document.getElementById("train");
		 //console.log("update before step.....===>" + JSON.stringify(event.detail));
		 //var toStep = event.detail['toStep'];
		 var toStepId = train.getNextSelectableStep();
		 var toStep = train.getStep(toStepId);
		 
		 var selectedStep = toStep.id.substr(toStep.id.length - 1, toStep.id.length);
		 
		 if(self.previousStep() == selectedStep && (self.isSaved() == 0)){
			 console.log('step is the same... quit now');
			 return;
		 }
		 //console.log('...previous step:' + self.previousStep());
		 //console.log("selected step.....===>" + JSON.stringify(toStep));
		 //console.log("train..."+ train);
         //console.log(train.getStep(event.detail.value).label);
		 
		 //process form and set form ID
		 self.saveStepRequest(self.previousStep(), toStep);
		 
		 //console.log("now hiding step 0" + event.detail.value.substr(event.detail.value.length - 1, event.detail.value.length));
      };
	  
	  this.previousStepsAction = function(event) {
         var train = document.getElementById("train");
		 
		 var toStepId = train.getPreviousSelectableStep();
		 var toStep = train.getStep(toStepId);
		 self.processStepDisplayAction(toStep, true);
      };
	  
	  self.processStepDisplayAction = function(toStep, skipCheck){
		 //console.log('In processStepDisplayAction event ---');
		 //console.log(self.isSaved() + '*******************************');
		 var train = document.getElementById("train");
		 if(skipCheck == false){
			 if(self.isSaved() == 0){
				 $("#status").html("<em class='error-message'>Unable to save information on " + toStep.label + "</em>");
				 return;
			 }
		 }
		 var tmpStepName = 'step' + self.previousStep();
		 self.selectedStepValue(toStep.id);
		 train.updateStep(toStep.id, toStep);
		 train.refresh();
		 $("#status").html("");
		 
		 console.log('.... ready to hide and show component for ' + toStep.id);
		 $( "#dvstep0").hide('slow');
		 $( "#dvstep1").hide('slow');
		 $( "#dvstep2").hide('slow');
		 $( "#dvstep3").hide('slow');
		 $( "#dvstep4").hide('slow');
		 $( "#dvstep5").hide('slow');
		 $( "#dvstep6").hide('slow');
		 
		 //$( "#dvstep0").css("display","none");
		 $("#dv" + toStep.id).show('slow');
		 var _selectedStepNo = toStep.id.substr(toStep.id.length - 1, toStep.id.length);
		 //console.log("Now showing this as my previous step" + _selectedStepNo);
		 self.previousStep(_selectedStepNo);
		 //reset isSaved back
		 self.isSaved(0);
	  }
	  
	  self.addNewItemAction = function(event){
		  $( "#dvItem").show('slow');
		  $( "#dvItemList").hide('slow');
		  $( "#cmdNewItem").hide('slow');
		  
	  }
	  self.cancelNewItemAction = function(event){
		  $( "#dvItemList").show('slow');
		  $( "#cmdNewItem").show('slow');
		   $( "#dvItem").hide('slow');
		  
	  }
	  
	  //file input settings
	  self.fileName = "";
	  self.fileType = '';
	  self.fileBlob = "";
	  self.fileExtension = "";
	  self.fileNames = ko.observableArray([]);
	  self.progressValue = ko.observable(0);
	  
	  
	  self.attachmentArray = [];
	  self.attachmentObservableArray = ko.observableArray(self.attachmentArray);
	  self.attachmentDataProvider = new oj.ArrayDataProvider(self.attachmentObservableArray, {keyAttributes: 'documentName'});

	  self.selectListener = function(event) {
        var files = event.detail.files;
        //we can pick more than one file but lets focus on just one and the first one
        for (var i = 0; i < 1/*files.length*/; i++) {
          
          var file = files[i];
          
          self.fileType = files[i].type;
          self.fileName = files[i].name;
          self.fileExtension = getExtension(self.fileName);
          //console.log('Extension... ' + getExtension(self.fileName));
          
		  var reader = new FileReader();
		  //read the file by using our method - readFile
		  reader.addEventListener('load', self.readFile);
		  
		  //try this out later
		  //reader.addEventListener('progress', self.showProgress);
		  //reader.addEventListener('loadend', self.showLoadEnd);
		  
		  //console.log(self.fileName + 'file type...' + files[i].type);
		  //reader.readAsBinaryString(file);
		  reader.readAsArrayBuffer(file);
		  //reader.readAsBinaryString(blob);
		  
		  //self.fileNames.push(files[i].name);
          
        }
      }
      
      self.readFile = function(event){
			//console.log('reading file..............');
			//console.log(event.target.result);
			var arrayBuffer = event.target.result;
			//console.log('****2****' + self.fileType);
			
			var blob = new Blob([arrayBuffer]);
			
			//console.log('******we****' + blob);
			if(self.fileName == ""){
				console.log('....No file name specified in request');
			}else{
				//do nothing but assign values required to self
				self.fileBlob = blob;
				$("#selectedFile").html("<b>File Selected: " + self.fileName + "</b>");
			
			}
	  };
	  
	  self.saveAttachments = function(event){
		  //console.log("****************** this is time to save attachment....");
		  
		  var _documentType = $("#cbDocType").val();
		  var annotation = $("#txtAnnotation").val();
		  self.uploadToDocumentService(self.fileBlob, self.fileName, self.fileType, self.fileExtension, _documentType, annotation);
	  };
	  
	  
	  /**
	   * uploadToDocumentService - The method is used to upload content to the DOCS. In this situation, we are using an intermediary because of restriction 
	   * on the browsers for CORS. The method will set timeout and the file.
	   * 
	   */ 
	  self.uploadToDocumentService = function(blob, fileName, mimeType, fileExtension, documentType, annotation){
			
			//var DoCSinstance = "https://documents-gse00013612.documents.us2.oraclecloud.com";
			//var docsUrl = DoCSinstance + '/documents/api/1.2';
			var docsUrl = self.rootModel.DOCSUrl + "/files/data";
			var parentId = self.rootModel.DOCSParentId;
			
			var strFileName = fileName;
			//console.log("OLD file Name---------->>>" + strFileName);
			//we need to change the file name
			try{
				strFileName = sessionStorage.getItem("userAlias") + '-' + generateRandomValue() + '.' + fileExtension;
			}catch(Ex){
				//do nothing and use the default name
			}
			//console.log("NEW file Name---------->>>" + strFileName);
			
			$("#status").html("<br/><img src='./images/spinner.gif' class='img-align' width='42' height'42'/>&nbsp;<em class='success-message'>Please wait, uploading attached file to the server.</em>");
			
			var fileContent = blob;//new Blob([editor1.value], { type: 'text/plain'});;
			var contentData = new FormData();
			contentData.append("parentID", parentId);
			contentData.append('primaryFile',fileContent, strFileName);
			//console.log(docsUrl + '....about to start request to save file ...' + JSON.stringify(contentData));
			
			//console.log('Ready to submit Form data... ');
			$.ajax({
				url: docsUrl,
				data: contentData,
				cache: false,
				contentType: false,
				processData: false,
				timeout: 960000, 
				method: 'POST',
				type: 'POST', // For jQuery < 1.9
				beforeSend: function (xhr) { 
					console.log('setting credentials.......'); 
					//xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');  
					//xhr.setRequestHeader("Connection", "Keep-Alive");
					//xhr.setRequestHeader("Content-Type","multipart/form-data;"); 
					
				},
				
				xhr: function()
				{
					var xhr = new window.XMLHttpRequest();
					//Upload progress
					xhr.upload.addEventListener("progress", function(evt){
					  if (evt.lengthComputable) {
						var percentComplete = evt.loaded / evt.total;
						//Do something with upload progress
						console.log('Total Uploaded in byte...' + percentComplete);
						percentageValue = parseFloat(percentComplete) * 100;
						self.progressValue(parseFloat(percentageValue));
					  }
					}, false);
					//Download progress
					xhr.addEventListener("progress", function(evt){
					  if (evt.lengthComputable) {
						var percentComplete = evt.loaded / evt.total;
						//Do something with download progress
						//console.log(percentComplete);
					  }
					}, false);
					return xhr;
				},
				success: function(data){
					//console.log("=====File saved/uploaded ========" + JSON.stringify(data));
					$("#status").html("<em class='success-message'><br/>Content Saved Successfully</em>");
					//get UserID from session ... 
					var userId = sessionStorage.getItem("userId");
					
					var x_MimeType = data.mimeType;
					var x_FileSize = data.size;
					var documentId = data.id; 
					linkName = generateRandomValue() + "-" + documentId;
					//console.log('------------after getting info--------------' + x_MimeType);
					
					self.createPublicLink(documentId, linkName, fileName, x_FileSize, x_MimeType, documentType, annotation);
				},
				start: function(data){
					console.log('starting ajax call to server');
				},
				complete: function(xhr, status){
					console.log(JSON.stringify(xhr) + '--->>>>>>>>>>In My Complete' + status);
					if(status == 'timeout'){
						$("#status").html("<br/><img src='./images/danger.png' class='img-align' width='42' height'42'/><em class='error-message'>Unable to upload file due to time out. Ensure your internet connection is good!</em>");
					}
				},
				error: function(jqXHR, textStatus, errorThrown) { 
					console.log(textStatus + "=====uploading system error ========" + errorThrown);
					$("#status").html("<br/><em  class='error-message'>ErrorMessage: " + errorThrown + "</em>");
					console.log(jqXHR.statusText);
					if(status == 'timeout'){
						$("#status").html("<br/><img src='./images/danger.png' class='img-align' width='42' height'42'/><em class='error-message'>Unable to upload file due to time out. Ensure your internet connection is good!</em>");
					}
				}
			});

			
	  };//end upload
	  
	  /**
	   * createPublicLink - This method calls the service to create a public shared link on the file uploaded.
	   * This will enable everyone to access the video/file remotely without credentials
	   * The key values are the linkName and document ID. The other parameters will be used by other functions
	   * */
	  self.createPublicLink = function(documentId, linkName, fileName, x_FileSize, x_MimeType, documentType, annotation){
			console.log("*******In PUBLIC LINK SECTION");
			
			var DoCSinstance = self.rootModel.DOCSUrl;//"https://documents-gse00013612.documents.us2.oraclecloud.com";
			//var docsUrl = DoCSinstance + '/publiclinks/file/' + documentId;
			var docsUrl = self.rootModel.DOCSUrl + "/publiclinks/file/" + documentId;
			var parentId = self.rootModel.DOCSParentId;
			//console.log(docsUrl);
			
			$("#status").html("<br/><img src='./images/spinner.gif' class='img-align' width='42' height'42'/>&nbsp;<em class='success-message'>Creating Public Link, Please wait...</em>");
			
			
			var contentData = new FormData();
			contentData.append("assignedUsers", "@everybody"); //"@serviceinstance";
			contentData.append("linkName", linkName);
			contentData.append("fileId", documentId);
			contentData.append("role", 'contributor'); 
			
			//console.log('....about to start request for public link ...');
			$.ajax ( {
				url: docsUrl,
				data: contentData,
				cache: false,
				contentType: false, 
				processData: false,
				method: 'POST',
				type: 'POST', // For jQuery < 1.9
				beforeSend: function (xhr) { 
					//console.log('setting credentials.......');
					//xhr.setRequestHeader ('Content-Type', 'application/json');  
					//xhr.setRequestHeader ('Access-Control-Allow-Origin', '*'); 
					//xhr.setRequestHeader ('Authorization', 
					//					  'Basic ' + btoa('john.dunbar:mesic@2IMpulse'));  
					//console.log('---- credential set ...... waiting for response');
				},
				success: function(data) { 
					
					//console.log("=====Link Created ========" + JSON.stringify(data));
					$("#status").html('<em class="success-message">Link successfully created... please wait</em>');
					
					var linkId = data.linkID;
					
					var _formID = self.formMID();
					//save to database
					self.saveAttachmentReference(_formID, fileName, documentId, parentId, documentType, linkId, linkName, annotation);
					
					//self.savePitchInformation(userId, fileName, title, companyName, businessCase, businessObjective, documentId, linkId, parentId, x_FileSize, x_MimeType, languageUsed);
					
				},
				error: function(jqXHR, textStatus, errorThrown) {
					//console.log("=====uploading system error ========" + errorThrown);
					$("#status").html('<br/><em class="error-message">System ErrorMessage: '+ errorThrown + '</em>');
					//$("#status").html(jqXHR.responseText);
				}
			} ); 
	  };//end public link
	  
	  self.saveAttachmentReference = function(formID, fileName, documentID, parentID, documentType, linkID, linkName, annotation){
			//URL to instantiate a process
			
			//console.log("+++++++++++++++++++++++++++++++" + formID);
			
			var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/formm/new/attachment";
			
			$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, Saving attachment...</em>");
			console.log(sessionStorage.getItem("userId") + "---->>>>>>>>>>>" + serviceURL);
			
			
			paramItem = {};
			paramItem["formid"] = formID;
			paramItem["file_name"] = fileName;
			paramItem["documentid"] = documentID;
			paramItem["parentid"] = parentID;
			paramItem["document_type"] = documentType;
			paramItem["linkid"] = linkID;
			paramItem["linkname"] = linkName;
			paramItem["annotation"] = annotation;
			
			console.log('....about to start request to send REST ...' + JSON.stringify(paramItem));
			
			$.ajax ( {
				type: 'POST',
				url: serviceURL,
				enctype: 'raw',
				data: JSON.stringify(paramItem),
				cache: false,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*'); 
				},
				success: function(data) { 
					//self.processUpdateRESTInfo(data);
					console.log('Saved successfully...' + data);
					//add details to grid object
					var attachmentData = {
								 'documentType': documentType,
								 'description': annotation,
								 'documentName': fileName
							  };
					
					self.attachmentObservableArray.push(attachmentData);
					self.uploadedDocumentTypes.push(documentType);
					$("#status").html("");
				},
				error: function(jqXHR, textStatus, errorThrown) {
					//console.log("=====sending error ========" + jqXHR.responseText);
					$("#status").html("<br/><em class='error-message'>Error while saving application attachment details. Please try again later</em>");
				}
			} ); 
			
	  };
	  
	  
	  
	self.saveItemAction = function(event){
		//console.log("************HSCode Clicked************");
		
		//var cbHscode = $("#cbHscode").val();
		var _cbHscode = document.getElementById("cbHscode");
		var cbHscode = _cbHscode.valueOption.value;
		var cbHscodeText = _cbHscode.valueOption.label;
		
		
		var chkFreeCharge = $("#chkFreeCharge").val();
		var txtDesc = $("#txtDesc").val();
		var cbGoodState = $("#cbGoodState").val();
		var cbGoodsOriginCountry = $("#cbGoodsOriginCountry").val();
		var cbSectoral = $("#cbSectoral").val();
		var txtPackage = $("#txtPackage").val();
		var cbPackageUnit = $("#cbPackageUnit").val();
		var txtNetWeight = $("#txtNetWeight").val();
		var txtQty = $("#txtQty").val();
		var cbMeasureUnit = $("#cbMeasureUnit").val();
		var txtUnitPrice = $("#txtUnitPrice").val();
		var txtGoodFOB = $("#txtGoodFOB").val();
		var txtGoodFCharge = $("#txtGoodFCharge").val();
		
		
		if(cbHscode == ""){
			return;
		}
		
		var exitActivity = false;
		//check if hscode already exist
		self.hsArray.forEach(function(item) {
			if(item.hscode == cbHscode){
				//alert('HSCode already exist in the list.');
				exitActivity = true;
				return;
			}
		});
		if(exitActivity == true){
			alert('HSCode already exist in the list.');
			return;
		}
		
		//calculate freight for this item
		//add to Array/ObservableArray and wait for the last step.
		var hscodeData = {
					 'hscode': cbHscode,
					 'description': txtDesc,
					 'stateOfGoods': cbGoodState,
					 'sectoralPurpose': cbSectoral,
					 'pkgQty.': txtPackage,
					 'pkgUnit.': cbPackageUnit,
					 'netWeight': txtNetWeight,
					 'masureQty': txtQty,
					 'measureUnit': cbMeasureUnit,
					 'unitPrice': txtUnitPrice,
					 'countryOfOrigin': cbGoodsOriginCountry,
					 'fobValue': txtGoodFOB,
					 'foc' : chkFreeCharge,
					 'freightCharge': "0"
				  };
		self.hsArray.push(hscodeData);
		self.hsObservableArray.push(hscodeData);
		
		var _totalFOB = txtGoodFOB;
		
		self.totalFOBValue(parseFloat(parseFloat(self.totalFOBValue()) + parseFloat(_totalFOB)));
		/*
		if(chkFreeCharge == true){
			self.totalFOC(parseFloat(parseFloat(self.totalFOC()) + parseFloat(_totalFOB)));
		}else{
			self.totalFOBValue(parseFloat(parseFloat(self.totalFOBValue()) + parseFloat(_totalFOB)));
		}
		*/
		//self.totalFreightValue(parseFloat(parseFloat(self.totalFreightValue()) + parseFloat(_totalFreight)));
		
		
		$("#status").html(""); 
		$( "#dvItemList").show('slow');
		$( "#cmdNewItem").show('slow');
		$( "#dvItem").hide('slow');
		
		
		
		//clear fields
		$("#chkFreeCharge").val("false");
		$("#txtDesc").val("");
		$("#cbGoodState").val("");
		$("#cbGoodsOriginCountry").val("");
		$("#cbSectoral").val("");
		$("#txtPackage").val("");
		$("#cbPackageUnit").val("");
		$("#txtNetWeight").val("");
		$("#txtQty").val("0");
		$("#cbMeasureUnit").val("");
		$("#txtUnitPrice").val("");
		$("#txtGoodFOB").val("");
		$("#txtGoodFCharge").val("");
		
		//console.log("existing added codes: " + self.hsArray);
		//self.saveNewItemRequest(self.formMID(), cbHscode, cbHscodeText, chkFreeCharge, txtDesc, cbGoodState, cbGoodsOriginCountry, cbSectoral, txtPackage, cbPackageUnit, txtNetWeight, txtQty, cbMeasureUnit, txtUnitPrice, txtGoodFOB, txtGoodFCharge);
		
		//window.location.replace("/");

		//console.log("***********processed*************");
		self.recalculateItemFreight();
		return true;
	  
	};
	  
	  
	self.recalculateItemFreight = function(){
		  var itemCount = self.hsObservableArray().length;
		  console.log("main array length------------->" + itemCount);
		  var _totalFOB = self.totalFOBValue();
		  var _totalFreight = self.totalFreightValue();
		  var percentage = parseFloat(_totalFreight)/parseFloat(_totalFOB);
		  
		  for(i = 0; i < itemCount; i++){
			  var item = self.hsObservableArray()[i];
			  var newItem = item;
			  
			  var itemFOB = newItem.fobValue;
			  var calcFreight = (parseFloat(itemFOB) * parseFloat(percentage)).toFixed(2);
			  newItem.freightCharge = calcFreight;
			  
			  if(newItem.foc == true){
				  self.totalFOC(parseFloat(parseFloat(self.totalFOC()) + parseFloat(calcFreight)))
			  }
			  console.log(calcFreight + "*******" + JSON.stringify(newItem));
			  self.hsObservableArray.replace(item, newItem);
			  
			  
		  }
		  //console.log(self.hsTempObservableArray().length + "new List------------->" + JSON.stringify(self.hsTempObservableArray()));
		  //var goodsTable = document.getElementById('savedGoodsTable');
		  //goodsTable.refresh();
	};
	 
	self.saveFinalApplicationItems = function(formID){
		  //self.completeApplicationForm(self.formMID());
		  var itemCount = self.hsObservableArray().length;
		  if(itemCount > 0){
			  var item = self.hsObservableArray()[0];
			  self.saveNewItemRequest(itemCount, 0, formID, item.hscode, "", item.foc, item.description, item.stateOfGoods, item.countryOfOrigin, item.sectoralPurpose, item.pkgQty, item.pkgUnit, item.netWeight, item.masureQty, item.measureUnit, item.unitPrice, item.fobValue, item.freightCharge);
		  }
	};
	
	self.saveNewItemRequest = function(itemCount, currentIndex, formID, cbHscode, cbHscodeText, chkFreeCharge, txtDesc, cbGoodState, cbGoodsOriginCountry, cbSectoral, txtPackage, cbPackageUnit, txtNetWeight, txtQty, cbMeasureUnit, txtUnitPrice, txtGoodFOB, txtGoodFCharge){
			//URL to instantiate a process
			var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/formm/new/hscode";
			
			$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, saving HSCode...</em>");
			
			var rankId = parseInt(currentIndex) + 1;
			 
			paramItem = {};
			paramItem["rankid"] = rankId;
			paramItem["formid"] = formID;
			paramItem["hscode"] = cbHscode;
			paramItem["free_of_charge"] = chkFreeCharge == true ? "1" : "0";
			paramItem["description"] = txtDesc;
			paramItem["state_of_goods"] = cbGoodState;
			paramItem["country_of_origin"] = cbGoodsOriginCountry;
			paramItem["sectoral_purpose"] = cbSectoral;
			paramItem["packages"] = txtPackage;
			paramItem["packages_unit"] = cbPackageUnit;
			paramItem["net_weight"] = txtNetWeight;
			paramItem["measurement_quantity"] = txtQty;
			paramItem["measurement_unit"] = cbMeasureUnit;
			paramItem["unit_price"] = txtUnitPrice;
			paramItem["fob_value"] = txtGoodFOB;
			paramItem["freight_charge"] = txtGoodFCharge;
			
			//return parseFloat((self.fobValue() * 0.0440314).toFixed(2))
			//self.totalFOBValue = ko.observable(0);
			//self.totalFreightValue = ko.observable(0);
			var _totalFOB = 0;
			var _totalFreight = 0;
			
			console.log('....about to start request to send REST ...' + JSON.stringify(paramItem));
			
			$.ajax ( {
				type: 'POST',
				url: serviceURL,
				enctype: 'raw',
				data: JSON.stringify(paramItem),
				cache: false,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*'); 
				},
				success: function(data) { 
					console.log(">>>>>>>>>>>>>" + JSON.stringify(data));
					var outcome = data.result;
					if(outcome == "1"){
						//itemCount, currentIndex
						currentIndex++;
						if(currentIndex < itemCount){
							var item = self.hsObservableArray()[currentIndex];
							self.saveNewItemRequest(itemCount, currentIndex, formID, item.hscode, "", item.foc, item.description, item.stateOfGoods, item.countryOfOrigin, item.sectoralPurpose, item.pkgQty, item.pkgUnit, item.netWeight, item.masureQty, item.measureUnit, item.unitPrice, item.fobValue, item.freightCharge);
						}else{
							//save step 4 back again
							self.saveStepRequest(4, 7);
						}
					}else{
						$("#status").html("<br/><em class='error-message'>Error while saving added items. Please try again later</em>");
						self.lastStep(0);
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("=====sending error ========" + jqXHR.responseText);
					self.lastStep(0);
					$("#status").html("<br/><em class='error-message'>Error while saving hscodes. Please try again later</em>");
				}
			} ); 
			
	  };
	  
	  
	  self.getFormMDetails = function(){
		var _formID = self.sessionFromID();
		//console.log("session form ID: " + _formID);
		//URL to instantiate a process
		var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/formm/detail/full/" + _formID;
		
		$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting form data.</em>");
		console.log("---->>>>>>>>>>>" + serviceURL);
		
		$.ajax ( {
			type: 'GET',
			url: serviceURL,
			cache: true,
			processData: false,
			contentType: false,
			crossDomain: true,
			xhrFields: { withCredentials: false },
			beforeSend: function (xhr) { 
				//console.log('setting credentials.......');
				xhr.setRequestHeader ('Content-Type', 'application/json');  
				xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
			},
			success: function(data) { 
				console.log("data from server..." + JSON.stringify(data));
				self.setFormData(data);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("=====sending error ========" + jqXHR.responseText);
				$("#status").html("<br/><em class='error-message'>Error getting existing Form M Details. Please try again later</em>");
				//$("#status").html(jqXHR.responseText);
			}
		} ); 
	  };
	  
	  
	  self.setFormData = function(data){
			console.log("Branch id...." + data.form[0].FORM_M[0].FORMID);
			console.log("Parties...." + data.form[0].PARTIES);
			$("#cbBranch").val(data.form[0].BRANCHID);
			$("#cbPrefix").val(data.form[0].FORM_PREFIX);
			$("#txtFormType").val(data.form[0].FORMTYPE);
			$("#txtGeneralDesc").val(data.form[0].GENERAL_DESCRIPTION);
			$("#ckFXValidity").val(data.form[0].FOREX_VALIDITY);
			
			if(data.form[0].PARTIES == null || data.form[0].PARTIES == "null"){
				//todo... nothing for now
			}else{
				$("#txtApplicantName").val(data.form[0].PARTIES[0].APPLICANT_NAME);
				$("#txtApplicantNumber").val(data.form[0].PARTIES[0].APPLICANT_ACCOUNT_NO);
				$("#txtApplicantRCNo").val(data.form[0].PARTIES[0].APPLICANT_RC_NUMBER);
				$("#txtApplicantTIN").val(data.form[0].PARTIES[0].TAXID);
				$("#txtApplicantEmail").val(data.form[0].PARTIES[0].APPLICANT_EMAIL);
				$("#txtApplicantPhone").val(data.form[0].PARTIES[0].APPLICANT_PHONENO);
				$("#txtApplicantFax").val(data.form[0].PARTIES[0].APPLICANT_FAX);
				$("#cbApplicantState").val(data.form[0].PARTIES[0].APPLICANT_STATE);
				$("#txtApplicantAddr").val(data.form[0].PARTIES[0].APPLICANT_ADDRESS);
				$("#txtApplicantCity").val(data.form[0].PARTIES[0].APPLICANT_CITY);
				$("#txtApplicantNEPC").val(data.form[0].PARTIES[0].APPLICANT_NEPC);
				$("#txtApplicantPass").val(data.form[0].PARTIES[0].APPLICANT_PASS);

				$("#txtBeneName").val(data.form[0].PARTIES[0].BENEFICIARY_NAME);
				$("#txtBeneEmail").val(data.form[0].PARTIES[0].BENEFICIARY_EMAIL);
				$("#txtBenePhone").val(data.form[0].PARTIES[0].BENEFICIARY_PHONENO);
				$("#txtBeneFax").val(data.form[0].PARTIES[0].BENEFICIARY_FAX);
				$("#txtBeneAddr").val(data.form[0].PARTIES[0].BENEFICIARY_ADDRESS);
				$("#txtBeneZip").val(data.form[0].PARTIES[0].BENEFICIARY_ZIP);
				$("#cbBeneCountry").val(data.form[0].PARTIES[0].BENEFICIARY_COUNTRY);
				$("#txtBeneOrderer").val(data.form[0].PARTIES[0].BENEFICIARY_ORDERBY);
			}
			
			if(data.form[0].TRANSPORT == null || data.form[0].TRANSPORT == "null"){
				//todo... nothing for now
			}else{
				$("#cbCustomsOffice").val(data.form[0].TRANSPORT[0].CUSTOMS_OFFICE);
				$("#cbShipmentMode").val(data.form[0].TRANSPORT[0].SHIPMENT_MODE);
				$("#cbOriginCountry").val(data.form[0].TRANSPORT[0].COUNTRY_OF_ORIGIN);
				$("#cbSupplyCountry").val(data.form[0].TRANSPORT[0].COUNTRY_OF_SUPPLY);
				$("#cbPortLoading").val(data.form[0].TRANSPORT[0].ACTUAL_PORT_LOADING);
				$("#cbPortDischarge").val(data.form[0].TRANSPORT[0].ACTUAL_PORT_DISCHARGE);
				$("#txtAgent").val(data.form[0].TRANSPORT[0].INSPECTION_AGENT);
				$("#txtShipmentDate").val((data.form[0].TRANSPORT[0].SHIPMENT_DATE == null ? "" : data.form[0].TRANSPORT[0].SHIPMENT_DATE.split("T")[0]));
			}
			
			if(data.form[0].FINANCIALS == null || data.form[0].FINANCIALS == "null"){
				//todo... nothing for now
			}else{
				$("#cbFundSource").val(data.form[0].FINANCIALS[0].FUND_SOURCE);
				$("#cbCurrency").val(data.form[0].FINANCIALS[0].CURRENCY_CODE);
				$("#txtExchangeRate").val(data.form[0].FINANCIALS[0].EXCHANGE_RATE);
				//$("#txtTotalFOB").val(data.total_fob_value);
				$("#txtFCharge").val(data.form[0].FINANCIALS[0].TOTAL_FREIGHT_CHARGE);
				$("#txtAncilary").val(data.form[0].FINANCIALS[0].ANCILARY_CHARGE);
				$("#txtInsurance").val(data.form[0].FINANCIALS[0].INSURANCE_VALUE);
				//$("#txtTotalCF").val(data.total_cf_value);
				$("#txtFOC").val(data.form[0].FINANCIALS[0].TOTAL_FOC);
				
				$("#txtInvoiceNo").val(data.form[0].FINANCIALS[0].PROFORMA_INVOICE_NO);
				$("#txtInvoiceDate").val((data.form[0].FINANCIALS[0].PROFORMA_INVOICE_DATE == null ? "" : data.form[0].FINANCIALS[0].PROFORMA_INVOICE_DATE.split("T")[0]));
				$("#cbPaymentMode").val(data.form[0].FINANCIALS[0].PAYMENT_MODE);
				$("#txtPaymentDate").val((data.form[0].FINANCIALS[0].PAYMENT_DATE == null ? "" : data.form[0].FINANCIALS[0].PAYMENT_DATE.split("T")[0]));
				$("#cbTransferMode").val(data.form[0].FINANCIALS[0].TRANSFER_MODE);
				$("#cbTermsDelivery").val(data.form[0].FINANCIALS[0].DELIVERY_TERM);
				
				$("#txtTransferMode").val(data.form[0].FINANCIALS[0].TRANSFER_MODE);
				//$("#cbShipmentMode").val(data.form[0].FINANCIALS[0].SHIPMENT_MODE);
				
			}
			$("#status").html("");
	  };
	  
	  self.getFormSavedGoods = function(){
		var _formID = self.sessionFromID();
		//console.log("session form ID: " + _formID);
		//URL to instantiate a process
		var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/formm/hscode/list/" + _formID;
		
		$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting saved Form M HSCode.</em>");
		//console.log("---->>>>>>>>>>>" + serviceURL);
		
		$.ajax ( {
			type: 'GET',
			url: serviceURL,
			cache: true,
			processData: false,
			contentType: false,
			crossDomain: true,
			xhrFields: { withCredentials: false },
			beforeSend: function (xhr) { 
				//console.log('setting credentials.......');
				xhr.setRequestHeader ('Content-Type', 'application/json');  
				xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
			},
			success: function(data) { 
				
				//self.setFormHSData(data);
				var countedResult = data.items.length;
				var topData = [];
				var _totalFOB = 0;
				var _totalFreight = 0;
				self.totalFOBValue(0);
				//self.totalFreightValue(0);
				
				for(i = 0; i < countedResult; i++){
					itemData = data.items[i];
					jsonData = itemData;//JSON.parse(JSON.stringify(itemData));
					
					var hscodeData = {
								 'hscode': jsonData.hscode,
								 'description': jsonData.description,
								 'stateOfGoods': jsonData.state_of_goods,
								 'countryOfOrigin': jsonData.country_of_origin,
								 'netWeight': jsonData.net_weight,
								 'fobValue': jsonData.fob_value,
								 'freightCharge': jsonData.freight_charge
							  };
							  
					/*
					var hscodeData = {
					 'hscode': cbHscode,
					 'description': txtDesc,
					 'stateOfGoods': cbGoodState,
					 'sectoralPurpose': cbSectoral,
					 'pkgQty.': txtPackage,
					 'pkgUnit.': cbPackageUnit,
					 'netWeight': txtNetWeight,
					 'masureQty': txtQty,
					 'measureUnit': cbMeasureUnit,
					 'unitPrice': txtUnitPrice,
					 'countryOfOrigin': cbGoodsOriginCountry,
					 'fobValue': txtGoodFOB,
					 'foc' : chkFreeCharge,
					 'freightCharge': "0"
				  };
					*/
					self.hsArray.push(hscodeData);
					self.hsObservableArray.push(hscodeData);
					
					//calculate FOB and Freight
					_totalFOB = parseFloat(_totalFOB) + parseFloat(jsonData.fob_value);
					//_totalFreight = parseFloat(_totalFreight) + parseFloat(jsonData.freight_charge);
					
				} 
				console.log("total freight charge..." + _totalFreight);
				var calTotalFOB = parseFloat((self.totalFOBValue()) + parseFloat(_totalFOB).toFixed(2));
				//var calTotalFreight = parseFloat((self.totalFreightValue()) + parseFloat(_totalFreight).toFixed(2));
				self.totalFOBValue(parseFloat(calTotalFOB));
				//self.totalFreightValue(parseFloat(calTotalFreight));
				console.log("about to refresh....");
				var goodsTable = document.getElementById('savedGoodsTable');
				goodsTable.refresh();
				
				$("#status").html("");
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("=====sending error ========" + jqXHR.responseText);
				$("#status").html("<br/><em class='error-message'>Error getting existing HSCodes. Please try again later</em>");
				//$("#status").html(jqXHR.responseText);
			}
		} ); 
	  };
	  
	  
	  self.getFormMAttachments = function(){
		var _formID = self.sessionFromID();
		//console.log("session form ID: " + _formID);
		//URL to instantiate a process
		var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/formm/attachment/list/" + _formID;
		
		$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting form data.</em>");
		//console.log("---->>>>>>>>>>>" + serviceURL);
		
		$.ajax ( {
			type: 'GET',
			url: serviceURL,
			cache: true,
			processData: false,
			contentType: false,
			crossDomain: true,
			xhrFields: { withCredentials: false },
			beforeSend: function (xhr) { 
				//console.log('setting credentials.......');
				xhr.setRequestHeader ('Content-Type', 'application/json');  
				xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
			},
			success: function(data) { 
				
				//self.setFormHSData(data);
				var countedResult = data.items.length;
				var topData = [];
				for(i = 0; i < countedResult; i++){
					itemData = data.items[i];
					jsonData = itemData;//JSON.parse(JSON.stringify(itemData));
					
					var attachmentData = {
								 'documentType': jsonData.document_type,
								 'description': jsonData.annotation,
								 'documentName': jsonData.file_name
							  };
					
					self.attachmentObservableArray.push(attachmentData);
					self.uploadedDocumentTypes.push(jsonData.document_type);
				}
				$("#status").html("");
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("=====sending error ========" + jqXHR.responseText);
				$("#status").html("<br/><em class='error-message'>Error while getting attachment details. Please try again later</em>");
				//$("#status").html(jqXHR.responseText);
			}
		} ); 
	  };
	  
	  
	  
	  self.saveStepRequest = function(stepNo, toStep){
	    
		//console.log("in save step..." + stepNo);
		//step 1
		var cbBranch = $("#cbBranch").val();
		var cbPrefix = $("#cbPrefix").val();
		var txtFormType = $("#txtFormType").val();
		var txtDesc = $("#txtGeneralDesc").val();
		var ckFXValidity = $("#ckFXValidity").val();
		
		//step 2
		var txtApplicantName = $("#txtApplicantName").val();
		var txtApplicantNumber = $("#txtApplicantNumber").val();
		var txtApplicantRCNo = $("#txtApplicantRCNo").val();
		var txtApplicantTIN = $("#txtApplicantTIN").val();
		var txtApplicantEmail = $("#txtApplicantEmail").val();
		var txtApplicantPhone = $("#txtApplicantPhone").val();
		var txtApplicantFax = $("#txtApplicantFax").val();
		var cbApplicantState = $("#cbApplicantState").val();
		var txtApplicantAddr = $("#txtApplicantAddr").val();
		var txtApplicantCity = $("#txtApplicantCity").val();
		var txtApplicantNEPC = $("#txtApplicantNEPC").val();
		var txtApplicantPass = $("#txtApplicantPass").val();
		
		//step 3
		var txtBeneName = $("#txtBeneName").val();
		var txtBeneEmail = $("#txtBeneEmail").val();
		var txtBenePhone = $("#txtBenePhone").val();
		var txtBeneFax = $("#txtBeneFax").val();
		var txtBeneAddr = $("#txtBeneAddr").val();
		var txtBeneZip = $("#txtBeneZip").val();
		var cbBeneCountry = $("#cbBeneCountry").val();
		var txtBeneOrderer = $("#txtBeneOrderer").val();
		
		//step 4
		var cbCustomsOffice = $("#cbCustomsOffice").val();
		var cbShipmentMode = $("#cbShipmentMode").val();
		var cbOriginCountry = $("#cbOriginCountry").val();
		var cbSupplyCountry = $("#cbSupplyCountry").val();
		var cbPortLoading = $("#cbPortLoading").val();
		var cbPortDischarge = $("#cbPortDischarge").val();
		var txtAgent = $("#txtAgent").val();
		var txtShipmentDate = $("#txtShipmentDate").val();
		
		//step 5
		var cbFundSource = $("#cbFundSource").val();
		var cbCurrency = $("#cbCurrency").val();
		var txtExchangeRate = $("#txtExchangeRate").val();
		var cbPaymentMode = $("#cbPaymentMode").val();
		var txtTotalFOB = $("#txtTotalFOB").val();
		var txtFCharge = $("#txtFCharge").val();
		var txtAncilary = $("#txtAncilary").val();
		var txtInsurance = $("#txtInsurance").val();
		var txtTotalCF = $("#txtTotalCF").val();
		var txtFOC = $("#txtFOC").val();
		var txtInvoiceNo = $("#txtInvoiceNo").val();
		var txtInvoiceDate = $("#txtInvoiceDate").val();
		var txtPaymentDate = $("#txtPaymentDate").val();
		var cbTransferMode = $("#cbTransferMode").val();
		var cbTermsDelivery = $("#cbTermsDelivery").val();
		
		
		//console.log("**** after form values.");
		
		//the initial part of the form
		if(parseInt(stepNo) == 0){
			if(self.formMID() == ""){
				self.formMID(generateCID());
			}else{
				console.log("No need to generate new formID");
			}
		}
		var formID = self.formMID();
		//for other steps... use the existing form ID
		self.saveRequest(stepNo, toStep, 
			formID, cbBranch, cbPrefix, txtFormType, txtDesc, ckFXValidity,
			txtApplicantName, txtApplicantNumber, txtApplicantRCNo, txtApplicantTIN, txtApplicantEmail, txtApplicantPhone, txtApplicantAddr, cbApplicantState, txtApplicantFax, txtApplicantCity, txtApplicantNEPC, txtApplicantPass,
			txtBeneName, txtBeneEmail, txtBenePhone, txtBeneAddr, txtBeneFax,txtBeneZip, cbBeneCountry, txtBeneOrderer,
			cbCustomsOffice, cbShipmentMode, cbOriginCountry, cbSupplyCountry, cbPortLoading, cbPortDischarge, txtAgent,txtShipmentDate,
			cbFundSource, cbCurrency, txtExchangeRate, txtTotalFOB, txtFCharge, txtAncilary, 
			txtInsurance, txtTotalCF, txtFOC, txtInvoiceNo, txtInvoiceDate, cbPaymentMode, txtPaymentDate, cbTransferMode, cbTermsDelivery);
	
	  };
	  
	  
	  self.saveRequest = function(stepNo, toStep, 
			formID, cbBranch, cbPrefix, txtFormType, txtDesc, ckFXValidity,
			txtApplicantName, txtApplicantNumber, txtApplicantRCNo, txtApplicantTIN, txtApplicantEmail, txtApplicantPhone, txtApplicantAddr, cbApplicantState, txtApplicantFax, txtApplicantCity, txtApplicantNEPC, txtApplicantPass,
			txtBeneName, txtBeneEmail, txtBenePhone, txtBeneAddr, txtBeneFax, txtBeneZip, cbBeneCountry, txtBeneOrderer,
			cbCustomsOffice, cbShipmentMode, cbOriginCountry, cbSupplyCountry, cbPortLoading, cbPortDischarge, txtAgent,txtShipmentDate,
			cbFundSource, cbCurrency, txtExchangeRate, txtTotalFOB, txtFCharge, txtAncilary, 
			txtInsurance, txtTotalCF, txtFOC, txtInvoiceNo, txtInvoiceDate, cbPaymentMode, txtPaymentDate, cbTransferMode, cbTermsDelivery){
			
			var addedNumber = 1;
			
			//URL to instantiate a process
			var reqStep = parseInt(stepNo) + parseInt(addedNumber);
			var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/formm/new/step" + reqStep;
			
			
			$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, submitting application form...</em>");
			//console.log(sessionStorage.getItem("userId") + "---->>>>>>>>>>>" + serviceURL);
			
			//console.log("using identity..." + formID);
			paramItem = {};
			paramItem["formid"] = formID;
			if(parseInt(stepNo) == 0){
				serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/formm/new/step" + reqStep;
				paramItem["branchid"] = cbBranch;
				paramItem["prefix"] = cbPrefix;
				paramItem["formtype"] = txtFormType;
				paramItem["description"] = txtDesc;
				paramItem["forex_validity"] = ckFXValidity == false? "N" : "Y";
				paramItem["inputer"] = sessionStorage.getItem("userId");
				
			}
			
			if(parseInt(stepNo) == 1){
				serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/formm/new/step" + reqStep;
				paramItem["applicant_name"] = txtApplicantName;
				paramItem["applicant_account_no"] = txtApplicantNumber;
				paramItem["applicant_rc_number"] = txtApplicantRCNo;
				paramItem["taxid"] = txtApplicantTIN;
				paramItem["applicant_email"] = txtApplicantEmail;
				paramItem["applicant_phoneno"] = txtApplicantPhone;
				paramItem["applicant_address"] = txtApplicantAddr;
				paramItem["applicant_state"] = cbApplicantState;
				paramItem["applicant_fax"] = txtApplicantFax;
				paramItem["applicant_city"] = txtApplicantCity;
				paramItem["applicant_nepc"] = txtApplicantNEPC;
				paramItem["applicant_pass"] = txtApplicantPass;
			}
			if(parseInt(stepNo) == 2){
				serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/formm/new/step" + reqStep;
				paramItem["beneficiary_name"] = txtBeneName;
				paramItem["beneficiary_email"] = txtBeneEmail;
				paramItem["beneficiary_phoneno"] = txtBenePhone;
				paramItem["beneficiary_fax"] = txtBeneFax;
				paramItem["beneficiary_address"] = txtBeneAddr;
				paramItem["beneficiary_zip"] = txtBeneZip;
				paramItem["beneficiary_country"] = cbBeneCountry;
				paramItem["beneficiary_orderby"] = txtBeneOrderer;
			}
			
			if(parseInt(stepNo) == 3){
				serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/formm/new/step" + reqStep;
				paramItem["customs_office"] = cbCustomsOffice;
				paramItem["shipment_mode"] = cbShipmentMode;
				paramItem["country_of_origin"] = cbOriginCountry;
				paramItem["country_of_supply"] = cbSupplyCountry;
				
				paramItem["actual_port_loading"] = cbPortLoading;
				paramItem["actual_port_discharge"] = cbPortDischarge;
				paramItem["inspection_agent"] = txtAgent;
				paramItem["shipment_date"] = txtShipmentDate;
			}
			if(parseInt(stepNo) == 4){ 
				addedNumber = 2;
				reqStep = parseInt(stepNo) + parseInt(addedNumber);
				serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/formm/new/step" + reqStep;
				
				paramItem["designated_bank"] = "214";
				paramItem["fund_source"] = cbFundSource;
				paramItem["currency_code"] = cbCurrency;
				paramItem["exchange_rate"] = txtExchangeRate;
				paramItem["total_fob_value"] = txtTotalFOB;
				paramItem["total_freight_charge"] = txtFCharge;
				paramItem["ancilary_charge"] = txtAncilary;
				paramItem["insurance"] = txtInsurance;
				paramItem["total_cf_value"] = txtTotalCF;
				paramItem["total_foc"] = txtFOC;
				
				paramItem["invoice_no"] = txtInvoiceNo;
				paramItem["invoice_date"] = txtInvoiceDate;
				paramItem["payment_mode"] = cbPaymentMode;
				paramItem["payment_date"] = txtPaymentDate;
				
				paramItem["transfer_mode"] = cbTransferMode;
				paramItem["delivery_term"] = cbTermsDelivery;
				
			}
			
			console.log('....about to start request to send REST ...' + JSON.stringify(paramItem));
			
			$.ajax ( {
				type: 'POST',
				url: serviceURL,
				enctype: 'raw',
				data: JSON.stringify(paramItem),
				cache: false,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					//console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*'); 
				},
				success: function(data) { 
					//self.processUpdateRESTInfo(data);
					//console.log('Saved successfully...' + data);
					//clear or redirect
					self.isSaved(1);
					
					if(self.lastStep() == 1 && parseInt(stepNo) == 4){
						self.completeApplicationForm(self.formMID());
					}else{
						self.processStepDisplayAction(toStep, false);
					}
					$("#status").html("");
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("=====sending error ========" + jqXHR.responseText);
					$("#status").html("<br/><em class='error-message'>Error while saving application form details. Please try again later</em>");
				}
			} ); 
			
	  };
	  
	  
	  self.completeButtonAction = function(event){
		  //self.completeApplicationForm(self.formMID());
		  self.lastStep(1);
		  self.saveFinalApplicationItems(self.formMID());
	  };
	  
	  self.completeApplicationForm = function(formID){
			//URL to instantiate a process
			var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/formm/new/stepcomplete";
			
			
			$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, Completing application form...</em>");
			//console.log(sessionStorage.getItem("userId") + "---->>>>>>>>>>>" + serviceURL);
			
			
			paramItem = {};
			paramItem["formid"] = formID;
			paramItem["inputer"] =  sessionStorage.getItem("userId");
			
			//console.log('....about to start request to send REST ...' + JSON.stringify(paramItem));
			
			$.ajax ( {
				type: 'POST',
				url: serviceURL,
				enctype: 'raw',
				data: JSON.stringify(paramItem),
				cache: false,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					//console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*'); 
				},
				success: function(data) { 
					//self.processUpdateRESTInfo(data);
					//console.log('Saved successfully...' + data);
					$("#status").html("");
					window.location.replace('/?root=' + self.navTo);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					//console.log("=====sending error ========" + jqXHR.responseText);
					$("#status").html("<br/><em class='error-message'>Error while saving application form. Please try again later</em>");
				}
			} ); 
			
	  };
	  
	  
	  self.emailPatternValidator = ko.pureComputed(function () {
        return [{
            type: 'regExp',
            options: {
              pattern: "[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*",
              hint: "Enter a valid email format",
              messageDetail: "Not a valid email format"}}];
      });
	  self.amountPatternValidator = ko.pureComputed(function () {
        return [{
            type: 'regExp',
            options: {
              pattern: "^\$?[0-9]?((\.[0-9]+)|([0-9]+(\.[0-9]+)?))$",
              hint: "Enter a valid amount format e.g 123.00",
              messageDetail: "Not a valid amount"}}];
      });
	  self.getBranches = function(){
			//URL to instantiate a process
			var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/config/branches/";
			
			$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting bank branches details.</em>");
			console.log("---->>>>>>>>>>>" + serviceURL);
			
			$.ajax ( {
				type: 'GET',
				url: serviceURL,
				cache: true,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					//console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
				},
				success: function(data) { 
					var countedResult = data.items.length;
					var topData = [];
					for(i = 0; i < countedResult; i++){
						itemData = data.items[i];
						jsonData = itemData;//JSON.parse(JSON.stringify(itemData));
						var item = {};
						item['branchid'] = jsonData.branchid;
						item['description'] = jsonData.description;
						topData.push(item);
						
					}
					self.branches(topData);
					$("#status").html("");
					//console.log("***********NOW check mode *****************");
					if(self.modMode() == "MODIFY"){
						console.log("***********NOW IN MODIFY *****************");
						self.formMID(self.sessionFromID());
						self.getFormMDetails();
						self.getFormSavedGoods();
						self.getFormMAttachments();
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					//console.log("=====sending error ========" + jqXHR.responseText);
					var existing_error = $("#status").html();
					$("#status").html(existing_error + "<br/><em class='error-message'>Error while getting list of branches. Please try again later</em>");
					//$("#status").html(jqXHR.responseText);
				}
			} ); 
	  };
	  self.getHSCodes = function(){
			//URL to instantiate a process
			var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/config/hscodes/";
			
			$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting hscode list.</em>");
			//console.log("---->>>>>>>>>>>" + serviceURL);
			
			$.ajax ( {
				type: 'GET',
				url: serviceURL,
				cache: true,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					//console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
					xhr.setRequestHeader ('like', '%%');
				},
				success: function(data) { 
					//console.log(data);
					var countedResult = data.items.length;
					var topData = [];
					for(i = 0; i < countedResult; i++){
						itemData = data.items[i];
						jsonData = itemData;//JSON.parse(JSON.stringify(itemData));
						var item = {};
						item['value'] = jsonData.hscode;
						item['label'] = jsonData.description;
						topData.push(item);
						
					}
					self.hscodes(topData);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					//console.log("=====sending error ========" + jqXHR.responseText);
					var existing_error = $("#status").html();
					$("#status").html(existing_error + "<br/><em class='error-message'>Error while getting list of HS Codes. Please try again later</em>");
					//$("#status").html(jqXHR.responseText);
				}
			} ); 
	  };
	  
	  self.populateDropDown = function(index){
			var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/config/hscodes/";
			
			switch(index){
				case 1: //customs office
					serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/config/customs_offices/";
					self.getDropDownValues(index, serviceURL, "Customs Office", self.lovCustomsOffices);
					break;
				case 2: //transportation mode
					serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/config/transportmodes/";
					self.getDropDownValues(index, serviceURL, "Transportation Mode", self.lovTransportModes);
					break;
				case 3: //ports
					serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/config/ports/";
					self.getDropDownValues(index, serviceURL, "Port Mode", self.lovPorts);
					break;
				case 4: //fund sources
					serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/config/fundsources/";
					self.getDropDownValues(index, serviceURL, "Fund Source", self.lovFundSources);
					break;
				case 5: //payment modes
					serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/config/paymentmodes/";
					self.getDropDownValues(index, serviceURL, "Payment Mode", self.lovPaymentModes);
					break;
				case 6: //transfer modes
					serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/config/transfermodes/";
					self.getDropDownValues(index, serviceURL, "Transfer Mode", self.lovTransferModes);
					break;
				case 7: //transfer modes
					serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/config/deliveryterms/";
					self.getDropDownValues(index, serviceURL, "Delivery Terms", self.lovDeliveryTerms);
					break;
				case 8: //transfer modes
					serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/config/stateofgoods/";
					self.getDropDownValues(index, serviceURL, "State of Goods", self.lovStateOfGoods);
					break;	
				case 9: //transfer modes
					serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/config/sectoralpurposes/";
					self.getDropDownValues(index, serviceURL, "Sectoral Purposes", self.lovSectoralPurposes);
					break;	
				case 10: //transfer modes
					serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/config/states/";
					self.getDropDownValues(index, serviceURL, "Nigerian States", self.lovStates);
					break;	
				default:
					self.getBranches();
					break;
			}
	  };
	  
	  self.getDropDownValues = function(index, serviceURL, identifier, lovObserver){
			
			$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting " + identifier + " list.</em>");
			console.log(index + "---->>>>>>>>>>>" + serviceURL);
			
			$.ajax ( {
				type: 'GET',
				url: serviceURL,
				cache: true,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					//console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
					xhr.setRequestHeader ('like', '%%');
				},
				success: function(data) { 
					//console.log(data);
					var countedResult = data.items.length;
					var topData = [];
					for(i = 0; i < countedResult; i++){
						itemData = data.items[i];
						jsonData = itemData;//JSON.parse(JSON.stringify(itemData));
						var item = {};
						item['code'] = jsonData.code;
						item['description'] = jsonData.description;
						topData.push(item);
						
					}
					lovObserver(topData);
					self.populateDropDown(parseInt(index) + 1);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					//console.log("=====sending error ========" + jqXHR.responseText);
					var existing_error = $("#status").html();
					$("#status").html(existing_error + "<br/><em class='error-message'>Error while getting list of " + identifier + ". Please try again later</em>");
					//$("#status").html(jqXHR.responseText);
				}
			} ); 
	  };
	  
	  self.populateDropDown(1);
	  
	  self.getHSCodes();
	  /*
	  if(self.modMode() == "MODIFY"){
		self.formMID(self.sessionFromID);
		self.getFormMDetails();
		self.getFormMHSCodes();
		self.getFormMAttachments();
	  }
	  */
	  
      // Below are a set of the ViewModel methods invoked by the oj-module component.
      // Please reference the oj-module jsDoc for additional information.

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here. 
       * This method might be called multiple times - after the View is created 
       * and inserted into the DOM and after the View is reconnected 
       * after being disconnected.
       */
      self.connected = function() {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function() {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = function() {
        // Implement if needed
      };
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    return new FormMViewModel();
  }
);
