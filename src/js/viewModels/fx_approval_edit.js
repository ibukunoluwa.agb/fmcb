/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your about ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
		'ojs/ojbutton', 'ojs/ojrouter', 'ojs/ojlabel', 'messages', 'ojs/ojarraydataprovider', 'ojs/ojtable', 'ojs/ojswitch',
		'ojs/ojmessages', 'ojs/ojmessage'],
 function(oj, ko, $) {
  
	function getParameterByName(name, url) {
		if (!url) url = window.location.href;
		name = name.replace(/[\[\]]/g, '\\$&');
		var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, ' '));
	}
	
	function generateCID(prefix){
			 
			var d = new Date();
			var multiplier = 3;
			var time = d.getTime(); 
			var year = d.getUTCFullYear();
			
			var guid =  prefix + "-" + (parseInt(multiplier) * parseInt(time));
			return guid;
			
	  };
	
	function getTimestampFromString(dateString){
		var currentTime = parseInt((new Date(dateString).getTime() / 1000).toFixed(0));
		return currentTime
	}
	
    function FXApprovalViewModel() {
      var self = this;
	  self.rootModel = ko.dataFor(document.getElementById('globalBody'));
	  self.navToError = "error";
	  self.navTo = "success";
	  
	  self.requestID = getParameterByName("req");
	  if(self.requestID == null || self.requestID == ""){
		  window.location.replace('/?root=' + self.navToError + "&code=100");
		  return;
	  }
	  
	  self.formStatus = ko.observable("PENDING");
	  self.inputer = ko.observable(sessionStorage.getItem("userId"));
	  
	  //form M request object
	  self.formID = ko.observable("");
	  self.approvalID = ko.observable("");
	  self.appNumber = ko.observable("");
	  self.formMNumber = ko.observable("");
	  self.requestAmount = ko.observable("");
	  self.requestLetter = ko.observable("");
	  self.requestDate = ko.observable("");
	  self.createDate = ko.observable("");
	  self.creator = ko.observable("");
	  
	  //switch control values
	  self.isFundAvailable = ko.observable(false);
	  self.isDocCorrect = ko.observable(true);
	  self.isValidity = ko.observable(false);
	  self.isValidOutstanding = ko.observable(true);
	  
	  //total and outstanding
	  self.totalAmount = ko.observable(0);
	  self.outstandingAmount = ko.observable(0);
	  
	  self.selectedRowKey = ko.observable("");
	  
	  self.formMArray = [];
	  self.formMObservableArray = ko.observableArray(self.formMArray);
	  //self.formMDataProvider = new oj.ArrayDataProvider(self.formMObservableArray, {keyAttributes: 'formid'});
	  self.formMDataProvider = new oj.ArrayTableDataSource(self.formMObservableArray, {idAttribute: 'formid'});
	  self.paginDataSource = new oj.PagingTableDataSource(self.formMDataProvider);
	  
      console.log('----------**-==-**-------------'); 
	  
	  
	  self.viewFormAction = function(event){
		  var wnd = window.open("/?root=form_view&act=v&fid=" + self.formID(), "_blank");
	  };
	  
	  self.rejectButtonAction = function(event){
		  var comment = $("#txtComment").val();
		  self.approveRequest("rejecting", self.requestID, self.approvalID(), "REJECTED", "PENDING", generateCID('AUD'), comment);
	  };
	  
	  self.approveButtonAction = function(event) 
	  {
		  console.log("*******approve button************");
		  var data = event.detail;
		  
		  if(self.isFundAvailable() == false){
			  $("#status").html("<br/><em class='error-message'><img src='./images/error.png' width='24px' height='24' class='img-align'/>&nbsp;&nbsp;You cannot approve due to insufficient fund.</em>");
			  return;
		  }
		  if(self.isDocCorrect() == false){
			  $("#status").html("<br/><em class='error-message'><img src='./images/error.png' width='24px' height='24' class='img-align'/>&nbsp;&nbsp;Confirm that documentation is correct and try again.</em>");
			  return;
		  }
	      if(self.isValidity() == false){
			  $("#status").html("<br/><em class='error-message'><img src='./images/error.png' width='24px' height='24' class='img-align'/>&nbsp;&nbsp;You cannot approve until you confirm the validity date is correct.</em>");
			  return;
		  }
	      if(self.isValidOutstanding() == false){
			  $("#status").html("<br/><em class='error-message'><img src='./images/error.png' width='24px' height='24' class='img-align'/>&nbsp;&nbsp;Please confirm that the customer outstanding is valid.</em>");
			  return;
		  }
	  
		  console.log(data);
		  var comment = $("#txtComment").val();
		  self.approveRequest("approving", self.requestID, self.approvalID(), "APPROVED", "PENDING", generateCID('AUD'), comment);
			
			
		  //$("#status").html("<br/><em class='error-message'><img src='./images/error.png' width='24px' height='24' class='img-align'/>&nbsp;&nbsp;Select one of the Form M from the list above to start making modification.</em>");
		  
		  //window.location.replace('/?root=' + self.navTo);
      };
	
	  self.approveRequest = function(key, requestID, approvalID, status, oldStatus, auditID, comment){
			//URL to instantiate a process
			var successCode = "001";
			
			if(key == "approving"){
				successCode = "010";
				//check if all switch is true
				if(self.isValidity() == false){
					$("#status").html("<br/><img src='./images/error.png' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Validity date must be valid and checked.</em>");
					return;
				}
				if(self.isDocCorrect() == false){
					$("#status").html("<br/><img src='./images/error.png' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Document validity must be checked.</em>");
					return;
				}
				if(self.isValidOutstanding() == false){
					$("#status").html("<br/><img src='./images/error.png' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Outstanding amount must be checked.</em>");
					return;
				}
				if(self.isFundAvailable() == false){
					$("#status").html("<br/><img src='./images/error.png' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Fund availability must be checked.</em>");
					return;
				}
			}
			
			if(key == "rejecting"){
				successCode = "020";
			}
			
			//https://129.156.113.190/ords/pdb1/fcmb/fx/request/action/approval/
			
			var serviceURL = self.rootModel.ORDSURL + "/ords/pdb1/fcmb/fx/request/action/approval/";
			
			$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, " + key + " FX Request...</em>");
			console.log(sessionStorage.getItem("userId") + "---->>>>>>>>>>>" + serviceURL);
			
			paramItem = {};
			paramItem["status"] = status;
			paramItem["oldstatus"] = oldStatus;
			paramItem["reqid"] = requestID;
			paramItem["id"] = approvalID;
			paramItem["auditid"] = auditID;
			paramItem["action_by"] = sessionStorage.getItem("userId");
			paramItem["comment"] = comment;
			
			paramItem["check_validity"] = self.isValidity() == true ? "1" : "0";
			paramItem["check_documentation"] = self.isDocCorrect() == true ? "1" : "0";
			paramItem["check_total_outstanding"] = self.isValidOutstanding() == true ? "1" : "0";
			paramItem["check_fund_availability"] = self.isFundAvailable() == true ? "1" : "0";
			
			
			console.log('....about to start request to send REST ...' + JSON.stringify(paramItem));
			
			$.ajax ( {
				type: 'POST',
				url: serviceURL,
				enctype: 'raw',
				data: JSON.stringify(paramItem),
				cache: false,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*'); 
				},
				success: function(data) { 
					//self.processUpdateRESTInfo(data);
					console.log('Saved successfully...' + data);
					if(data.result == "1"){
						$("#status").html("");
						//window.location.replace('/?root=' + self.navTo + "&type=" + successCode);
						var _validity = self.isValidity() == true ? "1" : "0";
						var _docCorrect = self.isDocCorrect() == true ? "1" : "0";
						var _validOutstanding = self.isValidOutstanding() == true ? "1" : "0";
						var _fundAvailability =  self.isFundAvailable() == true ? "1" : "0"; 
						
						
						requestedDate = getTimestampFromString(self.requestDate());
						self.saveBCSTransaction(approvalID, self.requestID, self.formID(), self.requestAmount(), comment, 
										sessionStorage.getItem("userId"), requestedDate, status,    
										_validity, _docCorrect, _validOutstanding, _fundAvailability)
					}else{
						$("#status").html("<br/><em class='error-message'>Unable to complete " + key + " request. Please try again later</em>");
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("=====sending error ========" + jqXHR.responseText);
					$("#status").html("<br/><em class='error-message'>Error while " + key + " your request. Please try again later</em>");
				}
			} ); 
			
	  };
	  
	  self.saveBCSTransaction = function(approvalID, requestID, formID, fxAmount, feedback, actionBy, transactionDate, status, 
										checkValidity, checkDocumentation, checkTotalOutstanding, checkFundAvailability ){
			
			var successCode = "001";			
			
			//URL to instantiate a process
			var serviceURL = self.rootModel.BCSExecuteURL;
			var chainCodeName = self.rootModel.BCSChainCodeName;
			var channelName = self.rootModel.BCSChannel;
			var chainCodeVer = self.rootModel.BCSChainCodeVer;
			console.log('BC URL:' + serviceURL);
			$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, processing bloackchain information.</em>");
			
			console.log(serviceURL + "=============>" + chainCodeName);
			
			//get current date in seconds
			//currentTime = parseInt((new Date().getTime() / 1000).toFixed(0));
			
			//self.buildBlockchainData();
			//var args = self.blockchainArgs();
			//return;
			
			var args = "[\"" + approvalID + "\",\"" + requestID + "\", \"" + formID + "\", \"" + fxAmount + 
				"\", \"" + feedback + "\", \"" + actionBy + "\", \"" + transactionDate + "\", \"" + status + 
				"\",\"" + checkValidity + "\",\"" + checkDocumentation + "\",\"" + checkTotalOutstanding + "\",\"" + checkFundAvailability + "\"] ";
			
			//console.log('8********');
			
			paramItem = {};
			paramItem["channel"] = channelName;
			paramItem["chaincode"] = chainCodeName;
			paramItem["method"] = "createFXTransaction";
			paramItem["args"] = args;
			paramItem["chaincodeVer"] = chainCodeVer;
			
		    //console.log(self.blockchainArgs());
			console.log('....about to start BCS request to send REST ...' + JSON.stringify(paramItem));
			//return;
			
			$.ajax ( {
				type: 'POST',
				url: serviceURL,
				enctype: 'raw',
				data: JSON.stringify(paramItem),
				cache: false,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');   
					
				},
				success: function(data) { 
					//self.processUpdateRESTInfo(data);
					console.log('Data...' + JSON.stringify(data));
					
					self.returnCode = data.returnCode;
					if(self.returnCode == "Success"){
						$("#status").html('<br/>Transaction Completed Successfully... Please wait for confirmation');
						window.location.replace('/?root=' + self.navTo + "&type=" + successCode);
						return;
					}if(self.returnCode == "Failure"){
						$("#status").html('<br/>Unable to Completed Registration on Blockchain.');
						return;
					}else{
						//try removing record from blockchain
					}
					
					//var encodedKey = btoa(transactionRef);
					//var encodedName = btoa(fullName);
					//window.location.replace("./booked?key=" + encodedKey + "&name=" + encodedName);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("=====sending error ========" + jqXHR.responseText);
					$("#status").html("<br/><em class='error-message'>Error while registering account. Please try again later</em>");
				}
			} ); 
	  };
	  
	  self.getAmountSummary = function(formId){
		
		//URL to instantiate a process
		var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/fx/form/outstanding/";
		
		$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting amount summary.</em>");
		console.log("---->>>>>>>>>>>" + serviceURL);
		
		$.ajax ( {
			type: 'GET',
			url: serviceURL,
			cache: true,
			processData: false,
			contentType: false,
			crossDomain: true,
			xhrFields: { withCredentials: false },
			beforeSend: function (xhr) { 
				//console.log('setting credentials.......');
				xhr.setRequestHeader ('Content-Type', 'application/json');  
				xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
				xhr.setRequestHeader ('formid', formId);
			},
			success: function(data) { 
				console.log("-------amount summary---------");
				console.log(JSON.stringify(data));
				
				self.totalAmount(data.total_amount);
				self.outstandingAmount(data.outstanding);
				
				if((parseFloat(self.outstandingAmount()) - parseFloat(self.requestAmount())) > 0){
					console.log("select and enable switch......");
					var chkOutstanding = document.getElementById('chkOutstanding');
					chkOutstanding.value = true;
					chkOutstanding.disabled = false;
				}else{
					//deselect switch and disable
					console.log("disable switch......");
					var chkOutstanding = document.getElementById('chkOutstanding');
					chkOutstanding.value = false;
					chkOutstanding.disabled = true;
				}
				$("#status").html("");
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("=====sending error ========" + jqXHR.responseText);
				$("#status").html("<br/><em class='error-message'>Error verifying email and password. Please try again later</em>");
				//$("#status").html(jqXHR.responseText);
			}
		} );
		
		
	  };
	  
      self.getFXRequestDetails = function(){
		console.log("in getting formM list " + self.formStatus());
		//URL to instantiate a process
		//https://129.156.113.190/ords/pdb1/fcmb/fx/request/details/
		var serviceURL = self.rootModel.ORDSURL + "/ords/pdb1/fcmb/fx/request/details/";
		
		$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting form data.</em>");
		console.log("---->>>>>>>>>>>" + serviceURL);
		
		$.ajax ( {
			type: 'GET',
			url: serviceURL,
			cache: true,
			processData: false,
			contentType: false,
			crossDomain: true,
			xhrFields: { withCredentials: false },
			beforeSend: function (xhr) { 
				console.log('setting credentials.......');
				xhr.setRequestHeader ('Content-Type', 'application/json');  
				xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
				xhr.setRequestHeader ('status', self.formStatus());
				xhr.setRequestHeader ('id', self.requestID);
			},
			success: function(data) { 
				
				console.log(JSON.stringify(data));
				
				var countedResult = data.items.length;
				var topData = [];
				for(i = 0; i < countedResult; i++){
					itemData = data.items[i];
					jsonData = itemData;//JSON.parse(JSON.stringify(itemData));
					self.formID(jsonData.formid);
					self.approvalID(jsonData.approvalid);
					self.appNumber(jsonData.application_number);
					self.formMNumber(jsonData.form_m_number);
					self.requestAmount(jsonData.amount);
					self.requestLetter(jsonData.letter);
					self.requestDate((jsonData.request_date == null? "" : jsonData.request_date.split("T")[0]));
					self.createDate(jsonData.application_number);
					self.creator(jsonData.action_by_name);
					
					
				}
				self.getAmountSummary(self.formID());
				//var formMTable = document.getElementById('formMList');
				//formMTable.refresh();
				//$("#status").html("");
				//self.formMDataProvider(new oj.ArrayTableDataSource(self.formMArray, {idAttribute: 'formid'}));
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("=====sending error ========" + jqXHR.responseText);
				$("#status").html("<br/><em class='error-message'>Error getting existing Form M List. Please try again later</em>");
				//$("#status").html(jqXHR.responseText);
			}
		} );
		
		
	  };
	  self.getFXRequestDetails();
	  
      // Below are a subset of the ViewModel methods invoked by the ojModule binding
      // Please reference the ojModule jsDoc for additional available methods.

      /**
       * Optional ViewModel method invoked when this ViewModel is about to be
       * used for the View transition.  The application can put data fetch logic
       * here that can return a Promise which will delay the handleAttached function
       * call below until the Promise is resolved.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @return {Promise|undefined} - If the callback returns a Promise, the next phase (attaching DOM) will be delayed until
       * the promise is resolved
       */
      self.handleActivated = function(info) {
        // Implement if needed
		sessionStorage.removeItem("mod_FormMID");
	    sessionStorage.removeItem("mod_Mode");
      };

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {boolean} info.fromCache - A boolean indicating whether the module was retrieved from cache.
       */
      self.handleAttached = function(info) {
        // Implement if needed
		
      };


      /**
       * Optional ViewModel method invoked after the bindings are applied on this View. 
       * If the current View is retrieved from cache, the bindings will not be re-applied
       * and this callback will not be invoked.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       */
      self.handleBindingsApplied = function(info) {
        // Implement if needed
      };

      /*
       * Optional ViewModel method invoked after the View is removed from the
       * document DOM.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {Array} info.cachedNodes - An Array containing cached nodes for the View if the cache is enabled.
       */
      self.handleDetached = function(info) {
        // Implement if needed
      };
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    //return new FormMPendingListViewModel();
	var fmM = new FXApprovalViewModel();
	$(document).ready
    (
		function()
		{
			console.log("-------event when document is ready");
			//var chkOutstanding = document.getElementById('chkOutstanding');
			// Getter
			//var value = myComponent.value;

			// Setter
			//chkOutstanding.value = true;
			//ko.applyBindings(fmM, formMTable);
			//formMTable.addEventListener('currentRowChanged', fmM.currentRowListener);
			
			/*
			
			
			table.addEventListener('ojBeforeCurrentRow', vm.beforeCurrentRowListener);
			table.addEventListener('ojSort', vm.sortListener);
			table.addEventListener('selectionChanged', vm.selectionListener);*/
		}
	);
	return fmM;
  }
);
