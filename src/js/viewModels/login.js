/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your about ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'crypto-js', 'ojs/ojbutton', 'ojs/ojinputtext', 
		'ojs/ojbutton', 'ojs/ojinputnumber', 'ojs/ojrouter', 'ojs/ojlabel', 'messages'],
 function(oj, ko, $, CryptoJS) {
  
    function encryptPassword(password, key){
		var CryptoJS = require("crypto-js");
		var ciphertext = CryptoJS.AES.encrypt(password, key).toString();
		//console.log(ciphertext); 
		return ciphertext;
	};
	function decryptPassword(password, key){
		var CryptoJS = require("crypto-js");
		var bytes = CryptoJS.AES.decrypt(password, key).toString();
		var plaintext = bytes.toString(CryptoJS.enc.Utf8);
		//console.log('From ' + password + ' to ' + plaintext); 
		return plaintext;
	};
	
    function LoginViewModel() {
      var self = this;
	  self.rootModel = ko.dataFor(document.getElementById('globalBody'));
	  
	  //getXMLBuilder();
      //load global variable into the Model
	  //console.log('----------**--------------' + self.rootModel);
	  //var decrypted = encryptPassword("amachree", self.rootModel.AESKey);
	  //console.log(decrypted);
	  
      
      console.log('----------**-==-**-------------');
      this.userName = ko.observable("");
	  this.password = ko.observable("");
	  self.tracker = ko.observable();
	  self.emailPatternValue = ko.observable();
	  
	  self.buttonClick = function(event){
		  console.log("************Login Clicked************");
		  
		  var x_userName = $("#username-input").val();
		  var x_Password = $("#password-input").val();
		
		  var encrypted = encryptPassword(x_Password, self.rootModel.AESKey);
		  console.log('------------>' + encrypted);
		  self.loginUser(x_userName, encrypted);
		  //sessionStorage.setItem("userKey", x_userName);
		  //sessionStorage.setItem("userPwd", x_Password);
		  
		  
		  //window.location.replace("/");
		  
		  //console.log("***********processed*************");
		  return true;
	  
	  };
	
	  self.emailPatternValidator = ko.pureComputed(function () {
        return [{
            type: 'regExp',
            options: {
              pattern: "[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*",
              hint: "Enter a valid email format",
              messageDetail: "Not a valid email format"}}];
      });
      
	  self.loginUser = function(email, password){
			//URL to instantiate a process
			//https://129.156.113.190/ords/PDB1/fcmb/authenticate/login/
			var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/authenticate/login/";
			//var serviceURL = "https://141.144.145.196/ords/pdb1/ncs/system/user/email/" + email;
			
			$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, validation in progress.</em>");
			console.log("---->>>>>>>>>>>" + serviceURL);
			
			
			$.ajax ( {
				type: 'GET',
				url: serviceURL,
				cache: true,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					//console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*'); 
					xhr.setRequestHeader ('email', email);   
					//xhr.setRequestHeader ('Authorization', 
					//					  'Basic ' + btoa('john.dunbar:mesic@2IMpulse'));
					//console.log('tHE RIGHT url: ' + serviceURL);
				},
				success: function(data) { 
					//self.processUpdateRESTInfo(data);
					console.log("=====>>" + JSON.stringify(data) + "========");
					var decrypted = decryptPassword(password, self.rootModel.AESKey);
					console.log(decrypted);
					self.loginValid(data, decrypted);
					//window.location.replace("/");
				},
				error: function(jqXHR, textStatus, errorThrown) {
					//console.log("=====sending error ========" + jqXHR.responseText);
					$("#status").html("<br/><em class='error-message'>Error verifying email and password. Please try again later</em>");
					//$("#status").html(jqXHR.responseText);
				}
			} ); 
	  };
	  
	  
	  self.loginValid = function(data, decrypted){
		  console.log(JSON.stringify(data));
		  
		  try{
			  if(data.userid == 'undefined'){
				$("#status").html("<br/><em class='error-message'>Invalid Email and Password Combination.</em>");
				return;
			  }
		  }catch(exception){
			  $("#status").html("<br/><em class='error-message'>Error: Invalid Email and Password Combination.</em>");
			  return;
		  }
		  /*
		    var x_userId = data.userid;
		  var x_firstName = data.items[0].firstname;
		  var x_email = data.items[0].email;
		  var x_lastName = data.items[0].lastname;
		  var x_password = data.items[0].password;
		  var x_processHandle = data.items[0].processhandle;
		  var x_profiletype = data.items[0].profiletype;
		  var x_isAdmin = data.items[0].isAdmin;
		   */ 
		  
		  var x_userId = data.userid;
		  var x_email = data.email;
		  var x_password = data.password;
		  var x_isCustomer = data.iscustomer;
		  var x_isPartner = data.ispartner;
		 
		  var x_decrypted = decryptPassword(x_password, self.rootModel.AESKey);
		  console.log(x_userId + ' == ' + x_isCustomer + ' >> ' + x_decrypted);
		  if(x_decrypted == decrypted){
			  console.log('Get customer details now');
			  self.getLoginUser(x_email, x_userId, x_isCustomer, x_isPartner);
			  //return;
			  
		  }else{
			  $("#status").html("<br/><em class='error-message'>Invalid Email and Password Combination...</em>");
		  }
		  //window.location.replace("/");
	  };
	  
	  self.getLoginUser = function(email, userId, isCustomer, isPartner){
			//URL to instantiate a process
			//https://129.156.113.190/ords/PDB1/fcmb/users/details/
			var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/users/details/";
			//if(isPartner == 1){
			//	serviceURL = self.rootModel.ORDSURL + "/ords/pdb1/amini/loyalty/participant/details/";
			//}
			//var serviceURL = "https://141.144.145.196/ords/pdb1/ncs/system/user/email/" + email;
			
			$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, validation in progress.</em>");
			console.log("---->>>>>>>>>>>" + serviceURL);
			
			$.ajax ( {
				type: 'GET',
				url: serviceURL,
				cache: true,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					//console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
					xhr.setRequestHeader ('userid', userId);
					xhr.setRequestHeader ('email', email);
				},
				success: function(data) { 
					//self.processUpdateRESTInfo(data);
					console.log("User Details=====>>" + JSON.stringify(data) + "========");
					
					self.saveSession(isCustomer, data);
					//window.location.replace("/");
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("=====sending error ========" + jqXHR.responseText);
					$("#status").html("<br/><em class='error-message'>Error verifying email and password. Please try again later</em>");
					//$("#status").html(jqXHR.responseText);
				}
			} ); 
	  };
	  
	  self.saveSession = function(isCustomer, data){
			try{
			  if(data.userid == 'undefined'){
				$("#status").html("<br/><em class='error-message'>Invalid Email and Password Combination.</em>");
				return;
			  }
			}catch(exception){
			  $("#status").html("<br/><em class='error-message'>Error: Invalid Email and Password Combination.</em>");
			  return;
			}
			
			 
			var x_userAlias = "";
			var x_firstName = data.firstname;
			var x_lastName = data.lastname;
			var x_email = data.email;
			var x_isVerified = data.isaccountverified;
			var x_isManager = data.ismanager;
			var x_isSuperUser = data.issuperuser;
			var x_status = data.status;
			var x_profileType = data.profiletype;
			var x_profileTypeDesc = data.profiletype_desc;
			var x_branch = data.branchid;
			var x_branchName = data.branch;
			var x_userId = data.userid;
			
			var strAlias = "";
			x_userAlias = (x_firstName + ' ').substring(0,1) + ' ' + (x_lastName + '').substring(0);
			//strAlias = '(' + x_userAlias + ')';
			
			console.log(x_branch + '>>>>Alias...' + x_userAlias);
			
			sessionStorage.setItem("userAlias", x_userAlias);
			sessionStorage.setItem("userKey", x_firstName + ' ' + x_lastName + strAlias);
			sessionStorage.setItem("userEmail", x_email);
			sessionStorage.setItem("userId", x_userId);
			sessionStorage.setItem("status", x_status);
			sessionStorage.setItem("profileTypeDesc", x_profileTypeDesc);
			sessionStorage.setItem("branchName", x_branchName);
			sessionStorage.setItem("branchId", x_branch);
			sessionStorage.setItem("isSuperAdmin", x_isSuperUser);
			sessionStorage.setItem("isManager", x_isManager);
			sessionStorage.setItem("profileType", x_profileType);
			sessionStorage.setItem("profileTypeDesc", x_profileTypeDesc);
			window.location.replace("/");
	  };
	    
      // Below are a subset of the ViewModel methods invoked by the ojModule binding
      // Please reference the ojModule jsDoc for additional available methods.

      /**
       * Optional ViewModel method invoked when this ViewModel is about to be
       * used for the View transition.  The application can put data fetch logic
       * here that can return a Promise which will delay the handleAttached function
       * call below until the Promise is resolved.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @return {Promise|undefined} - If the callback returns a Promise, the next phase (attaching DOM) will be delayed until
       * the promise is resolved
       */
      self.handleActivated = function(info) {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {boolean} info.fromCache - A boolean indicating whether the module was retrieved from cache.
       */
      self.handleAttached = function(info) {
        // Implement if needed
      };


      /**
       * Optional ViewModel method invoked after the bindings are applied on this View. 
       * If the current View is retrieved from cache, the bindings will not be re-applied
       * and this callback will not be invoked.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       */
      self.handleBindingsApplied = function(info) {
        // Implement if needed
      };

      /*
       * Optional ViewModel method invoked after the View is removed from the
       * document DOM.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {Array} info.cachedNodes - An Array containing cached nodes for the View if the cache is enabled.
       */
      self.handleDetached = function(info) {
        // Implement if needed
      };
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    return new LoginViewModel();
  }
);
