/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojinputtext', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource', 'ojs/ojarraydataprovider',
		'ojs/ojbutton', 'ojs/ojrouter', 'ojs/ojlabel', 'messages', 'ojs/ojarraydataprovider',  'ojs/ojlistview', 'ojs/ojradioset'],
 function(oj, ko, $) {
  
	function generateCID(prefix, multiplier){
			 
			var d = new Date();
			//var multiplier = 3;
			var time = d.getTime(); 
			var year = d.getUTCFullYear();
			
			var guid =  prefix + "-" + (parseInt(multiplier) * parseInt(time));
			return guid;
			
	};
	  
    function NetOffViewModel() {
      var self = this;
	  self.rootModel = ko.dataFor(document.getElementById('globalBody'));
	  
	  self.formStatus = ko.observable("APPROVED");
	  self.header = "Net Off";
	  
	  self.itemArray = [];
	  var data = [];/*[{id: 0, name: 'Potential cat names', date: 'Apr 30', content: 'Mew, Furball, Puss'},
                    {id: 1, name: 'Todo list for work', date: 'Apr 29', content: 'Add one more'},
                    {id: 2, name: 'Chicken recipes', date: 'Apr 15', content: 'Fried, Shake & Bake, Sautee'},
                    {id: 3, name: 'Running routes', date: 'Apr 3', content: 'Bedroom to kitchen and back'},
                    {id: 4, name: 'Groceries', date: 'Apr 1', content: 'Milk, bread, meat, veggie, can, etc.'},
                    {id: 5, name: 'Party guest list', date: 'Mar 29', content: ''},
                    {id: 6, name: 'Weekend projects', date: 'Mar 2', content: 'TBD'}
                   ];*/
	  
	  self.formID = ko.observable("");
	  self.formMNumber = ko.observable("");
	  self.formApplicationNo = ko.observable("");
	  self.formAmount = ko.observable("0");
	  self.requestDate = ko.observable("0");
	  self.currentRequestId = ko.observable("");
	  self.currentApproverId = ko.observable("");
	  
	  self.itemObservableArray = ko.observableArray(data);
	  self.itemDataProvider = new oj.ArrayTableDataSource(self.itemObservableArray);  //, {idAttribute: 'requestId'}
	  self.paginDataSource = new oj.PagingTableDataSource(self.itemDataProvider);
	  
	  self.makePayment = function(event){
		  //button was used for the section purposes... event is for the list
		  
	  };
	  self.processFXPayment = function(event){
		  //console.log("------");
		  //button was used for the section purposes... event is for the list
		  //self.processPaymentRequest();
		  self.saveBCSTransaction(self.formID(), self.formAmount());
	  };
	  self.cancelPayment = function(event){
		  $("#formView").hide('slow');
		  $("#listView").show('slow');
		  self.formID("");
		  self.formMNumber("");
		  self.formApplicationNo("");
		  self.formAmount("0");
		  self.requestDate("");
		  self.currentRequestId("");
		  self.currentApproverId("");
	  };
	  
	  self.gotoContent = function(event){
		  console.log(">>>>" + event.detail.value);
		  if (event.detail.value != null && event.detail.value.length > 0)
          {  
			//FORMM-4649982902556,MF20190007046,20000,21420190000876,2019-02-13T10:31:51.82Z,A0002,APP-4650161736714,REQ-4650161736705
			var selectedRow = event.detail.value;
			var items = selectedRow.toString().split(",");
			
			$("#formView").show('slow');
			$("#listView").hide('slow');
			self.formID(items[0]);
			self.formApplicationNo(items[1]);
			self.formMNumber(items[3]);
		    self.formAmount(items[2]);
		    self.requestDate(items[4].split("T")[0]);
		    self.currentRequestId(items[7]);
		    self.currentApproverId(items[6]);
          }
	  };
	  self.saveBCSTransaction = function(formID, fxAmount){
			
			var successCode = "001";			
			
			//URL to instantiate a process
			var serviceURL = self.rootModel.BCSExecuteURL;
			var chainCodeName = self.rootModel.BCSChainCodeName;
			var channelName = self.rootModel.BCSChannel;
			var chainCodeVer = self.rootModel.BCSChainCodeVer;
			console.log('BC URL:' + serviceURL);
			$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, processing bloackchain information.</em>");
			
			console.log(serviceURL + "=============>" + chainCodeName);
			
			var args = "[\"" + formID + "\",\"" + fxAmount + "\"] ";
			
			paramItem = {};
			paramItem["channel"] = channelName;
			paramItem["chaincode"] = chainCodeName;
			paramItem["method"] = "netoff";
			paramItem["args"] = args;
			paramItem["chaincodeVer"] = chainCodeVer;
			
		    //console.log(self.blockchainArgs());
			console.log('....about to start BCS request to send REST ...' + JSON.stringify(paramItem));
			//return;
			
			$.ajax ( {
				type: 'POST',
				url: serviceURL,
				enctype: 'raw',
				data: JSON.stringify(paramItem),
				cache: false,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');   
					
				},
				success: function(data) { 
					//self.processUpdateRESTInfo(data);
					console.log('Data...' + JSON.stringify(data));
					
					self.returnCode = data.returnCode;
					if(self.returnCode == "Success"){
						$("#status").html('<br/>Transaction Completed Successfully... Please wait for confirmation');
						//window.location.replace('/?root=' + self.navTo + "&type=" + successCode);
						//return;
						self.processPaymentRequest();
					}if(self.returnCode == "Failure"){
						$("#status").html('<br/>Unable to Completed Registration on Blockchain.');
						return;
					}else{
						//try removing record from blockchain
					}
					
					//var encodedKey = btoa(transactionRef);
					//var encodedName = btoa(fullName);
					//window.location.replace("./booked?key=" + encodedKey + "&name=" + encodedName);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("=====sending error ========" + jqXHR.responseText);
					$("#status").html("<br/><em class='error-message'>Error while registering account. Please try again later</em>");
				}
			} ); 
	  };
	  self.processPaymentRequest = function(){
			//URL to instantiate a process
			var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/fx/form/netoff/";
			
			$("#pstatus").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, Netting off Amount...</em>");
			
			var txtDesc = $("#txtDesc").val();
			
			paramItem = {};
			paramItem["formid"] = self.formID();
			paramItem["approved_amount"] = self.formAmount();
			paramItem["auditid"] = generateCID("AUD", 3);
			paramItem["transactionid"] = generateCID("TRX", 4);
			paramItem["feedback"] = txtDesc;
			paramItem["action_by"] = sessionStorage.getItem("userId");
			paramItem["id"] = self.currentApproverId();
			
			console.log('....about to start request to send REST ...' + JSON.stringify(paramItem));
			
			$.ajax ( {
				type: 'POST',
				url: serviceURL,
				enctype: 'raw',
				data: JSON.stringify(paramItem),
				cache: false,
				processData: false,
				contentType: false,
				crossDomain: true,
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*'); 
				},
				success: function(data) { 
					//console.log('Saved successfully...' + data);
					if(data.result == "1"){
						//reinitialize obserbale array
						self.itemObservableArray([]);
						$("#txtDesc").val("");
						self.formID("");
						self.formMNumber("");
						self.formApplicationNo("");
						self.formAmount("0");
						self.requestDate("");
						self.currentRequestId("");
						self.currentApproverId("");
						self.getFXList();
						$("#pstatus").html("<br/><img src='./images/success.png' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>FX Net-Off operation was successful. Awaiting approver action.</em>");
					}else{
						$("#pstatus").html("<br/><em class='error-message'>Error while processing request. Please try again later</em>"); 
					}
					
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("=====sending error ========" + jqXHR.responseText);
					$("#pstatus").html("<br/><em class='error-message'>Error while saving hscodes. Please try again later</em>");
				}
			} ); 
			
	  };
	  
	  self.getFXList = function(){
		console.log("in getting formM list " + self.formStatus());
		//URL to instantiate a process
		//https://129.156.113.190/ords/PDB1/fcmb/fx/request/approval/list/
		var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/fx/request/approval/list/";
		
		$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting form data.</em>");
		console.log("---->>>>>>>>>>>" + serviceURL);
		
		$.ajax ( {
			type: 'GET',
			url: serviceURL,
			cache: true,
			processData: false,
			contentType: false,
			crossDomain: true,
			xhrFields: { withCredentials: false },
			beforeSend: function (xhr) { 
				console.log('setting credentials.......');
				xhr.setRequestHeader ('Content-Type', 'application/json');  
				xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
				xhr.setRequestHeader ('status', self.formStatus());
				//xhr.setRequestHeader ('inputer', self.inputer());
			},
			success: function(data) { 
				
				console.log(JSON.stringify(data));
				
				/*
				select f.amount, f.application_number, f.form_m_number, f.formid, f.request_date, a.approvalid, a.requestid, a.check_validity, a.check_documentation, a.check_total_outstanding, a.check_fund_availability, a.status, a.action_by
from fx_request_approval a join fx_request f on a.requestid = f.requestid where status=:status
				*/
				var countedResult = data.items.length;
				var topData = [];
				for(i = 0; i < countedResult; i++){
					itemData = data.items[i];
					jsonData = itemData;//JSON.parse(JSON.stringify(itemData));
					//console.log("branch..." + data.items[i].branchname);
					var fxData = {
								 'formid': jsonData.formid,
								 'applicationNumber': jsonData.application_number,
								 'amount': jsonData.amount,
								 'formMNumber': jsonData.form_m_number,
								 'requestDate': jsonData.request_date,
								 'actionBy': jsonData.action_by,
								 'approvalId': jsonData.approvalid,
								 'requestId': jsonData.requestid
							  };
					self.itemObservableArray.push(fxData);
					//self.formMObservableArray.push(fxData);
					
				}
				$("#status").html("");
				//self.formMDataProvider(new oj.ArrayTableDataSource(self.formMArray, {idAttribute: 'formid'}));
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("=====sending error ========" + jqXHR.responseText);
				$("#status").html("<br/><em class='error-message'>Error getting existing Form M List. Please try again later</em>");
				//$("#status").html(jqXHR.responseText);
			}
		} );
		
		
	  };
	  self.getFXList();
      // Below are a set of the ViewModel methods invoked by the oj-module component.
      // Please reference the oj-module jsDoc for additional information.

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here. 
       * This method might be called multiple times - after the View is created 
       * and inserted into the DOM and after the View is reconnected 
       * after being disconnected.
       */
      self.connected = function() {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function() {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = function() {
        // Implement if needed
      };
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    return new NetOffViewModel();
  }
);
