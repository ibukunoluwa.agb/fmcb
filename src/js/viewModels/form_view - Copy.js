/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your about ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
		'ojs/ojbutton', 'ojs/ojrouter', 'ojs/ojlabel', 'messages', 'ojs/ojarraydataprovider', 'ojs/ojtable',
		'ojs/ojmessages', 'ojs/ojmessage'],
 function(oj, ko, $) {
  
	function getParameterByName(name, url) {
		if (!url) url = window.location.href;
		name = name.replace(/[\[\]]/g, '\\$&');
		var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, ' '));
	}
	
	
    function FormViewModel() {
		var self = this;
		self.rootModel = ko.dataFor(document.getElementById('globalBody'));
		self.navToError = "error";
		self.inputer = ko.observable(sessionStorage.getItem("userId"));
		
		self.formID = ko.observable(getParameterByName("fid"));
		if(self.formID() == null || self.formID() == ""){
			window.location.replace('/?root=' + self.navToError + "&code=100");
			return;
		}
		self.formStatus = ko.observable("PENDING");
		//self.formID = ko.observable("FORMM-4646659937061");
	  
		//declare all variables for form
		self.formtype = ko.observable("");
		self.forex_validity = ko.observable("");
		self.form_prefix = ko.observable("");
		self.general_description = ko.observable("");
		self.status = ko.observable("");
		self.create_date = ko.observable("");

		self.applicant_name = ko.observable("");
		self.applicant_rc_number = ko.observable("");
		self.applicant_account_no = ko.observable("");
		self.applicant_address = ko.observable("");
		self.applicant_email = ko.observable("");
		self.applicant_phoneno = ko.observable("");
		self.applicant_taxid = ko.observable("");
		self.applicant_fax = ko.observable("");
		self.applicant_statecode = ko.observable("");
		self.applicant_statedesc = ko.observable("");
		
		self.beneficiary_name = ko.observable("");
		self.beneficiary_address = ko.observable("");
		self.beneficiary_email = ko.observable("");
		self.beneficiary_phoneno = ko.observable("");
		self.beneficiary_fax = ko.observable("");
		
		self.customs_office = ko.observable("");
		self.shipment_mode = ko.observable("");
		self.country_of_origin = ko.observable("");
		self.country_of_supply = ko.observable("");
		self.actual_port_loading = ko.observable("");
		self.actual_port_discharge = ko.observable("");
		self.inspection_agent = ko.observable("");
		self.shipment_date = ko.observable("");
		self.designated_bank = ko.observable("");
		self.fund_source = ko.observable("");
		self.currency_code = ko.observable("");

		self.exchange_rate = ko.observable("");
		self.total_fob_value = ko.observable("");
		self.total_freight_charge = ko.observable("");
		self.ancilary_charge = ko.observable("");
		self.insurance_value = ko.observable("");
		self.total_cf_value = ko.observable("");
		self.total_foc = ko.observable("");
		self.proforma_invoice_no = ko.observable("");
		self.proforma_invoice_date = ko.observable("");
		self.payment_mode = ko.observable("");
		self.payment_date = ko.observable("");
		self.transfer_mode = ko.observable("");
		self.delivery_term = ko.observable("");
		
		self.no_of_goods = ko.observable("");
		self.net_weight = ko.observable("");
		
		self.navTo = "form_m";

		self.newButtonAction = function(event){
		  
		  window.location.replace('/?root=' + self.navTo);
		};
	  
	  
	  
	  self.getFormItemsDetails = function(){
		console.log("in getting formM list");
		//URL to instantiate a process
		//https://129.156.113.190/ords/pdb1/fcmb/formm/hscode/list/{formid}
		var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/formm/hscode/list/" + self.formID();
		
		$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting form data.</em>");
		console.log("---->>>>>>>>>>>" + serviceURL);
		
		$.ajax ( {
			type: 'GET',
			url: serviceURL,
			cache: true,
			processData: false,
			contentType: false,
			crossDomain: true,
			xhrFields: { withCredentials: false },
			beforeSend: function (xhr) { 
				//console.log('setting credentials.......');
				xhr.setRequestHeader ('Content-Type', 'application/json');  
				xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
				xhr.setRequestHeader ('status', self.formStatus());
				xhr.setRequestHeader ('inputer', self.inputer());
			},
			success: function(data) { 
				console.log("----------------");
				console.log(JSON.stringify(data));
				var countedResult = data.items.length;
				var topData = [];
				var total_net_weight = 0;
				for(i = 0; i < countedResult; i++){
					itemData = data.items[i];
					jsonData = itemData;//JSON.parse(JSON.stringify(itemData));
					total_net_weight = parseFloat(total_net_weight) + parseFloat(jsonData.net_weight);
					self.renderHTML(i, jsonData);
				}
				 
				self.no_of_goods(countedResult);
				self.net_weight(total_net_weight);
				
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("=====sending error ========" + jqXHR.responseText);
				$("#status").html("<br/><em class='error-message'>Error verifying email and password. Please try again later</em>");
				//$("#status").html(jqXHR.responseText);
			}
		} );
		
		
	  };
	  self.renderHTML = function (counter, item){ 
			
			var template = $("#item_template").html();
			var generatedOutput  = template.replace(/TEMP_SN/g, counter);
			generatedOutput  = generatedOutput.replace(/TEMP_HSCODE/g, item.hscode);
			generatedOutput  = generatedOutput.replace(/TEMP_DESC/g, item.commercial_description);
			generatedOutput  = generatedOutput.replace(/TEMP_STATEOFGOODS/g, item.state_of_goods);
			generatedOutput  = generatedOutput.replace(/TEMP_SEC_PURPOSE/g, item.sectoral_purpose);
			generatedOutput  = generatedOutput.replace(/TEMP_P_QTY/g, item.packages);
			generatedOutput  = generatedOutput.replace(/TEMP_P_TYPE/g, item.packages_unit);
			
			generatedOutput  = generatedOutput.replace(/TEMP_NET/g, item.net_weight);
			generatedOutput  = generatedOutput.replace(/TEMP_M_QTY/g, item.measurement_quantity);
			generatedOutput  = generatedOutput.replace(/TEMP_M_TYPE/g, item.measurement_unit);
			
			generatedOutput  = generatedOutput.replace(/TEMP_PRICE/g, item.unit_price);
			generatedOutput  = generatedOutput.replace(/TEMP_FOB/g, item.fob_value);
			generatedOutput  = generatedOutput.replace(/TEMP_FREIGHT/g, item.freight_charge);
			
			currentHTML = $("#itemTable").html();
			$("#itemTable").html(currentHTML + generatedOutput);
			//document.getElementById["flightinfo"].innerHTML = currentHTML + output;
			
	  }
	  
      self.getFormDetails = function(){
		console.log("in getting formM list");
		//URL to instantiate a process
		var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/formm/details/" + self.formID();
		
		$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting form data.</em>");
		console.log("---->>>>>>>>>>>" + serviceURL);
		
		$.ajax ( {
			type: 'GET',
			url: serviceURL,
			cache: true,
			processData: false,
			contentType: false,
			crossDomain: true,
			xhrFields: { withCredentials: false },
			beforeSend: function (xhr) { 
				//console.log('setting credentials.......');
				xhr.setRequestHeader ('Content-Type', 'application/json');  
				xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
				xhr.setRequestHeader ('status', self.formStatus());
				xhr.setRequestHeader ('inputer', self.inputer());
			},
			success: function(data) { 
				
				console.log(JSON.stringify(data));
				
				jsonData = data;//JSON.parse(JSON.stringify(itemData));
				//console.log("branch..." + data.items[i].branchname);
				
				
				self.formtype(jsonData.formtype);
				self.forex_validity(jsonData.forex_validity);
				self.form_prefix(jsonData.form_prefix);
				self.general_description(jsonData.general_description);
				self.status(jsonData.status);
				self.create_date(jsonData.create_date);
				
				self.applicant_name(jsonData.applicant_name);
				self.applicant_rc_number(jsonData.applicant_rc_number);
				self.applicant_account_no(jsonData.applicant_account_no);
				self.applicant_address(jsonData.applicant_address);
				self.applicant_email(jsonData.applicant_email);
				self.applicant_phoneno(jsonData.applicant_phoneno);
				self.applicant_taxid(jsonData.taxid);
				self.applicant_fax(jsonData.applicant_fax);
				self.applicant_statecode(jsonData.applicant_state);
				self.applicant_statedesc(jsonData.applicant_state);
				
				self.beneficiary_name(jsonData.beneficiary_name);
				self.beneficiary_address(jsonData.beneficiary_address);
				self.beneficiary_email(jsonData.beneficiary_email);
				self.beneficiary_phoneno(jsonData.beneficiary_phoneno);
				self.beneficiary_fax(jsonData.beneficiary_fax);
				
				self.customs_office(jsonData.customs_office);
				self.shipment_mode(jsonData.shipment_mode);
				self.country_of_origin(jsonData.country_of_origin);
				self.country_of_supply(jsonData.country_of_supply);
				self.actual_port_loading(jsonData.actual_port_loading);
				self.actual_port_discharge(jsonData.actual_port_discharge);
				self.inspection_agent(jsonData.inspection_agent);
				self.shipment_date(jsonData.shipment_date);
				self.designated_bank(jsonData.designated_bank);
				self.fund_source(jsonData.fund_source);
				self.currency_code(jsonData.currency_code);
				
				self.exchange_rate(jsonData.exchange_rate);
				self.total_fob_value(jsonData.total_fob_value);
				self.total_freight_charge(jsonData.total_freight_charge);
				self.ancilary_charge(jsonData.ancilary_charge);
				self.insurance_value(jsonData.insurance_value);
				self.total_cf_value(jsonData.total_cf_value);
				self.total_foc(jsonData.total_foc);
				self.proforma_invoice_no(jsonData.proforma_invoice_no);
				self.proforma_invoice_date(jsonData.proforma_invoice_date);
				self.payment_mode(jsonData.payment_mode);
				self.payment_date(jsonData.payment_date);
				self.transfer_mode(jsonData.transfer_mode);
				self.delivery_term(jsonData.delivery_term);
				
				self.getFormItemsDetails();
				
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("=====sending error ========" + jqXHR.responseText);
				$("#status").html("<br/><em class='error-message'>Error verifying email and password. Please try again later</em>");
				//$("#status").html(jqXHR.responseText);
			}
		} );
		
		
	  };
	  self.getFormDetails();
	  
      // Below are a subset of the ViewModel methods invoked by the ojModule binding
      // Please reference the ojModule jsDoc for additional available methods.

      /**
       * Optional ViewModel method invoked when this ViewModel is about to be
       * used for the View transition.  The application can put data fetch logic
       * here that can return a Promise which will delay the handleAttached function
       * call below until the Promise is resolved.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @return {Promise|undefined} - If the callback returns a Promise, the next phase (attaching DOM) will be delayed until
       * the promise is resolved
       */
      self.handleActivated = function(info) {
        
      };

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {boolean} info.fromCache - A boolean indicating whether the module was retrieved from cache.
       */
      self.handleAttached = function(info) {
        // Implement if needed
		
      };


      /**
       * Optional ViewModel method invoked after the bindings are applied on this View. 
       * If the current View is retrieved from cache, the bindings will not be re-applied
       * and this callback will not be invoked.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       */
      self.handleBindingsApplied = function(info) {
        // Implement if needed
      };

      /*
       * Optional ViewModel method invoked after the View is removed from the
       * document DOM.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {Array} info.cachedNodes - An Array containing cached nodes for the View if the cache is enabled.
       */
      self.handleDetached = function(info) {
        // Implement if needed
      };
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    //return new FormMPendingListViewModel();
	var fmM = new FormViewModel();
	$(document).ready
    (
		function()
		{
			console.log("-------event when document is ready");
			//var formMTable = document.getElementById('formMList');
			//ko.applyBindings(fmM, formMTable);
			//formMTable.addEventListener('currentRowChanged', fmM.currentRowListener);
			
			/*
			
			
			table.addEventListener('ojBeforeCurrentRow', vm.beforeCurrentRowListener);
			table.addEventListener('ojSort', vm.sortListener);
			table.addEventListener('selectionChanged', vm.selectionListener);*/
		}
	);
	return fmM;
  }
);
