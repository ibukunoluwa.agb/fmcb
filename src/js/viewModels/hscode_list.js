/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your about ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojpagingcontrol', 'ojs/ojpagingtabledatasource',
		'ojs/ojbutton', 'ojs/ojrouter', 'ojs/ojlabel', 'messages', 'ojs/ojarraydataprovider', 'ojs/ojtable','ojs/ojdialog', 'ojs/ojfilepicker'],
 function(oj, ko, $) {
  
	
    function HSCodeListViewModel() {
      var self = this;
	  self.rootModel = ko.dataFor(document.getElementById('globalBody'));
	  //clear the sessionStorage
	  //sessionStorage.removeItem("mod_FormMID");
	  //sessionStorage.removeItem("mod_Mode");
	  self.currentCount = 0;
	  self.totalCount = 0;
	  
	  self.selectedRowKey = ko.observable("");
	  
	  self.navTo = "hscode_upload";
	  self.hscodeArray = [];
	  self.hscodeObservableArray = ko.observableArray(self.hscodeArray);
	  //self.formMDataProvider = new oj.ArrayDataProvider(self.formMObservableArray, {keyAttributes: 'formid'});
	  self.hscodeDataProvider = new oj.ArrayTableDataSource(self.hscodeObservableArray, {idAttribute: 'hscode'});
	  self.paginDataSource = new oj.PagingTableDataSource(self.hscodeDataProvider);
	  
	  self.hscodeUploadArray = [];
	  self.hscodeUploadObservableArray = ko.observableArray(self.hscodeUploadArray);
	  self.hscodeUploadDataProvider = new oj.ArrayTableDataSource(self.hscodeUploadObservableArray, {idAttribute: 'hscode'});
	  self.uploadPaginDataSource = new oj.PagingTableDataSource(self.hscodeUploadDataProvider);
	  
	  self.defaultStatus = ko.observable("1");
	  this.statusList = ko.observableArray([
		  {value: '1', label: 'FX Enabled Codes'},
		  {value: '0',  label: 'Banned FX Item'}
		]);
		
	  //file input settings
	  self.fileName = "";
	  self.fileType = '';
	  self.fileBlob = "";
	  self.fileContent = "";
	  self.fileExtension = "";
	  self.fileNames = ko.observableArray([]);
	  self.progressValue = ko.observable(0);
	  
	  function getExtension(path) {
		var basename = path.split(/[\\/]/).pop(),  // extract file name from full path ... // (supports `\\` and `/` separators)
			pos = basename.lastIndexOf(".");       // get last position of `.`

		if (basename === "" || pos < 1)            // if file name is empty or ...
			return "";                             //  `.` not found (-1) or comes first (0)

		return basename.slice(pos + 1);            // extract extension ignoring `.`
	  }
	 
      console.log('----------**-==-**-------------'); 
	  self.close = function (event) {
		document.getElementById('modalDialog1').close();
	  }

	  self.open = function (event) {
		document.getElementById('modalDialog1').open();
		
	  }

	  self.currentRowListener = function(event)
	  {
		  console.log("*******************");
		  var data = event.detail;
		  console.log(data);
		  if (event.type == 'currentRowChanged' && data['value'] != null)
		  {
			var rowIndex = data['value']['rowIndex'];
			var rowKey = data['value']['rowKey'];
			console.log("selected Row Key" + rowKey);
			self.selectedRowKey(rowKey);
			
			
		  }
      };
	  
	  self.searchButtonAction = function(event)
	  {
		  console.log("*******edit button************" + event);
		  var txtSearch = $("#txtSearch").val();
		  if(txtSearch.trim() == ""){
			if(!window.confirm("The result might be much, do you want to continue?")){
				return false;
			}
		  }
		  self.getHSCodeList(txtSearch.trim());
      };
	  
	  self.editButtonAction = function(event)
	  {
		  console.log("*******edit button************" + self.selectedRowKey());
		  var data = event.detail;
		  console.log(data);
		  if (self.selectedRowKey() != "")
		  {
			
			//sessionStorage.setItem("mod_FormMID", self.selectedRowKey());
			//sessionStorage.setItem("mod_Mode", "MODIFY");
			
			//window.location.replace('/?root=' + self.navTo);
			
		  }
      };
	
      self.getHSCodeList = function(searchKey){
		console.log("in getting hscode list");
		//URL to instantiate a process
		var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/config/hscodes/";
		
		$("#status").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, getting form data.</em>");
		console.log("---->>>>>>>>>>>" + serviceURL);
		//empty array before processing
		self.hscodeArray= [];
		self.hscodeObservableArray([]);
		$.ajax ( {
			type: 'GET',
			url: serviceURL,
			cache: true,
			processData: false,
			contentType: false,
			crossDomain: true,
			xhrFields: { withCredentials: false },
			beforeSend: function (xhr) { 
				//console.log('setting credentials.......');
				xhr.setRequestHeader ('Content-Type', 'application/json');  
				xhr.setRequestHeader ('Access-Control-Allow-Origin', '*');
				xhr.setRequestHeader ('like', '%' + searchKey + '%');
			},
			success: function(data) { 
				
				console.log(JSON.stringify(data));
				var countedResult = data.items.length;
				var topData = [];
				for(i = 0; i < countedResult; i++){
					itemData = data.items[i];
					jsonData = itemData;//JSON.parse(JSON.stringify(itemData));
					//console.log("branch..." + data.items[i].branchname);
					var hscodeData = {
								 'hscode': jsonData.hscode,
								 'description': jsonData.description,
								 'status': jsonData.status
							  };
					self.hscodeArray.push(hscodeData);
					self.hscodeObservableArray.push(hscodeData);
					
				}
				var hscodeTable = document.getElementById('HSCodeList');
				hscodeTable.refresh();
				$("#status").html("");
				//self.formMDataProvider(new oj.ArrayTableDataSource(self.formMArray, {idAttribute: 'formid'}));
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log("=====sending error ========" + jqXHR.responseText);
				$("#status").html("<br/><em class='error-message'>Error verifying email and password. Please try again later</em>");
				//$("#status").html(jqXHR.responseText);
			}
		} );
		
		
	  };
	  
	  self.selectListener = function(event) {
		//$("#selectedFile").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, processing uploaded file.</em>");
		
        var files = event.detail.files;
        //we can pick more than one file but lets focus on just one and the first one
        for (var i = 0; i < 1/*files.length*/; i++) {
          //$("#uploadStatus").show('slow');
		  $("#uploadStatus").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, processing uploaded file.</em>");
		  
          var file = files[i];
          
          self.fileType = files[i].type;
          self.fileName = files[i].name;
          self.fileExtension = getExtension(self.fileName);
          console.log('Extension... ' + getExtension(self.fileName));
          
		  var reader = new FileReader();
		  //read the file by using our method - readFile
		  reader.addEventListener('load', self.readFile);
		  
		  //try this out later
		  //reader.addEventListener('progress', self.showProgress);
		  //reader.addEventListener('loadend', self.showLoadEnd);
		  
		  console.log(self.fileName + 'file type...' + files[i].type);
		  //reader.readAsBinaryString(file);
		  //reader.readAsArrayBuffer(file);
		  reader.readAsText(file);
		  //reader.readAsBinaryString(blob);
		  
		  //self.fileNames.push(files[i].name);
          
        }
      }
      
      self.readFile = function(event){
			//console.log('reading file..............' + event.result);
			//console.log(event.target.result);
			var csvContent = event.target.result;
			self.fileContent = csvContent;
			//console.log(arrayBuffer + '****2****' + self.fileType);
			
			$("#selectedFile").html("<b>File Selected: " + self.fileName + "</b>");
			
			
	  };
	  self.processCSVButtonAction = function(event){
		    $("#uploadStatus").show('slow');
			var csvContent = self.fileContent;
			
			$("#uploadStatus").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, processing CSV file content.</em>");
			
			$("#csvGrid").show('slow');
			
			//code before the pause
			setTimeout(function(){
				//process CSV file
				self.processCSVFile(csvContent);
			}, 3000);
			
	  };
	  
	  self.processCSVFile = function(data){
		  console.log(data);
		  self.hscodeUploadArray = [];
		  self.hscodeUploadObservableArray([]);
		  
		  
		  var rows = data.split("\r\n");
		  
		  for (var i = 1; i < rows.length; i++) {
				var cells = rows[i].split(",");
				console.log(cells[0] + '============' + cells[1]);
				var hscodeData = {
					 'hscode': cells[0],
					 'description': cells[1]
				  };
				self.hscodeUploadArray.push(hscodeData);
				self.hscodeUploadObservableArray.push(hscodeData);
		  }
		  $("#uploadStatus").html("<br/><img src='./images/success.png' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>File processed successfully.</em>");
		  
		  
		  $("#csvSpace").show('slow');
		  $("#csvDropdown").show('slow');
			
		  var hscodeTable = document.getElementById('uploadedList');
		  hscodeTable.refresh();
	  };
	  self.processNextArrayItem = function(){
		  if(self.currentCount < self.totalCount){
			  var txtStatus = $("#cbStatus").val();	
			  var hsCode = self.hscodeUploadArray[self.currentCount];
			  self.saveHSCode(parseInt(self.currentCount) + 1, hsCode.hscode, hsCode.description, txtStatus);
		  }else{
			console.log("end of it all.....");  
		  }
		  
	  };
	  
	  self.uploadSaveAction = function(event){
		  var counted = self.hscodeUploadArray.length;
		  if(counted == 0){
			  alert("No item to process");
			  return;
		  }
		  console.log("counted items..." + counted);
		  var txtStatus = $("#cbStatus").val();
		  self.currentCount = 0;
		  self.totalCount = counted;
		  
		  self.processNextArrayItem();
		  
		  
		  /*
		  for (var i = 0; i < counted; i++) {
			  
			  var persist = function(position, code, desc, status){
				  self.saveHSCode(parseInt(position) + 1, code, desc, status);
			  };
			  
			  var hsCode = self.hscodeUploadArray[i]; 
			  setTimeout(persist(i, hsCode.hscode, hsCode.description, txtStatus), 5000 * i);
				  
			  //setTimeout(function(){
				//var hsCode = self.hscodeUploadArray[i];
				//console.log(hsCode.hscode + "*************************" + hsCode.description);
				//self.saveHSCode(parseInt(i) + 1, hsCode.hscode, hsCode.description, txtStatus);
				//console.log(parseInt(i) + 1, );
			  //}, 5000 * i); 
		  } */
		  
	  }
	  self.saveHSCode = function(pos, hsCode, description, status){
			//URL to instantiate a process
			
			console.log("+++++++++++++++++++++++++++++++");
			
			var serviceURL = self.rootModel.ORDSURL + "/ords/PDB1/fcmb/config/hscode/add/";
			
			$("#savestatus").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, Saving HS Code at position " + pos + "...</em>");
			console.log(sessionStorage.getItem("userId") + "---->>>>>>>>>>>" + serviceURL);
			
			
			paramItem = {};
			paramItem["hscode"] = hsCode;
			paramItem["description"] = description;
			paramItem["status"] = status;
			
			console.log('....about to start request to send REST ...' + JSON.stringify(paramItem));
			
			$.ajax ( {
				type: 'POST',
				url: serviceURL,
				enctype: 'raw',
				data: JSON.stringify(paramItem),
				cache: false,
				processData: false,
				contentType: false,
				crossDomain: true, 
				xhrFields: { withCredentials: false },
				beforeSend: function (xhr) { 
					console.log('setting credentials.......');
					xhr.setRequestHeader ('Content-Type', 'application/json');  
					xhr.setRequestHeader ('Access-Control-Allow-Origin', '*'); 
				},
				success: function(data) { 
					//self.processUpdateRESTInfo(data);
					console.log('Saved successfully...' + data);
					//add details to grid object
					
					self.hscodeUploadArray.splice( self.hscodeUploadArray.indexOf(hsCode), 1 );
					self.hscodeUploadObservableArray.splice( self.hscodeUploadObservableArray.indexOf(hsCode), 1 );
					var hscodeTable = document.getElementById('uploadedList');
					hscodeTable.refresh();
					$("#savestatus").html("");
					self.currentCount = parseInt(self.currentCount) + 1;
					self.processNextArrayItem();
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log("=====sending error ========" + jqXHR.responseText);
					var prevMessage = $("#errorstatus").html();
					$("#errorstatus").html(prevMessage + "<br/><em class='error-message'>Error while saving HSCode " + hsCode + "</em>");
				}
			} ); 
			
	  };
	  
	  
	  //self.getHSCodeList();
	  
      // Below are a subset of the ViewModel methods invoked by the ojModule binding
      // Please reference the ojModule jsDoc for additional available methods.

      /**
       * Optional ViewModel method invoked when this ViewModel is about to be
       * used for the View transition.  The application can put data fetch logic
       * here that can return a Promise which will delay the handleAttached function
       * call below until the Promise is resolved.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @return {Promise|undefined} - If the callback returns a Promise, the next phase (attaching DOM) will be delayed until
       * the promise is resolved
       */
      self.handleActivated = function(info) {
        // Implement if needed
		sessionStorage.removeItem("mod_FormMID");
	    sessionStorage.removeItem("mod_Mode");
      };

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {boolean} info.fromCache - A boolean indicating whether the module was retrieved from cache.
       */
      self.handleAttached = function(info) {
        // Implement if needed
		
      };


      /**
       * Optional ViewModel method invoked after the bindings are applied on this View. 
       * If the current View is retrieved from cache, the bindings will not be re-applied
       * and this callback will not be invoked.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       */
      self.handleBindingsApplied = function(info) {
        // Implement if needed
      };

      /*
       * Optional ViewModel method invoked after the View is removed from the
       * document DOM.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {Array} info.cachedNodes - An Array containing cached nodes for the View if the cache is enabled.
       */
      self.handleDetached = function(info) {
        // Implement if needed
      };
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    //return new FormMPendingListViewModel();
	var fmM = new HSCodeListViewModel();
	$(document).ready
    (
		function()
		{
			$("#uploadStatus").html("<br/><img src='./images/spinner.gif' style='vertical-align: middle;' width='24' height'24'/>&nbsp;<em class='success-message'>Please wait, processing uploaded file.</em>");
			$("#uploadStatus").hide('fast');
			
			$("#csvGrid").hide('fast');
			$("#csvSpace").hide('fast');
			$("#csvDropdown").hide('fast');
	  
	  
			console.log("-------event when document is ready");
			//var formMTable = document.getElementById('formMList');
			//ko.applyBindings(fmM, formMTable);
			//formMTable.addEventListener('currentRowChanged', fmM.currentRowListener);
			
			/*
			
			
			table.addEventListener('ojBeforeCurrentRow', vm.beforeCurrentRowListener);
			table.addEventListener('ojSort', vm.sortListener);
			table.addEventListener('selectionChanged', vm.selectionListener);*/
		}
	);
	return fmM;
  }
);
